/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
  config.toolbarGroups = [
    { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'editing', groups: ['editing', 'links'] },
    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'paragraph'] },
    '/',
    { name: 'insert', groups: ['insert'] },
    { name: 'styles', groups: ['styles'] },
    { name: 'colors', groups: ['colors'] },
    { name: 'tools', groups: ['tools'] },
    { name: 'others', groups: ['others'] },
  ];

  config.removeButtons = [
    'About',
    'Save',
    'NewPage',
    'Print',
    'Templates',
    'Find',
    'Replace',
    'SelectAll',
    'CopyFormatting',
    'CreateDiv',
    'BidiLtr',
    'BidiRtl',
    'Language',
    'Flash',
    'Smiley',
    'PageBreak',
    'Undo',
    'Redo',
    'Image',
    'SpecialChar',
    'Iframe',
    'EqnEditor',
    'Table',
  ].join(',');
};
