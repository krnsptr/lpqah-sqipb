<?php

return [
    'api_key' => \env('AHAWEBS_API_KEY'),
    'api_url' => \env('AHAWEBS_API_URL'),
];
