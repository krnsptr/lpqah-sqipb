<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('web/provinsi', 'APIWebController@provinsi')->name('api.web.provinsi');
Route::get('web/kota', 'APIWebController@kota')->name('api.web.kota');
Route::get('web/bank', 'APIWebController@bank')->name('api.web.bank');
Route::get('web/kelas', 'APIWebController@kelas')->name('api.web.kelas');
Route::get('web/jenjang', 'APIWebController@jenjang')->name('api.web.jenjang');
Route::get('web/kelompok', 'APIWebController@kelompok')->name('api.web.kelompok');
