<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return \redirect()->route('anggota.masuk');
});

Route::prefix('admin')->namespace('Auth\Admin')->group(function () {
    Route::get('masuk', 'LoginController@showLoginForm')->name('admin.masuk');
    Route::post('masuk', 'LoginController@login');

    Route::post('keluar', 'LoginController@logout')->name('admin.keluar');
});

Route::namespace('Auth\Anggota')->group(function () {
    Route::get('daftar', 'RegisterController@showRegistrationForm')->name('anggota.daftar');
    Route::post('daftar', 'RegisterController@register');

    Route::get('masuk', 'LoginController@showLoginForm')->name('anggota.masuk');
    Route::post('masuk', 'LoginController@login');

    Route::post('keluar', 'LoginController@logout')->name('anggota.keluar');

    Route::get('password/lupa', 'ForgotPasswordController@showLinkRequestForm')
    ->name('anggota.password.lupa');

    Route::post('password/lupa', 'ForgotPasswordController@sendResetLinkEmail');

    Route::get('password/reset/{token?}', 'ResetPasswordController@showResetForm')
    ->name('anggota.password.reset');

    Route::post('password/reset/{token?}', 'ResetPasswordController@reset');

    Route::get('email/verifikasi/gagal', 'VerificationController@error')
    ->name('anggota.email.verifikasi.gagal');

    Route::post('email/verifikasi/kirim', 'VerificationController@resend')
    ->name('anggota.email.verifikasi.kirim');

    Route::get('email/verifikasi/cek/{id}', 'VerificationController@verify')
    ->name('anggota.email.verifikasi.cek');
});

Route::prefix('admin')->middleware('auth:admin')->group(function () {
    Route::get('/', fn () => \redirect()->route('admin.dasbor'));
    Route::get('dasbor', 'AdminController@dasbor')->name('admin.dasbor');

    Route::get('anggota', 'AdminController@anggota')->name('admin.anggota');
    Route::get('anggota/lihat/{anggota}', 'AdminController@anggota_lihat')->name('admin.anggota_lihat');
    Route::post('anggota/lihat/{anggota}', 'AdminController@anggota_lihat');
    Route::post('anggota/masuk/{anggota}', 'AdminController@anggota_masuk')->name('admin.anggota_masuk');

    Route::get('instruktur', 'AdminController@instruktur')->name('admin.instruktur');
    Route::get('instruktur/edit/{instruktur}', 'AdminController@instruktur_edit')->name('admin.instruktur_edit');
    Route::post('instruktur/edit/{instruktur}', 'AdminController@instruktur_edit');

    Route::get('santri', 'AdminController@santri')->name('admin.santri');
    Route::get('santri/edit/{santri}', 'AdminController@santri_edit')->name('admin.santri_edit');
    Route::post('santri/edit/{santri}', 'AdminController@santri_edit');

    Route::get('penjadwalan', 'AdminController@penjadwalan')->name('admin.penjadwalan');
    Route::post('penjadwalan/edit', 'AdminController@penjadwalan_edit')->name('admin.penjadwalan_edit');

    Route::get('biaya', 'AdminController@biaya')->name('admin.biaya');
    Route::get('biaya/edit/{santri}', 'AdminController@biaya_edit')->name('admin.biaya_edit');
    Route::post('biaya/edit/{santri}', 'AdminController@biaya_edit');

    Route::get('statistik', 'AdminController@statistik')->name('admin.statistik');

    Route::get('unduh', 'AdminController@unduh')->name('admin.unduh');

    Route::get('setelan', 'AdminController@setelan')->name('admin.setelan');
    Route::post('setelan', 'AdminController@setelan');
});

Route::middleware('auth:anggota')->group(function () {
    Route::get('dasbor', 'AnggotaController@dasbor')->name('anggota.dasbor');

    Route::get('profil', 'AnggotaController@profil')->name('anggota.profil');
    Route::post('profil', 'AnggotaController@profil');

    Route::get('program/daftar', 'AnggotaController@program_daftar')
    ->name('anggota.program_daftar');

    Route::post('program/daftar', 'AnggotaController@program_daftar');

    Route::get('program/penjadwalan', 'AnggotaController@program_penjadwalan')
    ->name('anggota.program_penjadwalan');

    Route::get('instruktur/daftar/{kelas?}', 'AnggotaController@instruktur_daftar')
    ->name('anggota.instruktur_daftar');

    Route::post('instruktur/daftar/{kelas?}', 'AnggotaController@instruktur_daftar');

    Route::get('instruktur/lihat/{instruktur}', 'AnggotaController@instruktur_lihat')
    ->name('anggota.instruktur_lihat');

    Route::post('instruktur/penjadwalan', 'AnggotaController@instruktur_penjadwalan')
    ->name('anggota.instruktur_penjadwalan');

    Route::get('jadwal/cari', 'AnggotaController@jadwal_cari')->name('anggota.jadwal_cari');
    Route::get('jadwal/tambah', 'AnggotaController@jadwal_tambah')->name('anggota.jadwal_tambah');
    Route::post('jadwal/tambah', 'AnggotaController@jadwal_tambah');
    Route::get('jadwal/edit/{jadwal}', 'AnggotaController@jadwal_edit')->name('anggota.jadwal_edit');
    Route::post('jadwal/edit/{jadwal}', 'AnggotaController@jadwal_edit');
    Route::get('jadwal/hapus/{jadwal}', 'AnggotaController@jadwal_hapus')->name('anggota.jadwal_hapus');
    Route::post('jadwal/hapus/{jadwal}', 'AnggotaController@jadwal_hapus');

    Route::get('santri/daftar/{kelas?}', 'AnggotaController@santri_daftar')
    ->name('anggota.santri_daftar');

    Route::post('santri/daftar/{kelas?}', 'AnggotaController@santri_daftar');

    Route::get('santri/lihat/{santri}', 'AnggotaController@santri_lihat')
    ->name('anggota.santri_lihat');

    Route::get('santri/penjadwalan/{santri}', 'AnggotaController@santri_penjadwalan')
    ->name('anggota.santri_penjadwalan');

    Route::post('santri/penjadwalan/{santri}', 'AnggotaController@santri_penjadwalan');

    Route::get('kelompok', 'AnggotaController@kelompok')->name('anggota.kelompok');

    Route::get('biaya', 'AnggotaController@biaya')->name('anggota.biaya');
});
