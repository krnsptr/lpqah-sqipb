<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AnggotaProfil;
use App\Models\Bank;
use App\Models\Wilayah\Kota;
use Faker\Generator as Faker;

$factory->define(AnggotaProfil::class, function (Faker $faker) {
    static $list_id_kota;
    static $list_id_bank;

    $list_id_kota ??= Kota::pluck('id');
    $list_id_bank ??= Bank::pluck('id');

    $username = $faker->domainWord . '_' . $faker->bothify('??###');
    $status = \collect(AnggotaProfil::STATUS)->keys()->random();

    $jenis_identitas = \collect(AnggotaProfil::JENIS_IDENTITAS[$status])->random();

    $regex_nomor_identitas = AnggotaProfil::REGEX[$jenis_identitas];
    $regex_nomor_hp = AnggotaProfil::REGEX['nomor_hp'];
    $regex_nomor_rekening = AnggotaProfil::REGEX['nomor_rekening'];

    $nomor_hp = $faker->regexify($regex_nomor_hp);
    $nomor_wa = $faker->boolean(80) ? $nomor_hp : $faker->regexify($regex_nomor_hp);
    $nomor_rekening = $faker->regexify($regex_nomor_rekening);

    return [
        'id_kota' => $list_id_kota->random(),
        'id_bank' => $list_id_bank->random(),
        'username' => $username,
        'jenis_kelamin' => $faker->boolean ? 'L' : 'P',
        'tanggal_lahir' => $faker->dateTimeBetween('-25 year', '-17 year'),
        'status' => $status,
        'jenis_identitas' => $jenis_identitas,
        'nomor_identitas' => $faker->regexify($regex_nomor_identitas),
        'nomor_hp' => $nomor_hp,
        'nomor_wa' => $nomor_wa,
        'nomor_rekening' => $nomor_rekening,
        'nama_rekening' => $faker->name,
    ];
});
