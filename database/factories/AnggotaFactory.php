<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Anggota;
use App\Models\AnggotaProfil;
use Faker\Generator as Faker;

$factory->define(Anggota::class, function (Faker $faker) {
    // $password = 'wordpass';
    $password = '$2y$10$H/T7.teffG.1AgrJWV.bBue8Cuh/deWNW6CeV.Qnl2Ntw2savZQz.';

    $username = $faker->userName . '_' . $faker->bothify('??###');
    $domain = $faker->safeEmailDomain;

    return [
        'nama' => $faker->name,
        'email' => "$username@$domain",
        'password' => $password,
        'email_verified_at' => \now(),
    ];
});

$factory->afterCreating(Anggota::class, function (Anggota $anggota) {
    \factory(AnggotaProfil::class)->create(['id' => $anggota->id]);
});
