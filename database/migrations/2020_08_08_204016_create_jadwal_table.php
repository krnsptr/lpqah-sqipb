<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('angkatan')->index();
            $table->unsignedInteger('id_anggota');
            $table->unsignedTinyInteger('hari');
            $table->time('waktu_mulai');
            $table->time('waktu_selesai');
            $table->string('jalur')->nullable();
            $table->timestamps();

            $table->foreign('id_anggota')->references('id')->on('anggota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal');
    }
}
