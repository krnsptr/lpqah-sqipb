<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetelanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setelan', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->unsignedTinyInteger('angkatan')->unique();
            $table->dateTime('waktu_buka_pendaftaran_instruktur');
            $table->dateTime('waktu_tutup_pendaftaran_instruktur');
            $table->dateTime('waktu_buka_pendaftaran_santri');
            $table->dateTime('waktu_tutup_pendaftaran_santri');
            $table->dateTime('waktu_buka_penjadwalan_instruktur');
            $table->dateTime('waktu_tutup_penjadwalan_instruktur');
            $table->dateTime('waktu_buka_penjadwalan_santri');
            $table->dateTime('waktu_tutup_penjadwalan_santri');
            $table->dateTime('waktu_buka_publikasi_jadwal');
            $table->dateTime('waktu_tutup_publikasi_jadwal');
            $table->json('detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_setelan');
    }
}
