<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelompokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelompok', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('angkatan')->index();
            $table->unsignedInteger('id_instruktur');
            $table->unsignedTinyInteger('id_jenjang');
            $table->unsignedBigInteger('id_jadwal')->unique();
            $table->unsignedSmallInteger('nomor')->nullable();
            $table->unsignedTinyInteger('kuota');
            $table->unsignedTinyInteger('sisa')->index();
            $table->timestamps();

            $table->foreign('id_instruktur')->references('id')->on('instruktur');
            $table->foreign('id_jenjang')->references('id')->on('jenjang');
            $table->foreign('id_jadwal')->references('id')->on('jadwal');

            $table->unique(['angkatan', 'nomor']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelompok');
    }
}
