<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSantriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('santri', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('angkatan')->index();
            $table->unsignedInteger('id_anggota');
            $table->unsignedTinyInteger('id_kelas');
            $table->unsignedTinyInteger('id_jenjang')->nullable();
            $table->unsignedBigInteger('id_kelompok')->nullable();
            $table->unsignedSmallInteger('nomor')->nullable();
            $table->string('nomor_induk')->index()->nullable();
            $table->json('detail');
            $table->timestamps();

            $table->foreign('id_anggota')->references('id')->on('anggota');
            $table->foreign('id_kelas')->references('id')->on('kelas');
            $table->foreign('id_jenjang')->references('id')->on('jenjang');
            $table->foreign('id_kelompok')->references('id')->on('kelompok');

            $table->unique(['angkatan', 'id_anggota', 'id_kelas']);
            $table->unique(['angkatan', 'nomor']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('santri');
    }
}
