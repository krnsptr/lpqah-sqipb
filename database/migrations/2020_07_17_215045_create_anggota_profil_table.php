<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnggotaProfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota_profil', function (Blueprint $table) {
            $table->unsignedInteger('id')->primary();
            $table->unsignedTinyInteger('id_provinsi');
            $table->unsignedSmallInteger('id_kota');
            $table->unsignedInteger('id_bank')->nullable();
            $table->string('username', 20)->unique();
            $table->char('jenis_kelamin', 1);
            $table->date('tanggal_lahir');
            $table->unsignedTinyInteger('status');
            $table->string('jenis_identitas');
            $table->string('nomor_identitas');
            $table->string('nomor_hp');
            $table->string('nomor_wa');
            $table->string('nomor_rekening')->nullable();
            $table->string('nama_rekening')->nullable();
            $table->json('detail');
            $table->timestamps();

            $table->foreign('id')->references('id')->on('anggota');
            $table->foreign('id_provinsi')->references('id')->on('wilayah_provinsi');
            $table->foreign('id_kota')->references('id')->on('wilayah_kota');
            $table->foreign('id_bank')->references('id')->on('bank');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota_profil');
    }
}
