<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWilayahKotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wilayah_kota', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedTinyInteger('id_provinsi');
            $table->string('nama');
            $table->string('kode')->nullable();

            $table->foreign('id_provinsi')->references('id')->on('wilayah_provinsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wilayah_kota');
    }
}
