<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiayaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biaya', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_santri')->unique();
            $table->unsignedTinyInteger('status');
            $table->unsignedInteger('nominal');
            $table->string('keterangan')->nullable();
            $table->timestamps();

            $table->foreign('id_santri')->references('id')->on('santri');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biaya');
    }
}
