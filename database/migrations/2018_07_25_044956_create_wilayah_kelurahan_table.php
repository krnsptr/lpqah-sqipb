<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWilayahKelurahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wilayah_kelurahan', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->unsignedSmallInteger('id_kecamatan');
            $table->string('nama');
            $table->string('kode')->nullable();

            $table->foreign('id_kecamatan')->references('id')->on('wilayah_kecamatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wilayah_kelurahan');
    }
}
