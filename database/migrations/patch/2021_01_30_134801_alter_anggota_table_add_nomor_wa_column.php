<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAnggotaTableAddNomorWaColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('anggota', 'nomor_wa')) {
            Schema::table('anggota', function (Blueprint $table) {
                $table->string('nomor_wa')->nullable()->after('foto');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('anggota', 'nomor_wa')) {
            Schema::table('anggota', function (Blueprint $table) {
                $table->dropColumn('nomor_wa');
            });
        }
    }
}
