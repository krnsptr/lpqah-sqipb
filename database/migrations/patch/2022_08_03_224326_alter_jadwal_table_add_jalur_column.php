<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterJadwalTableAddJalurColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('jadwal', 'jalur')) {
            Schema::table('jadwal', function (Blueprint $table) {
                $table->string('jalur')->nullable()->after('waktu_selesai');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('jadwal', 'jalur')) {
            Schema::table('jadwal', function (Blueprint $table) {
                $table->dropColumn('jalur');
            });
        }
    }
}
