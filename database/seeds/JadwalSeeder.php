<?php

use App\Enums\Hari;
use App\Models\Anggota;
use App\Models\Instruktur;
use App\Models\Jadwal;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $list_id_instruktur = Instruktur::pluck('id');
        $list_id_anggota = Anggota::whereIn('id', $list_id_instruktur)->pluck('id');

        \DB::beginTransaction();

        try {
            foreach ($list_id_anggota as $id_anggota) {
                $max_jadwal = ($id_anggota <= 9) ? 15 : 5;

                for ($i = 0; $i < $faker->numberBetween(0, $max_jadwal); $i++) {
                    $jadwal = new Jadwal();
                    $jadwal->angkatan = \angkatan();
                    $jadwal->id_anggota = $id_anggota;
                    $jadwal->hari = Hari::getRandomValue();
                    $jadwal->waktu_mulai = $faker->dateTimeBetween('05:00', '18:30');
                    $jadwal->save();
                }
            }
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
