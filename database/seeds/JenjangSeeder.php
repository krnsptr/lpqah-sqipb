<?php

use App\Models\Jenjang;
use App\Models\Kelas;
use Illuminate\Database\Seeder;

class JenjangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list_data_kelas = [
            'THS' => [
                [
                    'nama' => 'Pemula Tahsin',
                    'kode' => 'THS--PEM',
                ],
                [
                    'nama' => 'Pra-Tahsin',
                    'kode' => 'THS-0',
                ],
                [
                    'nama' => 'Tahsin 1',
                    'kode' => 'THS-1',
                ],
                [
                    'nama' => 'Tahsin 2',
                    'kode' => 'THS-2',
                ],
            ],
            'THF' => [
                [
                    'nama' => 'Takhossus',
                    'kode' => 'THF-0',
                ],
                [
                    'nama' => 'Tahfiz',
                    'kode' => 'THF-1',
                ],
            ],
            'ARB' => [
                [
                    'nama' => 'Tingkat 1',
                    'kode' => 'ARB-1',
                ],
                [
                    'nama' => 'Tingkat 2',
                    'kode' => 'ARB-2',
                ],
            ],
        ];

        \DB::beginTransaction();

        try {
            foreach ($list_data_kelas as $kode_kelas => $list_data_jenjang) {
                $kelas = Kelas::where('kode', $kode_kelas)->firstOrFail();

                foreach ($list_data_jenjang as $data_jenjang) {
                    $jenjang = new Jenjang();
                    $jenjang->id_kelas = $kelas->id;

                    $jenjang->fill($data_jenjang);
                    $jenjang->save();
                }
            }
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
