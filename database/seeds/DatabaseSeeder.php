<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(\WilayahSeeder::class);
        $this->call(\BankSeeder::class);

        $this->call(\SettingSeeder::class);

        $this->call(\AdminSeeder::class);

        $this->call(\KelasSeeder::class);
        $this->call(\JenjangSeeder::class);

        if (\App::environment(['local', 'testing'])) {
            $this->call(\SetelanSeeder::class);
            $this->call(\AnggotaSeeder::class);
            $this->call(\InstrukturSeeder::class);
            $this->call(\JadwalSeeder::class);
            $this->call(\KelompokSeeder::class);
            $this->call(\SantriSeeder::class);
            $this->call(\BiayaSeeder::class);
        }
    }
}
