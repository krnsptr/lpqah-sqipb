<?php

use App\Models\Anggota;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Models\Santri;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class SantriSeeder extends Seeder
{
    private function makeSantri(Faker $faker, int $id_anggota, Kelas $kelas): Santri
    {
        $jenjang = $kelas->jenjang->random();

        $santri = new Santri();
        $santri->angkatan = \angkatan();
        $santri->id_anggota = $id_anggota;
        $santri->id_kelas = $kelas->id;

        if ($faker->boolean(70)) {
            $list_id_kelompok = Kelompok::query()
                ->where('id_jenjang', $jenjang->id)
                ->where('sisa', '>', 0)
                ->whereHas(
                    'jadwal.anggota.profil',
                    function ($q) use ($santri) {
                        $q->where(
                            'jenis_kelamin',
                            $santri->anggota->profil->jenis_kelamin
                        );
                    }
                )
                ->pluck('id');

            $santri->id_jenjang = $jenjang->id;

            if ($list_id_kelompok->count()) {
                $santri->id_kelompok = $list_id_kelompok->random();
            }
        }

        $detail = [];

        $pendaftaran = $faker->randomElement(array_keys(
            Santri::DETAIL_PENDAFTARAN
        ));

        $detail['pendaftaran'] = $pendaftaran;

        if ($pendaftaran === 'ulang') {
            $angkatan_sebelumnya = \angkatan() - $faker->numberBetween(0, 4);

            $jenjang = $santri->jenjang ?? $jenjang;
            $jenjang_prev = $kelas->jenjang->firstWhere('id', '<', $jenjang->id);

            $lulus_jenjang_sebelumnya = !$jenjang_prev ? false : $faker->boolean();
            $lulus_jenjang_sebelumnya = $lulus_jenjang_sebelumnya ? 'lulus' : 'belum';

            if ($lulus_jenjang_sebelumnya === 'belum') {
                $id_jenjang_sebelumnya = $jenjang->id;
            } else {
                $id_jenjang_sebelumnya = $jenjang_prev->id;
            }

            $detail = array_merge($detail, compact(
                'angkatan_sebelumnya',
                'lulus_jenjang_sebelumnya',
                'id_jenjang_sebelumnya',
            ));
        }

        $santri->setDetail($detail);

        return $santri;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $list_id_anggota = Anggota::where('id', '>', 9)->pluck('id');
        $list_kelas = Kelas::get();

        $list_kelas->load('jenjang');

        \DB::beginTransaction();

        try {
            for ($i = 1; $i <= 9; $i++) {
                foreach ($list_kelas as $kelas) {
                    $santri = $this->makeSantri($faker, $i, $kelas);

                    $santri->save();
                    $santri->initNomorInduk();
                }
            }

            foreach ($list_id_anggota as $id_anggota) {
                if ($faker->boolean(20)) continue;

                $kelas = $list_kelas->random();
                $santri = $this->makeSantri($faker, $id_anggota, $kelas);

                $santri->save();
                $santri->initNomorInduk();
            }
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
