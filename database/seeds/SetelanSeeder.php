<?php

use App\Models\Kelas;
use App\Models\Setelan;
use Illuminate\Database\Seeder;

class SetelanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        try {
            $setelan = new Setelan();
            $setelan->angkatan = \setting('angkatan', \angkatan());

            $syarat_instruktur = Kelas::get()->mapWithKeys(function ($kelas) {
                return [$kelas->id => $kelas->detail->syarat_instruktur];
            });

            $list_jalur = \collect(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'])
                ->map(function ($huruf) {
                    return "LDF $huruf";
                })
                ->toArray();

            $jalur = Kelas::get()->mapWithKeys(function ($kelas) use ($list_jalur) {
                return [$kelas->id => $kelas->nama !== 'Tahsin' ? [] : $list_jalur];
            });

            $setelan->fill([
                'waktu_buka_pendaftaran_instruktur' => \now(),
                'waktu_tutup_pendaftaran_instruktur' => \now()->addDays(7),
                'waktu_buka_pendaftaran_santri' => \now(),
                'waktu_tutup_pendaftaran_santri' => \now()->addDays(7),
                'waktu_buka_penjadwalan_instruktur' => \now(),
                'waktu_tutup_penjadwalan_instruktur' => \now()->addDays(7),
                'waktu_buka_penjadwalan_santri' => \now(),
                'waktu_tutup_penjadwalan_santri' => \now()->addDays(7),
                'waktu_buka_publikasi_jadwal' => \now(),
                'waktu_tutup_publikasi_jadwal' => \now()->addDays(7),
            ]);

            $detail = $setelan->detail;

            foreach (array_keys($detail['pengumuman']) as $slug) {
                $detail['pengumuman'][$slug]['show'] = true;
            }

            $detail['syarat_instruktur'] = $syarat_instruktur;
            $detail['jalur'] = $jalur;

            $setelan->detail = $detail;
            $setelan->save();
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
