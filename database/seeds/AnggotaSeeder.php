<?php

use App\Models\Anggota;
use App\Models\AnggotaProfil;
use Illuminate\Database\Seeder;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        try {
            for ($i = 1; $i <= 9; $i++) {
                $anggota = Anggota::make([
                    'nama' => "Member $i",
                    'email' => $email = "member$i@example.com",
                    'password' => \bcrypt($email),
                    'email_verified_at' => \now(),
                ]);

                $anggota->save();

                \factory(AnggotaProfil::class)->create([
                    'id' => $anggota->id,
                    'username' => "member$i",
                ]);
            }

            \factory(Anggota::class, 991)->create();
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
