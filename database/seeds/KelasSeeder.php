<?php

use App\Models\Kelas;
use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list_data_kelas = [
            [
                'nama' => 'Tahsin',
                'kode' => 'THS',
                'detail' => [
                    'syarat_instruktur' => [
                        'Lulus Tahsin 2',
                        'Lulus Dauroh Syahadah',
                        'Dapat menggunakan teknologi pembelajaran',
                    ],
                ],
            ],
            [
                'nama' => 'Tahfiz',
                'kode' => 'THF',
                'detail' => [
                    'syarat_instruktur' => [
                        'Lulus Tahsin 2',
                        'Lulus Dauroh Syahadah',
                        'Dapat menggunakan teknologi pembelajaran',
                    ],
                ],
            ],
            [
                'nama' => 'Bahasa Arab',
                'kode' => 'ARB',
                'detail' => [
                    'syarat_instruktur' => [
                        'Pernah belajar Bahasa Arab min. 2 tahun',
                        'Punya pengalaman mengajar Bahasa Arab',
                        'Dapat menggunakan teknologi pembelajaran',
                    ],
                ],
            ],
        ];

        \DB::beginTransaction();

        try {
            foreach ($list_data_kelas as $data_kelas) {
                $kelas = new Kelas();

                $kelas->fill($data_kelas);
                $kelas->save();
            }
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
