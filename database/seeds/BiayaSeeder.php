<?php

use App\Models\Biaya;
use App\Models\Santri;
use App\Models\Setelan;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class BiayaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker, Setelan $setelan)
    {
        $list_id_santri = Santri::whereNotNull('id_jenjang')->pluck('id');

        \DB::beginTransaction();

        try {
            foreach ($list_id_santri as $id_santri) {
                $status = \collect(Biaya::STATUS)->keys()->random();
                $nominal = $setelan->detail['biaya']['nominal'];

                if ($faker->boolean(10)) {
                    $nominal = $faker->numberBetween(0, $nominal / 10_000) * 10_000;
                }

                if ($status === Biaya::STATUS_SUDAH_DIBAYAR) {
                    $keterangan = $faker->randomElement(
                        explode(',', $setelan->detail['biaya']['keterangan'])
                    );
                } else {
                    $keterangan = null;
                }

                $biaya = new Biaya();
                $biaya->id_santri = $id_santri;
                $biaya->status = $status;
                $biaya->nominal = $nominal;
                $biaya->keterangan = $keterangan;

                $biaya->save();
            }
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
