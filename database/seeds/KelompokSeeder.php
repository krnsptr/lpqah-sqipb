<?php

use App\Models\Instruktur;
use App\Models\Jadwal;
use App\Models\Kelompok;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class KelompokSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $list_id_instruktur = Instruktur::whereNotNull('id_jenjang')->pluck('id');
        $group_jalur = \app('setelan')->detail['jalur'];

        \DB::beginTransaction();

        try {
            foreach ($list_id_instruktur as $id_instruktur) {
                $instruktur = Instruktur::find($id_instruktur);

                $list_id_jadwal = $instruktur
                ->anggota
                ->jadwal()
                ->whereDoesntHave('kelompok')
                ->pluck('id');

                $count_jadwal = $list_id_jadwal->count();

                if ($count_jadwal < 1) continue;

                $kapasitas_membina = $faker->numberBetween(1, $count_jadwal);

                if ($kapasitas_membina > 3) {
                    $kapasitas_membina = ($kapasitas_membina % 3) + 1;
                }

                $instruktur->setDetail(array_merge((array) $instruktur->detail, [
                    'kapasitas_membina' => $kapasitas_membina,
                ]));

                $instruktur->save();

                $list_id_jenjang = $instruktur
                ->kelas
                ->jenjang()
                ->where('id', '<=', $instruktur->id_jenjang)
                ->pluck('id');

                foreach ($list_id_jadwal->random($kapasitas_membina) as $id_jadwal) {
                    $kelompok = new Kelompok();
                    $kelompok->id_instruktur = $instruktur->id;
                    $kelompok->id_jenjang = $list_id_jenjang->random();
                    $kelompok->id_jadwal = $id_jadwal;
                    $kelompok->save();

                    $list_jalur = $group_jalur[$instruktur->id_kelas];

                    if ($faker->boolean() && count($list_jalur)) {
                        $jalur = $faker->randomElement($list_jalur);
                        Jadwal::where('id', $id_jadwal)->update(['jalur' => $jalur]);
                    }
                }
            }
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
