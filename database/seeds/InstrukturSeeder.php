<?php

use App\Models\Anggota;
use App\Models\Instruktur;
use App\Models\Kelas;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class InstrukturSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $list_id_anggota = Anggota::where('id', '>', 9)->pluck('id');
        $list_kelas = Kelas::with('jenjang')->get();

        \DB::beginTransaction();

        try {
            for ($i = 1; $i <= 9; $i++) {
                foreach ($list_kelas as $kelas) {
                    $jenjang = $kelas->jenjang->random();

                    $instruktur = new Instruktur();
                    $instruktur->angkatan = \angkatan();
                    $instruktur->id_anggota = $i;
                    $instruktur->id_kelas = $kelas->id;

                    if ($faker->boolean(70)) {
                        $instruktur->id_jenjang = $jenjang->id;
                    }

                    $syarat_instruktur = $kelas->detail->syarat_instruktur;
                    $count_syarat = count($syarat_instruktur);

                    $instruktur->setDetail([
                        'pendaftaran' => $faker->randomElement(array_keys(
                            Instruktur::DETAIL_PENDAFTARAN
                        )),
                        'syarat_instruktur' => $faker->randomElements(
                            $syarat_instruktur,
                            $count_syarat - $faker->boolean(),
                        ),
                    ]);

                    $instruktur->save();
                    $instruktur->initNomorInduk();
                }
            }

            foreach ($list_id_anggota as $id_anggota) {
                if ($faker->boolean(80)) continue;

                $kelas = $list_kelas->random();
                $jenjang = $kelas->jenjang->random();

                $instruktur = new Instruktur();
                $instruktur->angkatan = \angkatan();
                $instruktur->id_anggota = $id_anggota;
                $instruktur->id_kelas = $kelas->id;

                if ($faker->boolean(70)) {
                    $instruktur->id_jenjang = $jenjang->id;
                }

                $syarat_instruktur = $kelas->detail->syarat_instruktur;
                $count_syarat = count($syarat_instruktur);

                $instruktur->setDetail([
                    'pendaftaran' => $faker->randomElement(array_keys(
                        Instruktur::DETAIL_PENDAFTARAN
                    )),
                    'syarat_instruktur' => $faker->randomElements(
                        $syarat_instruktur,
                        $count_syarat - $faker->boolean(),
                    ),
                ]);

                $instruktur->save();
                $instruktur->initNomorInduk();
            }
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
