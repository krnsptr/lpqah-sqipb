<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        $sql_names = [
            'bank',
        ];

        foreach ($sql_names as $sql_name) {
            $this->command->info("Importing: $sql_name.sql");
            $sql = "database/seeds/sql/$sql_name.sql";
            \DB::unprepared(file_get_contents($sql));
        }

        \DB::commit();
    }
}
