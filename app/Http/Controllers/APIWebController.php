<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Jenjang;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Models\Wilayah\Kota;
use App\Models\Wilayah\Provinsi;
use Illuminate\Http\Request;

class APIWebController extends Controller
{
    public function provinsi()
    {
        $data = Provinsi::select('id', 'nama AS text')
        ->orderBy('nama')
        ->get();

        return \response()->json($data);
    }

    public function kota(Request $request)
    {
        $data = Kota::select('id', 'nama AS text')
        ->where('id_provinsi', $request->input('id_provinsi'))
        ->orderBy('nama')
        ->get();

        return \response()->json($data);
    }

    public function bank()
    {
        $data = Bank::select('id', 'nama AS text')
        ->get();

        return \response()->json($data);
    }

    public function kelas()
    {
        $data = Kelas::select('id', 'nama AS text')
        ->get();

        return \response()->json($data);
    }

    public function jenjang(Request $request)
    {
        $data = Jenjang::select('id', 'nama AS text')
        ->where('id_kelas', $request->input('id_kelas'))
        ->get();

        return \response()->json($data);
    }

    public function kelompok(Request $request)
    {
        $data = Kelompok::angkatanNow()
        ->with('jenjang:id,nama')
        ->with('jadwal.anggota:id,nama')
        ->where('id_jenjang', $request->input('id_jenjang'))
        ->whereHas(
            'jadwal.anggota.profil',
            function ($q) use ($request) {
                $q->where('jenis_kelamin', $request->input('jenis_kelamin'));
            }
        )
        ->get()
        ->map(function (Kelompok $kelompok) {
            $text = "{$kelompok->nomor_reg_lengkap} ";
            $text .= "| {$kelompok->jenjang->nama} ";
            $text .= "| {$kelompok->jadwal->jalur_text} ";
            $text .= "| {$kelompok->jadwal->getText()} ";
            $text .= "- {$kelompok->jadwal->anggota->nama} ";
            $text .= "(sisa {$kelompok->sisa})";

            return [
                'id' => $kelompok->id,
                'text' => $text,
            ];
        });

        return \response()->json($data);
    }
}
