<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Anggota;
use App\Models\AnggotaProfil;
use App\Models\Instruktur;
use App\Models\Jadwal;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Models\Santri;
use App\Models\Setelan;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dasbor()
    {
        $jumlah = [
            'pengguna' => Anggota::getCount(),
            'belum_daftar' => Anggota::getCount('belum_daftar'),
            'instruktur' => Anggota::getCount('instruktur'),
            'santri' => Anggota::getCount('santri'),
            'calon_instruktur' => Anggota::getCount('calon_instruktur'),
            'calon_santri' => Anggota::getCount('calon_santri'),
        ];

        SEOTools::setTitle('Admin | Dasbor');
        SEOTools::setCanonical(\route('admin.dasbor'));

        return \view('admin-dasbor', compact(
            'jumlah',
        ));
    }

    public function statistik()
    {
        $list_kelas = Kelas::with('jenjang')->withCount('jenjang')->get();

        SEOTools::setTitle('Admin | Statistik');
        SEOTools::setCanonical(\route('admin.statistik'));

        return \view('admin-statistik', compact(
            'list_kelas',
        ));
    }

    public function instruktur(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Instruktur::angkatanNow()->select([
                'instruktur.id',
                'instruktur.angkatan',
                'instruktur.id_anggota',
                'instruktur.id_kelas',
                'instruktur.id_jenjang',
                'instruktur.nomor',
                // 'instruktur.nomor_induk',
                // 'instruktur.detail',
                // 'instruktur.created_at',
                // 'instruktur.updated_at',
            ])->with([
                'anggota:id,nama',
                'anggota.profil:id,jenis_kelamin,nomor_identitas',
                'kelas:id,nama',
                'jenjang:id,nama',
            ]);

            $dt = DataTables::eloquent($q)
            ->filterColumn('jenjang.nama', function ($query, $keyword) {
                if ($keyword === '-') {
                    $query->whereDoesntHave('jenjang');
                } else {
                    $query->whereHas('jenjang', function ($q) use ($keyword) {
                        $q->where('nama', $keyword);
                    });
                }
            });

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | Instruktur");
        SEOTools::setCanonical(\route('admin.instruktur'));

        return \view('admin-instruktur');
    }

    public function instruktur_edit(Request $request, Instruktur $instruktur)
    {
        if ($request->isMethod('post')) {
            $instruktur->id_jenjang = $request->input('id_jenjang');

            $instruktur->save();

            if ($instruktur->id_jenjang && !$instruktur->nomor_induk) {
                $instruktur->initNomorInduk();
            }

            return \redirect()->back()->withSuccess('Instruktur berhasil disimpan.');
        }

        SEOTools::setTitle("Admin | Edit Instruktur: {$instruktur->nomor_reg_lengkap}");
        SEOTools::setCanonical(\route('admin.instruktur_edit', $instruktur));

        return \view('admin-instruktur-edit', compact(
            'instruktur',
        ));
    }

    public function santri(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Santri::angkatanNow()->select([
                'santri.id',
                'santri.angkatan',
                'santri.id_anggota',
                'santri.id_kelas',
                'santri.id_jenjang',
                'santri.id_kelompok',
                'santri.nomor',
                // 'santri.nomor_induk',
                // 'santri.detail',
                // 'santri.created_at',
                // 'santri.updated_at',
            ])->with([
                'anggota:id,nama',
                'anggota.profil:id,jenis_kelamin,nomor_identitas',
                'kelas:id,nama',
                'jenjang:id,nama',
            ]);

            $dt = DataTables::eloquent($q)
            ->filterColumn('jenjang.nama', function ($query, $keyword) {
                if ($keyword === '-') {
                    $query->whereDoesntHave('jenjang');
                } else {
                    $query->whereHas('jenjang', function ($q) use ($keyword) {
                        $q->where('nama', $keyword);
                    });
                }
            });

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | Santri");
        SEOTools::setCanonical(\route('admin.santri'));

        return \view('admin-santri');
    }

    public function santri_edit(Request $request, Santri $santri)
    {
        if ($request->isMethod('post')) {
            $santri->id_kelas = $request->input('id_kelas');
            $santri->id_jenjang = $request->input('id_jenjang');
            $santri->id_kelompok = $request->input('id_kelompok');

            $santri->save();

            if ($santri->id_jenjang && !$santri->nomor_induk) {
                $santri->initNomorInduk();
            }

            return \redirect()->back()->withSuccess('Santri berhasil disimpan.');
        }

        SEOTools::setTitle("Admin | Edit Santri: {$santri->nomor_reg_lengkap}");
        SEOTools::setCanonical(\route('admin.santri_edit', $santri));

        return \view('admin-santri-edit', compact(
            'santri',
        ));
    }

    public function penjadwalan(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Anggota::select([
                'anggota.id',
                'anggota.nama',
                'anggota.email',
                'anggota.created_at',
            ])->with([
                'profil:id,jenis_kelamin,status,nomor_identitas',
            ])->whereHas('instruktur', function ($q) {
                $q->whereNotNull('id_jenjang');
            });

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | Penjadwalan");
        SEOTools::setCanonical(\route('admin.penjadwalan'));

        return \view('admin-penjadwalan');
    }

    public function penjadwalan_edit(Request $request, Setelan $setelan)
    {
        $jadwal = Jadwal::findOrFail($request->input('id_jadwal'));

        if ($request->input('aksi') === 'tambah_kelompok') {
            $instruktur = Instruktur::findOrFail($request->input('id_instruktur'));

            $instruktur->load('anggota.profil');
            $jenis_kelamin = $instruktur->anggota->profil->jenis_kelamin;

            $kelompok = new Kelompok();
            $kelompok->id_jadwal = $jadwal->id;
            $kelompok->id_instruktur = $instruktur->id;
            $kelompok->id_jenjang = $request->input('id_jenjang');
            $kelompok->kuota = $setelan->getKuotaKelompok($jenis_kelamin);
            $kelompok->sisa = $setelan->getKuotaKelompok($jenis_kelamin);

            $kelompok->save();

            return \redirect()->back()->withSuccess('Kelompok berhasil ditambahkan.');
        }

        if ($request->input('aksi') === 'hapus_kelompok') {
            $kelompok = $jadwal->kelompok;

            if (!$kelompok) {
                return \redirect()->back();
            }

            $kelompok->delete();

            return \redirect()->back()->withSuccess('Kelompok berhasil dihapus.');
        }
    }

    public function biaya(Request $request, Setelan $setelan)
    {
        if ($request->wantsJson()) {
            $q = Santri::angkatanNow()
            ->select([
                'santri.id',
                'santri.angkatan',
                'santri.id_anggota',
                'santri.id_kelas',
                'santri.id_jenjang',
                // 'santri.id_kelompok',
                'santri.nomor',
                // 'santri.nomor_induk',
                // 'santri.detail',
                // 'santri.created_at',
                // 'santri.updated_at',
            ])->with([
                'anggota:id,nama',
                'anggota.profil:id,jenis_kelamin',
                'kelas:id,nama',
                'jenjang:id,nama',
                'biaya',
            ])->where(function ($q) {
                $q->whereNotNull('id_kelompok')->orWhereHas('biaya');
            });

            $dt = DataTables::eloquent($q)
            ->filterColumn('jenjang.nama', function ($query, $keyword) {
                if ($keyword === '-') {
                    $query->whereDoesntHave('jenjang');
                } else {
                    $query->whereHas('jenjang', function ($q) use ($keyword) {
                        $q->where('nama', $keyword);
                    });
                }
            });

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | {$setelan->detail['biaya']['label']}");
        SEOTools::setCanonical(\route('admin.biaya'));

        return \view('admin-biaya');
    }

    public function biaya_edit(Request $request, Santri $santri)
    {
        $biaya = $santri->biaya;

        if ($request->isMethod('post')) {
            $biaya->nominal = $request->input('nominal');
            $biaya->status = $request->input('status');
            $biaya->keterangan = $request->input('keterangan');

            $biaya->save();

            return \redirect()->back()->withSuccess('Biaya berhasil disimpan.');
        }

        SEOTools::setTitle("Admin | Edit Biaya: {$santri->nomor_reg_lengkap}");
        SEOTools::setCanonical(\route('admin.biaya_edit', $santri));

        return \view('admin-biaya-edit', compact(
            'santri',
        ));
    }

    public function anggota(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Anggota::select([
                'anggota.id',
                'anggota.nama',
                'anggota.email',
                'anggota.created_at',
            ])->with([
                'profil:id,jenis_kelamin,status,nomor_identitas',
            ]);

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | Anggota");
        SEOTools::setCanonical(\route('admin.anggota'));

        return \view('admin-anggota');
    }

    public function anggota_lihat(Request $request, Anggota $anggota)
    {
        $profil = $anggota->profil ?? new AnggotaProfil();

        if ($request->isMethod('post')) {
            if ($request->input('aksi') === 'reset_password') {
                $anggota->email = $request->input('email');
                $anggota->password = \bcrypt($request->input('password'));
                $anggota->save();

                return \redirect()->back()->withSuccess(
                    'Password berhasil direset.'
                );
            }
        }

        SEOTools::setTitle("Admin | Anggota: {$anggota->nama}");
        SEOTools::setCanonical(\route('admin.anggota_lihat', $anggota));

        return \view('admin-anggota-lihat', compact(
            'anggota',
            'profil',
        ));
    }

    public function anggota_masuk(Request $request, Anggota $anggota)
    {
        /** @var \Illuminate\Contracts\Auth\StatefulGuard $guard */

        $guard = \Auth::guard('anggota');
        $guard->login($anggota);

        if ($redirect = $request->input('redirect')) {
            return \redirect($redirect);
        }

        return \redirect()->route('anggota.dasbor');
    }

    public function unduh(Request $request)
    {
        if ($unduh = $request->input('unduh')) {
            if (method_exists($this, "unduh_$unduh")) {
                return $this->{"unduh_$unduh"}();
            };

            return \abort(404);
        }

        SEOTools::setTitle("Admin | Unduh Data");
        SEOTools::setCanonical(\route('admin.unduh'));

        return \view('admin-unduh');
    }

    public function unduh_instruktur()
    {
        $setelan = \app('setelan');
        $app_code = \config('app.code');
        $angkatan = $setelan->angkatan;
        $date = date('Y-m-d H-i-s');

        set_time_limit(0);

        return Excel::download(
            new \App\Exports\InstrukturExport(),
            "Instruktur _ $angkatan _ $app_code _ $date.xlsx"
        );
    }

    public function unduh_kelompok()
    {
        $setelan = \app('setelan');
        $app_code = \config('app.code');
        $angkatan = $setelan->angkatan;
        $date = date('Y-m-d H-i-s');

        set_time_limit(0);

        return Excel::download(
            new \App\Exports\KelompokExport(),
            "Santri _ $angkatan _ $app_code _ $date.xlsx"
        );
    }

    public function unduh_santri()
    {
        $setelan = \app('setelan');
        $app_code = \config('app.code');
        $angkatan = $setelan->angkatan;
        $date = date('Y-m-d H-i-s');

        set_time_limit(0);

        return Excel::download(
            new \App\Exports\SantriExport(),
            "Santri _ $angkatan _ $app_code _ $date.xlsx"
        );
    }

    public function unduh_biaya()
    {
        $setelan = \app('setelan');
        $app_code = \config('app.code');
        $angkatan = $setelan->angkatan;
        $date = date('Y-m-d H-i-s');

        $label = $setelan->detail['biaya']['label'];

        set_time_limit(0);

        return Excel::download(
            new \App\Exports\BiayaExport(),
            "$label _ $angkatan _ $app_code _ $date.xlsx"
        );
    }

    public function setelan(Request $request, Setelan $setelan)
    {
        if ($request->isMethod('post')) {
            $angkatan_min = Setelan::orderBy('angkatan')->value('angkatan') ?? \angkatan();
            $angkatan_max = \angkatan() + 1;

            $request->validate([
                'angkatan' => "required|integer|min:$angkatan_min|max:$angkatan_max",
            ]);

            $angkatan = (int) $request->input('angkatan');

            if ($request->input('aksi') === 'pindah_semester') {
                \DB::beginTransaction();

                $setelan = Setelan::angkatan($angkatan)->first();
                $setelan_prev = Setelan::angkatan($angkatan - 1)->first();

                if (!$setelan) {
                    if ($setelan_prev) {
                        $setelan = (clone $setelan_prev);
                        $setelan->id = null;
                        $setelan->exists = false;
                    } else {
                        $setelan = new Setelan();
                    }

                    $setelan->angkatan = $angkatan;
                    $setelan->save();
                }

                \setting(['angkatan' => $angkatan]);
                \setting()->save();

                \DB::commit();

                return \redirect()->back()->withSuccess('Perpindahan semester berhasil.');
            }

            if ($setelan->angkatan !== $angkatan) {
                return \redirect()->back();
            }

            $request->validate([
                'detail' => 'required|array',
                'detail.syarat_instruktur' => 'required|array',
                'detail.syarat_instruktur.*' => 'required|string',
                'detail.pengumuman' => 'required|array',
            ]);

            $setelan->fill($request->all());
            $detail = $request->input('detail');

            foreach (Kelas::pluck('id') as $id_kelas) {
                $text_syarat = $request->input("detail.syarat_instruktur.$id_kelas");
                $array_syarat = explode("\r\n", $text_syarat);

                $list_syarat = \collect();

                foreach ($array_syarat as $syarat) {
                    if (trim($syarat)) {
                        $list_syarat->push($syarat);
                    }
                }

                $detail['syarat_instruktur'][$id_kelas] = $list_syarat;

                $text_jalur = $request->input("detail.jalur.$id_kelas");
                $array_jalur = explode("\r\n", $text_jalur);

                $list_jalur = \collect();

                foreach ($array_jalur as $jalur) {
                    if (trim($jalur)) {
                        $list_jalur->push($jalur);
                    }
                }

                $detail['jalur'][$id_kelas] = $list_jalur;
            }

            foreach (array_keys($detail['pengumuman']) as $p) {
                $detail['pengumuman'][$p]['show'] = $request->boolean(
                    "detail.pengumuman.$p.show"
                );
            }

            $setelan->detail = $detail;
            $setelan->save();

            return \redirect()->back()->withSuccess('Setelan berhasil disimpan.');
        }

        SEOTools::setTitle("Admin | Setelan");
        SEOTools::setCanonical(\route('admin.setelan'));

        return \view('admin-setelan', compact(
            'setelan',
        ));
    }
}
