<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Anggota;
use App\Models\AnggotaProfil;
use App\Models\Instruktur;
use App\Models\Jadwal;
use App\Models\Jenjang;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Models\Santri;
use App\Models\Setelan;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Http\Request;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:anggota');
        $this->middleware('profile.completed')->except('dasbor', 'profil');
    }

    public function dasbor()
    {
        SEOTools::setTitle('Dasbor');
        SEOTools::setCanonical(\route('anggota.dasbor'));

        return \view('anggota-dasbor');
    }

    public function profil(Request $request, Setelan $setelan)
    {
        /** @var Anggota $anggota **/

        $anggota = \auth('anggota')->user();
        $profil = $anggota->profil ?? new AnggotaProfil();

        if ($request->isMethod('post')) {
            if (!$profil->exists || \admin()) {
                $anggota->nama = $request->input('nama');
                $profil->jenis_kelamin = $request->input('jenis_kelamin');
            }

            if (!$anggota->email_verified_at || \admin()) {
                $anggota->email = $request->input('email');
            }

            $profil->id = $anggota->id;
            $profil->id_bank = $request->input('id_bank');
            $profil->id_kota = $request->input('id_kota');
            $profil->fill($request->all());

            \DB::beginTransaction();

            if ($profil->isClean() && $anggota->isClean()) {
                $profil->touch();
            } else {
                $profil->save();
                $anggota->save();
            }

            \DB::commit();

            if ($profil->wasRecentlyCreated) {
                if ($setelan->isDibukaPendaftaran()) {
                    $route = 'anggota.program_daftar';
                } else {
                    $route = 'anggota.dasbor';
                }
            } else {
                $route = 'anggota.profil';
            }

            return \redirect()->route($route)->withSuccess('Profil berhasil disimpan');
        }

        if ($profil->updated_at && $profil->updated_at < \now()->subMonth(6)) {
            $message = 'Pastikan profil kamu lengkap dan up-to-date. Jika sudah, klik Simpan.';
            \session()->now('info', $message);
        }

        SEOTools::setTitle('Edit Profil');
        SEOTools::setCanonical(\route('anggota.profil'));

        return \view('anggota-profil', compact(
            'anggota',
            'profil',
        ));
    }

    public function program_daftar(Request $request, Setelan $setelan)
    {
        if (!$setelan->isDibukaPendaftaran()) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Pendaftaran program sedang ditutup.'
            );
        }

        if ($request->isMethod('post')) {
            $request->validate([
                'sebagai' => 'required|in:santri,instruktur',
                'id_kelas' => 'required|exists:kelas,id',
            ]);

            $sebagai = $request->input('sebagai');
            $id_kelas = $request->input('id_kelas');

            $route = "anggota.{$sebagai}_daftar";
            $url = \route($route, ['kelas' => $id_kelas]);

            return \redirect($url);
        }

        SEOTools::setTitle("Pendaftaran Program");

        return \view('anggota-program-daftar');
    }

    public function program_penjadwalan(Setelan $setelan)
    {
        if (!$setelan->isDibukaPenjadwalan()) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Penjadwalan program sedang ditutup.'
            );
        }

        $list_instruktur = \anggota()->instruktur;
        $list_jadwal = \anggota()->jadwal;
        $list_santri = \anggota()->santri;
        $kuota = $setelan->getKuotaKelompok(\anggota()->profil->jenis_kelamin);

        SEOTools::setTitle("Penjadwalan Program");

        return \view('anggota-program-penjadwalan', compact(
            'list_instruktur',
            'list_jadwal',
            'list_santri',
            'kuota',
        ));
    }

    public function instruktur_daftar(Request $request, Setelan $setelan, Kelas $kelas)
    {
        if (!$setelan->isDibuka('pendaftaran_instruktur')) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Pendaftaran instruktur sedang ditutup.'
            );
        }

        if (!$kelas->id) {
            return \redirect()->route('anggota.program_daftar', [
                'sebagai' => 'instruktur',
            ]);
        }

        if ($request->isMethod('post')) {
            $id_kelas = $kelas->id;

            $instruktur = \anggota()->instruktur()
            ->where('id_kelas', $id_kelas)
            ->firstOrNew([]);

            if (!$instruktur->angkatan) {
                $instruktur->angkatan = \setting('angkatan', \angkatan());
            }

            $instruktur->id_kelas = $id_kelas;
            $instruktur->setDetail($request->input('detail'));

            $instruktur->save();

            return \redirect()->route('anggota.dasbor')->withSuccess(
                'Pendaftaran berhasil.'
            );
        }

        SEOTools::setTitle("Pendaftaran Instruktur");

        return \view('anggota-instruktur-daftar', compact(
            'kelas',
        ));
    }

    public function instruktur_lihat(Instruktur $instruktur)
    {
        if ($instruktur->id_anggota !== \anggota()->id) \abort(404);

        SEOTools::setTitle("Program: {$instruktur->nomor_reg_lengkap}");

        return \view('anggota-instruktur-lihat', compact(
            'instruktur',
        ));
    }

    public function instruktur_penjadwalan(Request $request, Setelan $setelan)
    {
        if (!$setelan->isDibuka('penjadwalan_instruktur')) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Penjadwalan instruktur sedang ditutup.'
            );
        }

        $list_kapasitas_membina = (array) $request->input('kapasitas_membina');

        \DB::beginTransaction();

        foreach ($list_kapasitas_membina as $id_instruktur => $kapasitas_membina) {
            $instruktur = Instruktur::findOrFail($id_instruktur);

            if ($instruktur->id_anggota === \anggota()->id) {
                $instruktur->setDetail(array_merge((array) $instruktur->detail, [
                    'kapasitas_membina' => $kapasitas_membina,
                ]));
            }

            $instruktur->save();
        }

        \DB::commit();

        return \redirect()->back()->withSuccess('Data berhasil disimpan.');
    }

    public function santri_daftar(Request $request, Setelan $setelan, Kelas $kelas)
    {
        if (!$setelan->isDibuka('pendaftaran_santri')) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Pendaftaran santri sedang ditutup.'
            );
        }

        if (!$kelas->id) {
            return \redirect()->route('anggota.program_daftar', [
                'sebagai' => 'santri',
            ]);
        }

        if ($request->isMethod('post')) {
            $id_kelas = $kelas->id;

            $santri = \anggota()->santri()
            ->where('id_kelas', $id_kelas)
            ->firstOrNew([]);

            if (!$santri->angkatan) {
                $santri->angkatan = \setting('angkatan', \angkatan());
            }

            $santri->id_kelas = $id_kelas;
            $santri->setDetail($request->input('detail'));

            $santri->save();

            return \redirect()->route('anggota.dasbor')->withSuccess(
                'Pendaftaran berhasil.'
            );
        }

        SEOTools::setTitle("Pendaftaran Santri");

        return \view('anggota-santri-daftar', compact(
            'kelas',
        ));
    }

    public function santri_lihat(Santri $santri)
    {
        if ($santri->id_anggota !== \anggota()->id) \abort(404);

        SEOTools::setTitle("Program: {$santri->nomor_reg_lengkap}");

        return \view('anggota-santri-lihat', compact(
            'santri',
        ));
    }

    public function santri_penjadwalan(Request $request, Setelan $setelan, Santri $santri)
    {
        if (!$setelan->isDibuka('penjadwalan_santri')) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Penjadwalan santri sedang ditutup.'
            );
        }

        if ($santri->id_anggota !== \anggota()->id) \abort(404);

        if ($request->isMethod('post')) {
            $santri->id_kelompok = $request->input('id_kelompok');
            $santri->save();

            return \redirect()->back()->withSuccess(
                "Kelompok berhasil dipilih."
            );
        }

        $list_kelompok = Kelompok::angkatanNow()
        ->with('jadwal')
        ->where('id_jenjang', $santri->id_jenjang)
        ->whereHas(
            'jadwal.anggota.profil',
            function ($q) use ($santri) {
                $q->where(
                    'jenis_kelamin',
                    $santri->anggota->profil->jenis_kelamin
                );
            }
        )
        ->whereHas('jadwal', function ($q) use ($request) {
            $q->where('jalur', $request->input('jalur'));
        })
        ->orderBy('sisa')
        ->get()
        ->map(function (Kelompok $kelompok) {
            $text = "{$kelompok->nomor_reg_lengkap} ";
            $text .= "{$kelompok->jadwal->getText()} ";
            $text .= "(sisa {$kelompok->sisa})";

            return [
                'id' => $kelompok->id,
                'text' => $text,
            ];
        })
        ->pipe(function ($collection) {
            $collection->prepend(['id' => '', 'text' => '(Belum dipilih)']);

            return $collection;
        });

        return \response()->json($list_kelompok);
    }

    public function jadwal_tambah(Request $request, Setelan $setelan)
    {
        if (!$setelan->isDibuka('penjadwalan_instruktur')) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Penjadwalan instruktur sedang ditutup.'
            );
        }

        if ($request->isMethod('post')) {
            $jadwal = new Jadwal();
            $jadwal->angkatan = \setting('angkatan', \angkatan());
            $jadwal->id_anggota = \anggota()->id;
            $jadwal->hari = $request->input('hari');
            $jadwal->waktu_mulai = $request->input('waktu_mulai');
            $jadwal->jalur = $request->input('jalur');
            $jadwal->save();

            return \redirect()->route('anggota.program_penjadwalan')->withSuccess(
                "Jadwal berhasil disimpan: {$jadwal->getText()}."
            );
        }

        $list_jalur = \collect($setelan->detail['jalur'])->flatten(1)->unique();

        SEOTools::setTitle("Tambah Jadwal");

        return \view('anggota-jadwal-tambah', compact(
            'list_jalur',
        ));
    }

    public function jadwal_edit(Request $request, Setelan $setelan, Jadwal $jadwal)
    {
        if (!$setelan->isDibuka('penjadwalan_instruktur')) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Penjadwalan instruktur sedang ditutup.'
            );
        }

        if (!\anggota()->jadwal()->find($jadwal->id)) \abort(404);

        if ($request->isMethod('post')) {
            $jadwal->hari = $request->input('hari');
            $jadwal->waktu_mulai = $request->input('waktu_mulai');
            $jadwal->jalur = $request->input('jalur');
            $jadwal->save();

            return \redirect()->route('anggota.program_penjadwalan')->withSuccess(
                "Jadwal berhasil disimpan: {$jadwal->getText()}."
            );
        }

        $list_jalur = \collect($setelan->detail['jalur'])->flatten(1)->unique();

        SEOTools::setTitle("Edit Jadwal");

        return \view('anggota-jadwal-edit', compact(
            'jadwal',
            'list_jalur',
        ));
    }

    public function jadwal_hapus(Request $request, Setelan $setelan, Jadwal $jadwal)
    {
        if (!$setelan->isDibuka('penjadwalan_instruktur')) {
            return \redirect()->route('anggota.dasbor')->withError(
                'Penjadwalan instruktur sedang ditutup.'
            );
        }

        if (!\anggota()->jadwal()->find($jadwal->id)) \abort(404);

        if ($request->isMethod('post')) {
            $jadwal->delete();

            return \redirect()->route('anggota.program_penjadwalan')->withSuccess(
                "Jadwal berhasil dihapus: {$jadwal->getText()}."
            );
        }

        SEOTools::setTitle("Hapus Jadwal");

        return \view('anggota-jadwal-hapus', compact(
            'jadwal',
        ));
    }

    public function biaya(Setelan $setelan)
    {
        $list_santri = \anggota()->santri()->whereNotNull('id_jenjang')->get();

        $list_santri->load('kelas');
        $list_santri->load('biaya');

        SEOTools::setTitle($setelan->detail['biaya']['label']);

        return \view('anggota-biaya', compact(
            'list_santri',
        ));
    }

    public function jadwal_cari(Request $request)
    {
        $id_jenjang = $request->input('id_jenjang');
        $hari = $request->input('hari');
        $jenis_kelamin = \anggota()->profil->jenis_kelamin;

        $list_kelompok = Kelompok::angkatanNow()
        ->where('id_jenjang', $id_jenjang)
        ->whereHas('jadwal', function ($q) use ($hari) {
            if ($hari) $q->where('hari', $hari);
        })
        ->whereHas('jadwal.anggota.profil', function ($q) use ($jenis_kelamin) {
            $q->where('jenis_kelamin', $jenis_kelamin);
        })
        ->get();

        if ($list_kelompok->count()) {
            $list_kelompok->load([
                'jadwal.anggota.profil:id,jenis_kelamin,nomor_wa,nomor_hp',
                'jenjang.kelas',
            ]);

            $list_kelompok->loadCount('santri');

            $list_kelompok = $list_kelompok->sortBy(function (Kelompok $kelompok) {
                $jadwal = $kelompok->jadwal;

                return "{$jadwal->hari->value}{$jadwal->waktu_mulai->timestamp}";
            });
        }

        $jenjang = Jenjang::find($id_jenjang);

        SEOTools::setTitle('Cari Jadwal');

        return \view('anggota-jadwal-cari', compact(
            'jenjang',
            'list_kelompok',
        ));
    }

    public function kelompok()
    {
        $list_instruktur = \anggota()->instruktur;
        $list_santri = \anggota()->santri;

        $list_instruktur->load([
            'kelompok.jadwal.anggota.profil:id,jenis_kelamin,nomor_wa,nomor_hp',
            'kelompok.jenjang.kelas',
            'kelompok.santri.anggota.profil:id,jenis_kelamin,nomor_wa,nomor_hp',
        ]);

        $list_santri->load([
            'kelompok.jadwal.anggota.profil:id,jenis_kelamin,nomor_wa,nomor_hp',
            'kelompok.jenjang.kelas',
            'kelompok.santri.anggota.profil:id,jenis_kelamin,nomor_wa,nomor_hp',
        ]);

        SEOTools::setTitle('Kelompok');

        return \view('anggota-kelompok', compact(
            'list_instruktur',
            'list_santri',
        ));
    }
}
