<?php

namespace App\Http\Middleware;

use App\Exceptions\EmailThrottleException;
use Illuminate\Routing\Middleware\ThrottleRequests;

class EmailThrottleRequests extends ThrottleRequests
{
    /**
     * Create a 'too many attempts' exception.
     *
     * @param  string  $key
     * @param  int  $maxAttempts
     * @return \Illuminate\Http\Exceptions\ThrottleRequestsException
     */
    protected function buildException($key, $maxAttempts)
    {
        $retryAfter = $this->getTimeUntilNextRetry($key);

        $headers = $this->getHeaders(
            $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );

        return new EmailThrottleException('Too Many Attempts.',null, $headers);
    }
}
