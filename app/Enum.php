<?php

namespace App;

use BenSampo\Enum\Enum as BaseEnum;

class Enum extends BaseEnum
{
    public const DETAIL = [];
    public array $detail = [];

    /**
     * Construct an Enum instance.
     *
     * @param  mixed  $enumValue
     * @return void
     */
    public function __construct($enumValue)
    {
        parent::__construct($enumValue);

        $this->detail = static::getDetail($enumValue);
    }

    /**
     * Get all of the constants defined on the class.
     *
     * @return array
     */
    protected static function getConstants(): array
    {
        $calledClass = get_called_class();

        if (!array_key_exists($calledClass, static::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            $constants = $reflect->getConstants();

            unset($constants['DETAIL']);

            static::$constCacheArray[$calledClass] = $constants;
        }

        return static::$constCacheArray[$calledClass];
    }

    /**
     * Get the description for an enum value
     *
     * @param  mixed  $value
     * @return string
     */
    public static function getDescription($value): string
    {
        if (isset(static::DETAIL[$value]['text'])) {
            return static::DETAIL[$value]['text'];
        }

        return parent::getDescription($value);
    }

    /**
     * Get the detail for an enum value
     *
     * @param  mixed  $value
     * @return string
     */
    public static function getDetail($value): array
    {
        if (isset(static::DETAIL[$value])) {
            return static::DETAIL[$value];
        }

        return [];
    }
}
