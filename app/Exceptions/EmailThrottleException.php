<?php

namespace App\Exceptions;

use Illuminate\Http\Exceptions\ThrottleRequestsException;

class EmailThrottleException extends ThrottleRequestsException
{
    //
}
