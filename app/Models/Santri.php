<?php

namespace App\Models;

use App\Model;
use App\Models\Anggota;
use App\Models\Biaya;
use App\Models\Jenjang;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Traits\GetCountTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;
use Korridor\LaravelModelValidationRules\Rules\ExistsEloquent;
use TestMonitor\Incrementable\Traits\Incrementable;

/**
 * App\Models\Santri
 *
 * @property int $id
 * @property int $angkatan
 * @property int $id_anggota
 * @property int $id_kelas
 * @property int|null $id_jenjang
 * @property int|null $id_kelompok
 * @property int|null $nomor
 * @property string|null $nomor_induk
 * @property object $detail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Anggota $anggota
 * @property-read string $nomor_induk_lengkap
 * @property-read string $nomor_reg_lengkap
 * @property-read string $nomor_reg
 * @property-read \App\Models\Biaya $biaya
 * @property-read \App\Models\Jenjang|null $jenjang
 * @property-read \App\Models\Kelas $kelas
 * @property-read \App\Models\Kelompok|null $kelompok
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Santri angkatan($angkatan)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Santri angkatanNow()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Santri belumDites()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Santri newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Santri newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Santri query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Santri sudahDites()
 * @mixin \Eloquent
 */
class Santri extends Model
{
    use Incrementable;
    use GetCountTrait;

    public const DETAIL_PENDAFTARAN = [
        'baru' => 'Pendaftaran baru',
        'ulang' => 'Pendaftaran ulang',
    ];

    public const DETAIL_LULUS_JENJANG = [
        'lulus' => 'Lulus',
        'belum' => 'Belum lulus',
    ];

    protected $table = 'santri';
    protected $incrementable = 'nomor';
    protected $incrementableGroup = ['angkatan'];

    protected $fillable = [
        // 'id',
        // 'angkatan',
        // 'id_anggota',
        // 'id_kelas',
        // 'id_jenjang',
        // 'id_kelompok',
        // 'nomor',
        // 'nomor_induk',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'angkatan' => 'integer',
        'id_anggota' => 'integer',
        'id_kelas' => 'integer',
        'id_jenjang' => 'integer',
        'id_kelompok' => 'integer',
        'nomor' => 'integer',
        'detail' => 'object',
    ];

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        $rule_id_kelompok = (!$this->id_kelompok) ? '' : new ExistsEloquent(
            Kelompok::class,
            'id',
            function (Builder $query) {
                return $query
                ->where('id_jenjang', $this->id_jenjang)
                ->where('angkatan', $this->angkatan)
                // ->where('sisa', '>=', $this->isDirty('id_kelompok'))
                ->whereHas(
                    'jadwal.anggota.profil',
                    function (Builder $query) {
                        return $query->where(
                            'jenis_kelamin',
                            $this->anggota->profil->jenis_kelamin ?? ''
                        );
                    }
                );
            }
        );

        return [
            'angkatan' => 'required|integer|min:1|max:255',
            'id_anggota' => 'required|integer|exists:anggota,id',
            'id_kelas' => [
                'required',
                'integer',
                'exists:kelas,id',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where([
                        ['angkatan', $this->angkatan],
                        ['id_anggota', $this->id_anggota],
                    ]);
                }),
            ],
            'id_jenjang' => 'nullable|integer|exists:jenjang,id',
            'id_kelompok' => [
                'nullable',
                'integer',
                $rule_id_kelompok,
            ],
            'nomor' => [
                'nullable',
                'integer',
                'max:65535',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where('angkatan', $this->angkatan);
                }),
            ],
            'nomor_induk' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where('id_anggota', '<>', $this->id_anggota);
                }),
            ],
            'detail' => 'present|array',
            'detail.pendaftaran' => [
                'required',
                Rule::in(array_keys(self::DETAIL_PENDAFTARAN)),
            ],
            'detail.angkatan_sebelumnya' => [
                'nullable',
                Rule::requiredIf(($this->detail->pendaftaran ?? '') === 'ulang'),
                'integer',
                'min:1',
                'max:' . \angkatan(),
            ],
            'detail.id_jenjang_sebelumnya' => [
                'nullable',
                Rule::requiredIf(($this->detail->pendaftaran ?? '') === 'ulang'),
                'integer',
                // Rule::exists('jenjang', 'id')->where(function ($q) {
                //     $q->where('id_kelas', $this->id_kelas);
                // }),
            ],
            'detail.nama_jenjang_sebelumnya' => [
                'nullable',
                Rule::requiredIf(($this->detail->pendaftaran ?? '') === 'ulang'),
                'string',
            ],
            'detail.lulus_jenjang_sebelumnya' => [
                'nullable',
                Rule::requiredIf(($this->detail->pendaftaran ?? '') === 'ulang'),
                Rule::in(array_keys(self::DETAIL_LULUS_JENJANG)),
            ],
        ];
    }

    public function setDetail(array $detail_input)
    {
        $defaults = [
            'pendaftaran' => null,
        ];

        if (($detail_input['pendaftaran'] ?? '') === 'ulang') {
            $defaults = array_merge($defaults, [
                'angkatan_sebelumnya' => null,
                'id_jenjang_sebelumnya' => null,
                // 'nama_jenjang_sebelumnya' => null,
                'lulus_jenjang_sebelumnya' => null,
            ]);
        }

        $detail = [];

        foreach ($defaults as $key => $value) {
            $detail[$key] = $detail_input[$key] ?? $value;
        }

        if ($id_jenjang = $detail['id_jenjang_sebelumnya'] ?? null) {
            if ($jenjang = Jenjang::find($id_jenjang)) {
                $detail['nama_jenjang_sebelumnya'] = $jenjang->nama;
            }
        }

        $this->detail = $detail;
    }

    public function save(array $options = []): bool
    {
        if ($this->isClean('id_kelompok')) return parent::save();

        \DB::beginTransaction();

        $old_kelompok = Kelompok::find($this->original['id_kelompok'] ?? null);
        $new_kelompok = Kelompok::find($this->attributes['id_kelompok'] ?? null);

        $saved = parent::save($options);

        if ($old_kelompok) $old_kelompok->updateSisa();
        if ($new_kelompok) $new_kelompok->updateSisa();

        \DB::commit();

        return $saved;
    }

    public function scopeAngkatan(Builder $query, $angkatan)
    {
        return $query->where('angkatan', $angkatan);
    }

    public function scopeAngkatanNow(Builder $query)
    {
        return $this->scopeAngkatan($query, \setting('angkatan', \angkatan()));
    }

    public function scopeBelumDites(Builder $query)
    {
        return $query->whereNull('id_jenjang');
    }

    public function scopeSudahDites(Builder $query)
    {
        return $query->whereNotNull('id_jenjang');
    }

    public function isSudahDites()
    {
        return $this->id_jenjang !== null;
    }

    public function getNomorRegAttribute(): string
    {
        if (!$this->nomor) return '';

        return sprintf('%02d-%04d', $this->angkatan, $this->nomor);
    }

    public function getNomorRegLengkapAttribute(): string
    {
        if (!$this->nomor) return '';

        return "NRS.{$this->nomor_reg}";
    }

    public function getNomorIndukLengkapAttribute(): string
    {
        if (!$this->nomor_induk) return '';

        return "NIS.{$this->nomor_induk}";
    }

    public function initNomorInduk(): void
    {
        if (!$this->id || !$this->id_jenjang || $this->nomor_induk) return;

        \DB::beginTransaction();

        $nomor_induk = self::where('id_anggota', $this->id_anggota)->value('nomor_induk');

        if (!$nomor_induk) {
            $count = self::query()
            ->where('id_anggota', '<>', $this->id_anggota)
            ->where('angkatan', $this->angkatan)
            ->whereNotNull('nomor_induk')
            ->where('nomor_induk', 'LIKE', sprintf('%02d-%%', $this->angkatan))
            ->lockForUpdate()
            ->count(\DB::raw('DISTINCT id_anggota'));

            $nomor_induk = sprintf('%02d-%04d', $this->angkatan, $count + 1);
            $update_anggota = true;
        }

        $this->nomor_induk = $nomor_induk;
        $this->save();

        if (isset($update_anggota)) {
            $anggota = $this->anggota;
            $anggota->nomor_induk_santri = $nomor_induk;
            $anggota->save();
        }

        \DB::commit();
    }

    public function anggota()
    {
        return $this->belongsTo(Anggota::class, 'id_anggota');
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'id_kelas');
    }

    public function jenjang()
    {
        return $this->belongsTo(Jenjang::class, 'id_jenjang');
    }

    public function kelompok()
    {
        return $this->belongsTo(Kelompok::class, 'id_kelompok');
    }

    public function biaya()
    {
        return $this->hasOne(Biaya::class, 'id_santri')->withDefault([
            'nominal' => \app('setelan')->detail['biaya']['nominal'],
            'status' => Biaya::STATUS_BELUM_DIBAYAR,
        ]);
    }
}
