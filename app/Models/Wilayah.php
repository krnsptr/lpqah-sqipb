<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Wilayah
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Wilayah extends Model
{
    use Cachable;

    public $timestamps = false;
}
