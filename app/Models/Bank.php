<?php

namespace App\Models;

use App\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\Bank
 *
 * @property int $id
 * @property string $nama
 * @property string|null $kode
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Bank newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Bank newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Bank query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bank withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Bank extends Model
{
    use Cachable;

    protected $table = 'bank';
    public $timestamps = false;
}
