<?php

namespace App\Models;

use App\Model;
use App\Models\Instruktur;
use App\Models\Jadwal;
use App\Models\Jenjang;
use App\Models\Santri;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use TestMonitor\Incrementable\Traits\Incrementable;

/**
 * App\Models\Kelompok
 *
 * @property int $id
 * @property int $angkatan
 * @property int $id_instruktur
 * @property int $id_jenjang
 * @property int $id_jadwal
 * @property int|null $nomor
 * @property int $kuota
 * @property int $sisa
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $nomor_reg_lengkap
 * @property-read string $nomor_reg
 * @property-read \App\Models\Instruktur $instruktur
 * @property-read \App\Models\Jadwal $jadwal
 * @property-read \App\Models\Jenjang $jenjang
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Santri[] $santri
 * @property-read int|null $santri_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelompok angkatan($angkatan)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelompok angkatanNow()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelompok newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelompok newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelompok query()
 * @mixin \Eloquent
 */
class Kelompok extends Model
{
    use Incrementable;

    protected $table = 'kelompok';
    protected $incrementable = 'nomor';
    protected $incrementableGroup = ['angkatan'];

    protected $fillable = [
        // 'id',
        // 'angkatan',
        // 'id_instruktur',
        // 'id_jenjang',
        // 'id_jadwal',
        // 'nomor'
        // 'kuota',
        // 'sisa',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'kuota' => 10,
        'sisa' => 10,
    ];

    protected $casts = [
        'angkatan' => 'integer',
        'id_instruktur' => 'integer',
        'id_jenjang' => 'integer',
        'id_jadwal' => 'integer',
        'nomor' => 'integer',
        'kuota' => 'integer',
        'sisa' => 'integer',
    ];

    protected function getRules(): array
    {
        return [
            'angkatan' => 'required|integer|min:1|max:255',
            'id_instruktur' => [
                'required',
                'integer',
                Rule::exists('instruktur', 'id')->where(function ($q) {
                    $q->where([
                        ['id_anggota', '=', $this->jadwal->id_anggota ?? 0],
                        ['angkatan', '=', $this->jadwal->angkatan ?? 0],
                    ]);
                }),
            ],
            'id_jenjang' => [
                'required',
                'integer',
                Rule::exists('jenjang', 'id')->where(function ($q) {
                    $q->where([
                        ['id_kelas', '=', $this->instruktur->id_kelas ?? 0],
                        ['kode', '<=', $this->instruktur->jenjang->kode ?? 0],
                    ]);
                }),
            ],
            'id_jadwal' => [
                'required',
                'integer',
                'exists:jadwal,id',
                Rule::unique($this->table)->ignore($this->id),
            ],
            'nomor' => [
                'nullable',
                'integer',
                'max:65535',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where('angkatan', $this->angkatan);
                }),
            ],
            'kuota' => 'required|integer|min:1|max:255',
            'sisa' => 'required|integer|min:0|max:255|lte:kuota',
        ];
    }

    public function delete()
    {
        if ($this->santri->count()) {
            throw ValidationException::withMessages([
                'id' => 'Kelompok tidak dapat dihapus karena memiliki santri.'
            ]);
        }

        return parent::delete();
    }

    public function scopeAngkatan(Builder $query, $angkatan)
    {
        return $query->where('angkatan', $angkatan);
    }

    public function scopeAngkatanNow(Builder $query)
    {
        return $this->scopeAngkatan($query, \setting('angkatan', \angkatan()));
    }

    public function getNomorRegAttribute(): string
    {
        if (!$this->nomor) return '';

        return sprintf('%02d-%04d', $this->angkatan, $this->nomor);
    }

    public function getNomorRegLengkapAttribute(): string
    {
        if (!$this->nomor) return '';

        return "KLP.{$this->nomor_reg}";
    }

    public function setIdJadwalAttribute($id_jadwal)
    {
        if ($jadwal = Jadwal::find($id_jadwal)) {
            $this->attributes['angkatan'] = $jadwal->angkatan;
            $this->attributes['id_jadwal'] = $jadwal->id;
        }
    }

    public function updateSisa(): void
    {
        \DB::beginTransaction();

        $kelompok = self::where('id', $this->id)->lockForUpdate()->first();

        $count_santri = $this->santri()->lockForUpdate()->count();
        $sisa = $kelompok->kuota - $count_santri;

        if ($sisa < 0) {
            throw ValidationException::withMessages([
                'id_kelompok' => 'Kelompok sudah penuh.',
            ]);
        }

        $kelompok->sisa = $sisa;
        $kelompok->save();

        \DB::commit();

        $this->refresh();
    }

    public function instruktur()
    {
        return $this->belongsTo(Instruktur::class, 'id_instruktur');
    }

    public function jenjang()
    {
        return $this->belongsTo(Jenjang::class, 'id_jenjang');
    }

    public function jadwal()
    {
        return $this->belongsTo(Jadwal::class, 'id_jadwal');
    }

    public function santri()
    {
        return $this->hasMany(Santri::class, 'id_kelompok');
    }
}
