<?php

namespace App\Models;

use App\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

const DETAIL = <<<'EOD'
{
    "pengumuman": {
        "calon_instruktur": {
            "show": false,
            "title": "Info Penting Calon Instruktur",
            "text": "<p>...</p>"
        },
        "calon_santri": {
            "show": false,
            "title": "Info Penting Calon Santri",
            "text": "<p>...</p>"
        },
        "instruktur": {
            "show": false,
            "title": "Info Penting Instruktur",
            "text": "<p>...</p>"
        },
        "santri": {
            "show": false,
            "title": "Info Penting Santri",
            "text": "<p>...</p>"
        },
        "umum": {
            "show": false,
            "title": "Pengumuman",
            "text": "<p>...</p>"
        },
        "pembayaran": {
            "show": false,
            "title": "Petunjuk Pembayaran",
            "text": "<p>...</p>"
        }
    },
    "syarat_instruktur": [],
    "biaya": {
        "label": "Biaya SPP",
        "nominal": 100000,
        "keterangan": "Mandiri,BSM,BNIS,BRIS,BCA,BCAS,BTN,BTPN,Tunai",
        "rekening_instruktur_display": "required",
        "rekening_santri_display": "hidden"
    },
    "kuota_kelompok": 10
}
EOD;

/**
 * App\Models\Setelan
 *
 * @property int $id
 * @property int $angkatan
 * @property \Illuminate\Support\Carbon $waktu_buka_pendaftaran_instruktur
 * @property \Illuminate\Support\Carbon $waktu_tutup_pendaftaran_instruktur
 * @property \Illuminate\Support\Carbon $waktu_buka_pendaftaran_santri
 * @property \Illuminate\Support\Carbon $waktu_tutup_pendaftaran_santri
 * @property \Illuminate\Support\Carbon $waktu_buka_penjadwalan_instruktur
 * @property \Illuminate\Support\Carbon $waktu_tutup_penjadwalan_instruktur
 * @property \Illuminate\Support\Carbon $waktu_buka_penjadwalan_santri
 * @property \Illuminate\Support\Carbon $waktu_tutup_penjadwalan_santri
 * @property \Illuminate\Support\Carbon $waktu_buka_publikasi_jadwal
 * @property \Illuminate\Support\Carbon $waktu_tutup_publikasi_jadwal
 * @property object $detail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setelan angkatan($angkatan)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setelan angkatanNow()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setelan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setelan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setelan query()
 * @mixin \Eloquent
 */
class Setelan extends Model
{
    public const BIAYA_LABEL = [
        'Biaya SPP',
        'Biaya Komitmen',
        'Infaq',
    ];

    public const REKENING_DISPLAY = [
        'required' => 'Wajib',
        'optional' => 'Opsional',
        'hidden' => 'Tidak perlu',
    ];

    protected $table = 'setelan';

    protected $fillable = [
        // 'id',
        // 'angkatan',
        'waktu_buka_pendaftaran_instruktur',
        'waktu_tutup_pendaftaran_instruktur',
        'waktu_buka_pendaftaran_santri',
        'waktu_tutup_pendaftaran_santri',
        'waktu_buka_penjadwalan_instruktur',
        'waktu_tutup_penjadwalan_instruktur',
        'waktu_buka_penjadwalan_santri',
        'waktu_tutup_penjadwalan_santri',
        'waktu_buka_publikasi_jadwal',
        'waktu_tutup_publikasi_jadwal',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => DETAIL,
    ];

    protected $casts = [
        'angkatan' => 'integer',
        'waktu_buka_pendaftaran_instruktur' => 'datetime',
        'waktu_tutup_pendaftaran_instruktur' => 'datetime',
        'waktu_buka_pendaftaran_santri' => 'datetime',
        'waktu_tutup_pendaftaran_santri' => 'datetime',
        'waktu_buka_penjadwalan_instruktur' => 'datetime',
        'waktu_tutup_penjadwalan_instruktur' => 'datetime',
        'waktu_buka_penjadwalan_santri' => 'datetime',
        'waktu_tutup_penjadwalan_santri' => 'datetime',
        'waktu_buka_publikasi_jadwal' => 'datetime',
        'waktu_tutup_publikasi_jadwal' => 'datetime',
        'detail' => 'array',
    ];

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        $pengumuman_keys = array_keys(json_decode(DETAIL, true)['pengumuman']);
        $detail_rules = [
            'detail' => 'present|array',
            'detail.syarat_instruktur' => 'required|array',
            'detail.syarat_instruktur.*' => 'required|array',
            'detail.syarat_instruktur.*.*' => 'required|string',
            'detail.jalur' => 'nullable|array',
            'detail.jalur.*' => 'nullable|array',
            'detail.jalur.*.*' => 'nullable|string',
            'detail.biaya.label' => 'required|string',
            'detail.biaya.nominal' => 'required|integer|min:0',
            'detail.biaya.keterangan' => 'required|string',
            'detail.biaya.rekening_instruktur_display' => [
                'required',
                Rule::in(array_keys(self::REKENING_DISPLAY)),
            ],
            'detail.biaya.rekening_santri_display' => [
                'required',
                Rule::in(array_keys(self::REKENING_DISPLAY)),
            ],
        ];

        foreach ($pengumuman_keys as $key) {
            $detail_rules["detail.pengumuman.$key.show"] = 'required|boolean';
            $detail_rules["detail.pengumuman.$key.title"] = 'required|string';
            $detail_rules["detail.pengumuman.$key.text"] = 'required|string';
        }

        return array_merge([
            'angkatan' => [
                'required',
                'integer',
                'min:1',
                'max:255',
                Rule::unique($this->table)->ignore($this->id),
            ],
            'waktu_buka_pendaftaran_instruktur' => [
                'required',
                'date',
            ],
            'waktu_tutup_pendaftaran_instruktur' => [
                'required',
                'date',
                'after_or_equal:waktu_buka_pendaftaran_instruktur',
            ],
            'waktu_buka_pendaftaran_santri' => [
                'required',
                'date',
            ],
            'waktu_tutup_pendaftaran_santri' => [
                'required',
                'date',
                'after_or_equal:waktu_buka_pendaftaran_santri',
            ],
            'waktu_buka_penjadwalan_instruktur' => [
                'required',
                'date',
            ],
            'waktu_tutup_penjadwalan_instruktur' => [
                'required',
                'date',
                'after_or_equal:waktu_buka_penjadwalan_instruktur',
            ],
            'waktu_buka_penjadwalan_santri' => [
                'required',
                'date',
            ],
            'waktu_tutup_penjadwalan_santri' => [
                'required',
                'date',
                'after_or_equal:waktu_buka_penjadwalan_santri',
            ],
            'waktu_buka_publikasi_jadwal' => [
                'required',
                'date',
            ],
            'waktu_tutup_publikasi_jadwal' => [
                'required',
                'date',
                'after_or_equal:waktu_buka_publikasi_jadwal',
            ],
        ], $detail_rules);
    }

    public function scopeAngkatan(Builder $query, $angkatan)
    {
        return $query->where('angkatan', $angkatan);
    }

    public function scopeAngkatanNow(Builder $query)
    {
        return $this->scopeAngkatan($query, \setting('angkatan', \angkatan()));
    }

    public function isDibuka(string $slug = ''): bool
    {
        if (\admin()) return true;

        if (!isset($this->{'waktu_buka_' . $slug})) return false;
        if (!isset($this->{'waktu_tutup_' . $slug})) return false;

        return (
            $this->{'waktu_buka_' . $slug} <= \now()
            && $this->{'waktu_tutup_' . $slug} >= \now()
        );
    }

    public function isDibukaPendaftaran(string $slug = null): bool
    {
        if (\admin()) return true;

        if (!$slug) return (
            $this->isDibuka('pendaftaran_santri')
            || $this->isDibuka('pendaftaran_instruktur')
        );

        return $this->isDibuka("pendaftaran_$slug");
    }

    public function isDibukaPenjadwalan(string $slug = null): bool
    {
        if (\admin()) return true;

        if (!$slug) return (
            $this->isDibuka('penjadwalan_santri')
            || $this->isDibuka('penjadwalan_instruktur')
        );

        return $this->isDibuka("penjadwalan_$slug");
    }

    public function getKuotaKelompok($jenis_kelamin = null): int
    {
        if (isset($jenis_kelamin, $this->detail["kuota_kelompok_$jenis_kelamin"])) {
            return (int) $this->detail["kuota_kelompok_$jenis_kelamin"];
        }

        return (int) ($this->detail['kuota_kelompok'] ?? 10);
    }
}
