<?php

namespace App\Models;

use App\Models\Pengguna;
use Illuminate\Validation\Rule;

/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string|null $foto
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $foto_url
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin query()
 * @mixin \Eloquent
 */
final class Admin extends Pengguna
{
    protected $table = 'admin';

    protected function getRules(): array
    {
        return array_merge(parent::$rules, [
            'email' => [
                'required',
                'email',
                Rule::unique('admin')->ignore($this->id),
            ],
        ]);
    }
}
