<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kota;

/**
 * App\Models\Wilayah\Provinsi
 *
 * @property int $id
 * @property string $nama
 * @property string|null $kode
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wilayah\Kota[] $kota
 * @property-read int|null $kota_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Provinsi newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Provinsi newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Provinsi query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Provinsi extends Wilayah
{
    protected $table = 'wilayah_provinsi';

    public function kota()
    {
        return $this->hasMany(Kota::class, 'id_provinsi');
    }
}
