<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kelurahan;
use App\Models\Wilayah\Kota;

/**
 * App\Models\Wilayah\Kecamatan
 *
 * @property int $id
 * @property int $id_kota
 * @property string $nama
 * @property string|null $kode
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wilayah\Kelurahan[] $kelurahan
 * @property-read int|null $kelurahan_count
 * @property-read \App\Models\Wilayah\Kota $kota
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kecamatan newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kecamatan newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kecamatan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Kecamatan extends Wilayah
{
    protected $table = 'wilayah_kecamatan';

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'id_kota');
    }
    public function kelurahan()
    {
        return $this->hasMany(Kelurahan::class, 'id_kecamatan');
    }
}
