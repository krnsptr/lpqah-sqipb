<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Provinsi;

/**
 * App\Models\Wilayah\Kota
 *
 * @property int $id
 * @property int $id_provinsi
 * @property string $nama
 * @property string|null $kode
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wilayah\Kecamatan[] $kecamatan
 * @property-read int|null $kecamatan_count
 * @property-read \App\Models\Wilayah\Provinsi $provinsi
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kota newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kota newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kota query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Kota extends Wilayah
{
    protected $table = 'wilayah_kota';

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'id_provinsi');
    }

    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class, 'id_kota');
    }
}
