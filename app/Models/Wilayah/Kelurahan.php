<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kecamatan;

/**
 * App\Models\Wilayah\Kelurahan
 *
 * @property int $id
 * @property int $id_kecamatan
 * @property string $nama
 * @property string|null $kode
 * @property-read \App\Models\Wilayah\Kecamatan $kecamatan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kelurahan newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kelurahan newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|\App\Models\Wilayah\Kelurahan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Wilayah withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Kelurahan extends Wilayah
{
    protected $table = 'wilayah_kelurahan';

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'id_kecamatan');
    }
}
