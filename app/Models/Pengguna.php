<?php

namespace App\Models;

use App\Models\Admin;
use App\Models\Anggota;
use App\Notifications\Anggota\ResetPasswordNotification;
use App\Notifications\Anggota\VerifyEmailNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

/**
 * App\Models\Pengguna
 *
 * @property int $id
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string|null $foto
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $foto_url
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pengguna newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pengguna newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pengguna query()
 * @mixin \Eloquent
 */
class Pengguna extends Authenticatable
{
    use Notifiable;

    public const FOTO_DIR = '';
    public const FOTO_DEFAULT = '_default.png';

    protected $fillable = [
        // 'id',
        'nama',
        'email',
        'password',
        // 'remember_token',
        // 'created_at',
        // 'updated_at',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected static $rules = [
        'nama' => 'required|string|max:255',
        'email' => 'required|email|max:191',
        'password' => 'required|string|max:255',
        'foto' => 'nullable|string|max:255',
    ];

    protected function getRules(): array
    {
        return static::$rules;
    }

    protected function getData(): array
    {
        return $this->getAttributes();
    }

    public function save(array $options = []): bool
    {
        Validator::make($this->getData(), $this->getRules())->validate();

        return parent::save($options);
    }

    public function getFotoUrlAttribute(): string
    {
        $dir = static::FOTO_DIR;
        $file = $this->attributes['foto'] ?? static::FOTO_DEFAULT;

        return \Storage::disk('public')->url("$dir/$file");
    }

    public function setFotoAttribute($foto): void
    {
        if ($foto instanceof UploadedFile) {
            \Validator::make(compact('foto'), [
                'foto' => 'image|max:5000',
            ])->validate();

            $id = (int) $this->id;
            $uuid = (string) Str::orderedUuid();
            $extension = $foto->extension();
            $name = "$id--$uuid.$extension";
            $dir = static::FOTO_DIR;

            $path = \Storage::disk('public')->putFileAs($dir, $foto, $name);

            if ($path) $this->attributes['foto'] = $name;
        } elseif (is_string($foto)) {
            $this->attributes['foto'] = $foto;
        }
    }

    public function unlinkFotoOld(): void
    {
        $dir = static::FOTO_DIR;
        $file = $this->changes['foto'];

        if (empty($file) || $file === static::FOTO_DEFAULT) return;

        \Storage::disk('public')->delete("$dir/$file");
    }

    public function isAdmin(): bool
    {
        return $this instanceof Admin;
    }

    public function isAnggota(): bool
    {
        return $this instanceof Anggota;
    }

    public function sendPasswordResetNotification($token): void
    {
        if ($this->isAnggota()) {
            $this->notify(new ResetPasswordNotification($token));
        } else {
            parent::sendPasswordResetNotification($token);
        }
    }

    public function sendEmailVerificationNotification(): void
    {
        if ($this->isAnggota()) {
            $this->notify(new VerifyEmailNotification());
        } else {
            parent::sendEmailVerificationNotification();
        }
    }
}
