<?php

namespace App\Models;

use App\Model;
use App\Models\Anggota;
use App\Models\Jadwal;
use App\Models\Jenjang;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Traits\GetCountTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;
use TestMonitor\Incrementable\Traits\Incrementable;

/**
 * App\Models\Instruktur
 *
 * @property int $id
 * @property int $angkatan
 * @property int $id_anggota
 * @property int $id_kelas
 * @property int|null $id_jenjang
 * @property int|null $nomor
 * @property string|null $nomor_induk
 * @property object $detail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Anggota $anggota
 * @property-read string $nomor_induk_lengkap
 * @property-read string $nomor_reg_lengkap
 * @property-read string $nomor_reg
 * @property-read \App\Models\Jenjang|null $jenjang
 * @property-read \App\Models\Kelas $kelas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelompok[] $kelompok
 * @property-read int|null $kelompok_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Instruktur angkatan($angkatan)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Instruktur angkatanNow()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Instruktur belumDites()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Instruktur newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Instruktur newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Instruktur query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Instruktur sudahDites()
 * @mixin \Eloquent
 */
class Instruktur extends Model
{
    use Incrementable;
    use GetCountTrait;

    public const DETAIL_PENDAFTARAN = [
        'baru' => 'Pendaftaran baru',
        'ulang' => 'Pendaftaran ulang',
    ];

    protected $table = 'instruktur';
    protected $incrementable = 'nomor';
    protected $incrementableGroup = ['angkatan'];

    protected $fillable = [
        // 'id',
        // 'angkatan',
        // 'id_anggota',
        // 'id_kelas',
        // 'id_jenjang',
        // 'nomor',
        // 'nomor_induk',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'angkatan' => 'integer',
        'id_anggota' => 'integer',
        'id_kelas' => 'integer',
        'id_jenjang' => 'integer',
        'nomor' => 'integer',
        'detail' => 'object',
    ];

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'angkatan' => 'required|integer|min:1|max:255',
            'id_anggota' => 'required|integer|exists:anggota,id',
            'id_kelas' => [
                'required',
                'integer',
                'exists:kelas,id',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where([
                        ['angkatan', $this->angkatan],
                        ['id_anggota', $this->id_anggota],
                    ]);
                }),
            ],
            'id_jenjang' => 'nullable|integer|exists:jenjang,id',
            'nomor' => [
                'nullable',
                'integer',
                'max:65535',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where('angkatan', $this->angkatan);
                }),
            ],
            'nomor_induk' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where('id_anggota', '<>', $this->id_anggota);
                }),
            ],
            'detail' => 'present|array',
            'detail.pendaftaran' => [
                'required',
                Rule::in(array_keys(self::DETAIL_PENDAFTARAN)),
            ],
            'detail.kapasitas_membina' => 'required|integer|min:1',
            'detail.syarat_instruktur' => 'required|array|min:1',
            'detail.syarat_instruktur.*' => 'required|string',
        ];
    }

    public function setDetail(array $detail_input)
    {
        $defaults = [
            'pendaftaran' => null,
            'kapasitas_membina' => 1,
            'syarat_instruktur' => [],
        ];

        $detail = [];

        foreach ($defaults as $key => $value) {
            $detail[$key] = $detail_input[$key] ?? $value;

            if ($key === 'syarat_instruktur' && is_array($detail[$key])) {
                $detail[$key] = array_values($detail[$key]);
            }
        }

        $this->detail = $detail;
    }

    public function scopeAngkatan(Builder $query, $angkatan)
    {
        return $query->where('angkatan', $angkatan);
    }

    public function scopeAngkatanNow(Builder $query)
    {
        return $this->scopeAngkatan($query, \setting('angkatan', \angkatan()));
    }

    public function scopeBelumDites(Builder $query)
    {
        return $query->whereNull('id_jenjang');
    }

    public function scopeSudahDites(Builder $query)
    {
        return $query->whereNotNull('id_jenjang');
    }

    public function isSudahDites()
    {
        return $this->id_jenjang !== null;
    }

    public function getNomorRegAttribute(): string
    {
        if (!$this->nomor) return '';

        return sprintf('%02d-%04d', $this->angkatan, $this->nomor);
    }

    public function getNomorRegLengkapAttribute(): string
    {
        if (!$this->nomor) return '';

        return "NRI.{$this->nomor_reg}";
    }

    public function getNomorIndukLengkapAttribute(): string
    {
        if (!$this->nomor_induk) return '';

        return "NII.{$this->nomor_induk}";
    }

    public function initNomorInduk(): void
    {
        if (!$this->id || !$this->id_jenjang || $this->nomor_induk) return;

        \DB::beginTransaction();

        $nomor_induk = self::where('id_anggota', $this->id_anggota)->value('nomor_induk');

        if (!$nomor_induk) {
            $count = self::query()
            ->where('id_anggota', '<>', $this->id_anggota)
            ->where('angkatan', $this->angkatan)
            ->whereNotNull('nomor_induk')
            ->where('nomor_induk', 'LIKE', sprintf('%02d-%%', $this->angkatan))
            ->lockForUpdate()
            ->count(\DB::raw('DISTINCT id_anggota'));

            $nomor_induk = sprintf('%02d-%04d', $this->angkatan, $count + 1);
            $update_anggota = true;
        }

        $this->nomor_induk = $nomor_induk;
        $this->save();

        if (isset($update_anggota)) {
            $anggota = $this->anggota;
            $anggota->nomor_induk_instruktur = $nomor_induk;
            $anggota->save();
        }

        \DB::commit();
    }

    public function anggota()
    {
        return $this->belongsTo(Anggota::class, 'id_anggota');
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'id_kelas');
    }

    public function jenjang()
    {
        return $this->belongsTo(Jenjang::class, 'id_jenjang');
    }

    public function kelompok()
    {
        return $this->hasMany(Kelompok::class, 'id_instruktur');
    }
}
