<?php

namespace App\Models;

use App\Model;
use App\Models\Kelas;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Jenjang
 *
 * @property int $id
 * @property int $id_kelas
 * @property string $nama
 * @property string $kode
 * @property object $detail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Kelas $kelas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jenjang aktif()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jenjang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jenjang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jenjang query()
 * @mixin \Eloquent
 */
class Jenjang extends Model
{
    protected $table = 'jenjang';

    protected $fillable = [
        // 'id',
        // 'id_kelas',
        'nama',
        'kode',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'id_kelas' => 'integer',
        'detail' => 'object',
    ];

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::addGlobalScope('kode', function (Builder $builder) {
            $builder->orderBy('kode');
        });
    }

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'id_kelas' => 'required|integer|exists:kelas,id',
            'nama' => 'required|string|max:255',
            'kode' => 'required|string|max:255',
            'detail' => 'present|array',
        ];
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'id_kelas');
    }
}
