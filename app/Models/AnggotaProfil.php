<?php

namespace App\Models;

use App\Model;
use App\Models\Anggota;
use App\Models\Bank;
use App\Models\Wilayah\Kota;
use App\Models\Wilayah\Provinsi;
use Illuminate\Validation\Rule;

/**
 * App\Models\AnggotaProfil
 *
 * @property int $id
 * @property int $id_provinsi
 * @property int $id_kota
 * @property int $id_bank
 * @property string $username
 * @property string $jenis_kelamin
 * @property \Illuminate\Support\Carbon $tanggal_lahir
 * @property int $status
 * @property string $jenis_identitas
 * @property string $nomor_identitas
 * @property string $nomor_hp
 * @property string $nomor_wa
 * @property string|null $nomor_rekening
 * @property string|null $nama_rekening
 * @property object $detail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $jenis_kelamin_text
 * @property-read string $status_text
 * @property-read \App\Models\Anggota $anggota
 * @property-read \App\Models\Bank|null $bank
 * @property-read \App\Models\Wilayah\Kota $kota
 * @property-read \App\Models\Wilayah\Provinsi $provinsi
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AnggotaProfil newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AnggotaProfil newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AnggotaProfil query()
 * @mixin \Eloquent
 */
class AnggotaProfil extends Model
{
    public const JENIS_KELAMIN = [
        'L' => 'Laki-laki',
        'P' => 'Perempuan',
    ];

    public const STATUS_MAHASISWA = 1;
    public const STATUS_ALUMNI = 2;
    public const STATUS_PEGAWAI = 3;
    public const STATUS_UMUM = 4;

    public const STATUS = [
        self::STATUS_MAHASISWA => 'Mahasiswa IPB',
        self::STATUS_ALUMNI => 'Alumni IPB',
        self::STATUS_PEGAWAI => 'Pegawai IPB',
        self::STATUS_UMUM => 'Umum',
    ];

    public const JENIS_IDENTITAS = [
        self::STATUS_MAHASISWA => ['NIM'],
        self::STATUS_ALUMNI => ['NIK', 'NIM'],
        self::STATUS_PEGAWAI => ['NIK'],
        self::STATUS_UMUM => ['NIK'],
    ];

    public const REGEX = [
        'NIM' => '/^[A-Z]\d[A-Z0-9]\d{6,8}$/',
        'NIK' => '/^[1-9]\d\.\d{2}\.\d{2}\.\d{2}-\d{2}-\d{2}\.\d{4}$/',
        'nomor_hp' => '/^\+62-8\d{2}-\d{4}-\d{2,5}$/',
        'nomor_rekening' => '/^\d{10,20}$/',
    ];

    protected $table = 'anggota_profil';
    public $incrementing = false;

    protected $fillable = [
        // 'id',
        // 'id_provinsi',
        // 'id_kota',
        // 'id_bank',
        'username',
        // 'jenis_kelamin',
        'tanggal_lahir',
        'status',
        'jenis_identitas',
        'nomor_identitas',
        'nomor_hp',
        'nomor_wa',
        'nomor_rekening',
        'nama_rekening',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'id_provinsi' => 'integer',
        'id_kota' => 'integer',
        'tanggal_lahir' => 'date',
        'status' => 'integer',
        'detail' => 'object',
    ];

    protected $touches = ['anggota'];

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        $is_rekening_required = $this->isRekeningRequired();

        return [
            'id_provinsi' => 'required|integer|exists:wilayah_provinsi,id',
            'id_kota' => 'required|integer|exists:wilayah_kota,id',
            'id_bank' => [
                Rule::requiredIf($is_rekening_required),
                'nullable',
                'integer',
                'exists:bank,id',
            ],
            'username' => [
                'required',
                'string',
                'min:6',
                'max:20',
                'alpha_dash',
                Rule::unique('anggota_profil')->ignore($this->id),
            ],
            'jenis_kelamin' => 'required|string|in:L,P',
            'tanggal_lahir' => 'required|date|before:now',
            'status' => [
                'required',
                Rule::in(array_keys(self::STATUS)),
            ],
            'jenis_identitas' => [
                'required',
                Rule::in(self::JENIS_IDENTITAS[$this->status] ?? []),
            ],
            'nomor_identitas' => [
                'required',
                'string',
                'regex:' . (self::REGEX[$this->jenis_identitas] ?? ''),
            ],
            'nomor_hp' => 'required|string|regex:' . self::REGEX['nomor_hp'],
            'nomor_wa' => 'required|string|regex:' . self::REGEX['nomor_hp'],
            'nomor_rekening' => [
                Rule::requiredIf($is_rekening_required),
                'nullable',
                'string',
                'regex:' . self::REGEX['nomor_rekening'],
            ],
            'nama_rekening' => [
                Rule::requiredIf($is_rekening_required),
                'nullable',
                'string',
                'max:255',
            ],
            'detail' => 'present|array',
        ];
    }

    protected function getRulesForCompleteness(): array
    {
        $is_rekening_required = $this->isRekeningRequired();

        return [
            'id_provinsi' => 'required',
            'id_kota' => 'required',
            'id_bank' => [Rule::requiredIf($is_rekening_required)],
            'username' => 'required',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required',
            'status' => 'required',
            'nomor_identitas' => 'required',
            'nomor_hp' => 'required',
            'nomor_wa' => 'required',
            'nomor_rekening' => [Rule::requiredIf($is_rekening_required)],
            'nama_rekening' => [Rule::requiredIf($is_rekening_required)],
            'detail' => 'present|array',
            'updated_at' => 'required|date|after:-6 month',
        ];
    }

    public function isComplete(): bool
    {
        $data = $this->getData();
        $rules = $this->getRulesForCompleteness();
        $validator = \Validator::make($data, $rules);

        return $validator->passes();
    }

    public function isRekeningRequired(): bool
    {
        $is_instruktur_diterima = $this->anggota->isInstrukturSudahDites();
        $is_santri_diterima = $this->anggota->isSantriSudahDites();

        $setelan = \app('setelan');

        $rekening_instruktur_display = $setelan->detail['biaya']['rekening_instruktur_display'];
        $rekening_santri_display = $setelan->detail['biaya']['rekening_santri_display'];

        $is_rekening_required = (
            ($is_instruktur_diterima && $rekening_instruktur_display === 'required')
            ||
            ($is_santri_diterima && $rekening_santri_display === 'required')
        );

        return $is_rekening_required;
    }

    public function isRekeningHidden(): bool
    {
        $is_instruktur_diterima = $this->anggota->isInstrukturSudahDites();
        $is_santri_diterima = $this->anggota->isSantriSudahDites();

        $setelan = \app('setelan');

        $rekening_instruktur_display = $setelan->detail['biaya']['rekening_instruktur_display'];
        $rekening_santri_display = $setelan->detail['biaya']['rekening_santri_display'];

        $is_rekening_hidden = (
            (!$is_instruktur_diterima || $rekening_instruktur_display === 'hidden')
            &&
            (!$is_santri_diterima || $rekening_santri_display === 'hidden')
        );

        return $is_rekening_hidden;
    }

    public function setUsernameAttribute($username)
    {
        if (is_string($username)) $username = strtolower($username);

        $this->attributes['username'] = $username;
    }

    public function getJenisKelaminTextAttribute(): ?string
    {
        return self::JENIS_KELAMIN[$this->jenis_kelamin] ?? null;
    }

    public function getStatusTextAttribute(): ?string
    {
        return self::STATUS[$this->status] ?? null;
    }

    public function setIdKotaAttribute($id_kota)
    {
        $kota = Kota::find($id_kota);

        if ($kota) {
            $this->attributes['id_kota'] = $kota->id;
            $this->attributes['id_provinsi'] = $kota->id_provinsi;
        }
    }

    public function setTanggalLahirAttribute($tanggal_lahir)
    {
        if (is_string($tanggal_lahir)) {
            \Validator::make(compact('tanggal_lahir'), [
                'tanggal_lahir' => 'required|date_format:d-m-Y',
            ])->validate();

            $tanggal_lahir = \Carbon\Carbon::parse($tanggal_lahir);
        }

        $this->attributes['tanggal_lahir'] = $tanggal_lahir;
    }

    public function setNomorIdentitasAttribute($nomor_identitas)
    {
        if (is_string($nomor_identitas)) $nomor_identitas = strtoupper($nomor_identitas);

        $this->attributes['nomor_identitas'] = $nomor_identitas;
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'id_bank');
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'id_provinsi');
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'id_kota');
    }

    public function anggota()
    {
        return $this->belongsTo(Anggota::class, 'id');
    }
}
