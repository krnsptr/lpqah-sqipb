<?php

namespace App\Models;

use App\Enums\Hari;
use App\Model;
use App\Models\Anggota;
use App\Models\Kelompok;
use BenSampo\Enum\Rules\EnumValue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\ValidationException;

/**
 * App\Models\Jadwal
 *
 * @property int $id
 * @property int $angkatan
 * @property int $id_anggota
 * @property \App\Enums\Hari $hari
 * @property \Illuminate\Support\Carbon $waktu_mulai
 * @property \Illuminate\Support\Carbon $waktu_selesai
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Anggota $anggota
 * @property-read \App\Models\Kelompok|null $kelompok
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jadwal angkatan($angkatan)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jadwal angkatanNow()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jadwal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jadwal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Jadwal query()
 * @mixin \Eloquent
 */
class Jadwal extends Model
{
    public const DURASI = '2 hour';
    public const WAKTU_MULAI_MIN = '05:00';
    public const WAKTU_MULAI_MAX = '20:00';
    public const WAKTU_SELESAI_MIN = '07:00';
    public const WAKTU_SELESAI_MAX = '22:00';

    protected $table = 'jadwal';

    protected $fillable = [
        // 'id',
        // 'angkatan',
        // 'id_anggota',
        'hari',
        'waktu_mulai',
        // 'waktu_selesai',
        // 'created_at',
        // 'updated_at',
    ];

    protected $casts = [
        'angkatan' => 'integer',
        'id_anggota' => 'integer',
        'hari' => Hari::class,
        'waktu_mulai' => 'datetime',
        'waktu_selesai' => 'datetime',
    ];

    protected function getRules(): array
    {
        return [
            'angkatan' => 'required|integer|min:1|max:255',
            'id_anggota' => 'required|integer|exists:anggota,id',
            'hari' => [
                'required',
                new EnumValue(Hari::class, false),
            ],
            'waktu_mulai' => [
                'required',
                'date_format:H:i',
                'after_or_equal:' . self::WAKTU_MULAI_MIN,
                'before_or_equal:' . self::WAKTU_MULAI_MAX,
            ],
            'waktu_selesai' => [
                'required',
                'date_format:H:i',
                'after_or_equal:' . self::WAKTU_SELESAI_MIN,
                'before_or_equal:' . self::WAKTU_SELESAI_MAX,
            ],
        ];
    }

    public function save(array $options = []): bool
    {
        if ($this->isDirty() && $this->kelompok && !\admin() && !\app()->runningInConsole()) {
            throw ValidationException::withMessages([
                'id' => 'Jadwal tidak dapat diubah.'
            ]);
        }

        return parent::save($options);
    }

    public function delete()
    {
        if ($this->kelompok) {
            throw ValidationException::withMessages([
                'id' => 'Jadwal tidak dapat dihapus.'
            ]);
        }

        return parent::delete();
    }

    public function scopeAngkatan(Builder $query, $angkatan)
    {
        return $query->where('angkatan', $angkatan);
    }

    public function scopeAngkatanNow(Builder $query)
    {
        return $this->scopeAngkatan($query, \setting('angkatan', \angkatan()));
    }

    public function getText(): string
    {
        $text = "{$this->hari->description}";
        $text .= " {$this->waktu_mulai->format('H:i')} WIB";

        return $text;
    }

    public function getJalurTextAttribute(): string
    {
        return $this->jalur ?? 'Reguler';
    }

    public function setWaktuMulaiAttribute($waktu_mulai)
    {
        if ($waktu_mulai = Carbon::parse($waktu_mulai)) {
            $waktu_mulai = $waktu_mulai->ceilMinute(15);
            $waktu_selesai = $waktu_mulai->copy()->add(self::DURASI);

            $this->attributes['waktu_mulai'] = $waktu_mulai->format('H:i');
            $this->attributes['waktu_selesai'] = $waktu_selesai->format('H:i');
        }
    }

    public function anggota()
    {
        return $this->belongsTo(Anggota::class, 'id_anggota');
    }

    public function kelompok()
    {
        return $this->hasOne(Kelompok::class, 'id_jadwal');
    }
}
