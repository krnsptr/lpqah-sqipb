<?php

namespace App\Models;

use App\Model;
use App\Models\Santri;
use Illuminate\Validation\Rule;

/**
 * App\Models\Biaya
 *
 * @property int $id
 * @property int $id_santri
 * @property int $status
 * @property int $nominal
 * @property string|null $keterangan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string|null $status_text
 * @property-read \App\Models\Santri $santri
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Biaya newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Biaya newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Biaya query()
 * @mixin \Eloquent
 */
class Biaya extends Model
{
    public const STATUS_BELUM_DIBAYAR = 0;
    public const STATUS_SUDAH_DIBAYAR = 1;

    public const STATUS = [
        self::STATUS_BELUM_DIBAYAR => 'Belum dibayar',
        self::STATUS_SUDAH_DIBAYAR => 'Dibayar',
    ];

    protected $table = 'biaya';

    protected $fillable = [
        // 'id',
        // 'id_santri',
        'status',
        'nominal',
        'keterangan',
        // 'created_at',
        // 'updated_at',
    ];

    protected $casts = [
        'id_santri' => 'integer',
        'status' => 'integer',
        'nominal' => 'integer',
    ];

    protected function getRules(): array
    {
        return [
            'id_santri' => [
                'required',
                'integer',
                'exists:santri,id',
                Rule::unique($this->table)->ignore($this->id)->where(function ($q) {
                    $q->where('id_santri', $this->id_santri);
                }),
            ],
            'status' => [
                'required',
                Rule::in(array_keys(self::STATUS)),
            ],
            'nominal' => 'required|integer|min:0',
            'keterangan' => 'nullable|string',
        ];
    }

    public function getStatusTextAttribute(): ?string
    {
        return self::STATUS[$this->status] ?? null;
    }

    public function santri()
    {
        return $this->belongsTo(Santri::class, 'id_santri');
    }
}
