<?php

namespace App\Models;

use App\Model;
use App\Models\Jenjang;

/**
 * App\Models\Kelas
 *
 * @property int $id
 * @property string $nama
 * @property string $kode
 * @property object $detail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Jenjang[] $jenjang
 * @property-read int|null $jenjang_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelas aktif()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kelas query()
 * @mixin \Eloquent
 */
class Kelas extends Model
{
    protected $table = 'kelas';

    protected $fillable = [
        // 'id',
        'nama',
        'kode',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{ syarat_instruktur: [] }',
    ];

    protected $casts = [
        'detail' => 'object',
    ];

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'nama' => 'required|string|max:255',
            'kode' => 'required|string|max:255',
            'detail' => 'present|array',
            'detail.syarat_instruktur' => 'present|array',
            'detail.syarat_instruktur.*' => 'required|string',
        ];
    }

    public function jenjang()
    {
        return $this->hasMany(Jenjang::class, 'id_kelas');
    }

    public function getListJalurAttribute(): array
    {
        return \app('setelan')->detail['jalur'][$this->id] ?? [];
    }
}
