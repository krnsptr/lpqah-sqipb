<?php

namespace App\Models;

use App\Models\AnggotaProfil;
use App\Models\Instruktur;
use App\Models\Jadwal;
use App\Models\Pengguna;
use App\Models\Santri;
use App\Services\AhawebsService;
use App\Traits\GetCountAnggotaTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Validation\Rule;

/**
 * App\Models\Anggota
 *
 * @property int $id
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string|null $foto
 * @property string|null $nomor_wa
 * @property string|null $nomor_induk_instruktur
 * @property string|null $nomor_induk_santri
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $foto_url
 * @property-read string $nomor_induk_instruktur_lengkap
 * @property-read string $nomor_induk_santri_lengkap
 * @property-read bool $is_email_verified
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Instruktur[] $instruktur
 * @property-read int|null $instruktur_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Jadwal[] $jadwal
 * @property-read int|null $jadwal_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\AnggotaProfil|null $profil
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Santri[] $santri
 * @property-read int|null $santri_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Anggota newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Anggota newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Anggota query()
 * @mixin \Eloquent
 */
final class Anggota extends Pengguna implements MustVerifyEmail
{
    use GetCountAnggotaTrait;

    protected $table = 'anggota';

    protected $fillable = [
        // 'id',
        'nama',
        'email',
        'password',
        'foto',
        'nomor_wa',
        // 'nomor_induk_instruktur',
        // 'nomor_induk_santri',
        // 'remember_token',
        // 'email_verified_at',
        // 'created_at',
        // 'updated_at',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'is_email_verified',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function getRules(): array
    {
        return array_merge(parent::$rules, [
            'email' => [
                'required',
                'email',
                Rule::unique('anggota')->ignore($this->id),
            ],
            'nomor_wa' => 'nullable|string|regex:' . AnggotaProfil::REGEX['nomor_hp'],
            'nomor_induk_instruktur' => [
                'nullable',
                'string',
                Rule::unique($this->table)->ignore($this->id),
            ],
            'nomor_induk_santri' => [
                'nullable',
                'string',
                Rule::unique($this->table)->ignore($this->id),
            ],
        ]);
    }

    public function save(array $options = []): bool
    {
        if ($this->isDirty('email') && \App::environment('production')) {
            $email = $this->email;
            $service = new AhawebsService();

            if (!$service->validateEmail($email)) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'email' => "Alamat email tidak valid: $email",
                ]);
            }
        }

        return parent::save($options);
    }

    public function hasVerifiedEmail(): bool
    {
        return true;
    }

    public function getIsEmailVerifiedAttribute(): bool
    {
        return !empty($this->email_verified_at);
    }

    public function getNomorIndukInstrukturLengkapAttribute(): string
    {
        if (!$this->nomor_induk_instruktur) return '';

        return "NII.{$this->nomor_induk_instruktur}";
    }

    public function getNomorIndukSantriLengkapAttribute(): string
    {
        if (!$this->nomor_induk_santri) return '';

        return "NIS.{$this->nomor_induk_santri}";
    }

    public function isInstruktur(): bool
    {
        return $this->instruktur->count();
    }

    public function isSantri(): bool
    {
        return $this->santri->count();
    }

    public function isInstrukturBelumdites(): bool
    {
        return $this->instruktur()->belumDites()->count();
    }

    public function isSantriBelumdites(): bool
    {
        return $this->santri()->belumDites()->count();
    }

    public function isInstrukturSudahdites(): bool
    {
        return $this->instruktur()->sudahDites()->count();
    }

    public function isSantriSudahdites(): bool
    {
        return $this->santri()->sudahDites()->count();
    }

    public function profil()
    {
        return $this->hasOne(AnggotaProfil::class, 'id')->withDefault();
    }

    public function instruktur()
    {
        return $this->hasMany(Instruktur::class, 'id_anggota')->angkatanNow();
    }

    public function jadwal()
    {
        return $this->hasMany(Jadwal::class, 'id_anggota')->angkatanNow();
    }

    public function santri()
    {
        return $this->hasMany(Santri::class, 'id_anggota')->angkatanNow();
    }
}
