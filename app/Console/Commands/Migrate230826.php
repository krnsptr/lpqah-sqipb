<?php

namespace App\Console\Commands;

use App\Models\Jenjang;
use App\Models\Kelas;
use Illuminate\Console\Command;

class Migrate230826 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:230826';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add jenjang: Pemula Tahsin (THS--PEM)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $kelas = Kelas::where('kode', 'THS')->first();

        $jenjang = new Jenjang([
            'kode' => 'THS--PEM',
            'nama' => 'Pemula Tahsin',
        ]);

        $jenjang->id_kelas = $kelas->id;
        $jenjang->save();

        $this->info('Jenjang berhasil ditambahkan: ' . json_encode($jenjang));

        return 0;
    }
}
