<?php

namespace App\Services;

class AhawebsService
{
    protected $api_key;
    protected $api_url;
    protected $client;

    public function __construct()
    {
        $this->api_key = \config('ahawebs.api_key');
        $this->api_url = \config('ahawebs.api_url');

        $this->client = new \GuzzleHttp\Client([
            'base_uri' => $this->api_url,
        ]);
    }

    public function validateEmail($email): bool
    {
        $go_endpoint = '/go/aha-smtp-validator';

        $go_response = $this->client->get($go_endpoint, [
            'allow_redirects' => false,
        ]);

        if ($request_url = $go_response->getHeader('Location')[0] ?? null) {
            $response = $this->client->post($request_url, [
                'body' => json_encode([
                    'key' => $this->api_key,
                    'email' => $email,
                ]),
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ],
            ]);

            $json = json_decode($response->getBody(), true);

            if ($json['valid'] ?? false) {
                return true;
            }
        }

        return false;
    }
}
