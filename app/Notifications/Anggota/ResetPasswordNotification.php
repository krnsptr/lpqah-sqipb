<?php

namespace App\Notifications\Anggota;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class ResetPasswordNotification extends ResetPassword
{
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $expire = Config::get('auth.verification.expire', 60);

        $url = URL::route('anggota.password.reset', [
            'token' => $this->token,
            'email' => $notifiable->getEmailForPasswordReset(),
        ]);

        return (new MailMessage())
        ->subject('Reset Kata Sandi')
        ->greeting("Assalamu`alaikum, {$notifiable->nama}!")
        ->line('Silakan klik tombol berikut untuk mengatur ulang kata sandi.')
        ->action('Reset Kata Sandi', $url)
        ->line("Tautan ini akan kedaluwarsa dalam $expire menit.");
    }
}
