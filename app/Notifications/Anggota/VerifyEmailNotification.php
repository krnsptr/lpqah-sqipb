<?php

namespace App\Notifications\Anggota;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmailNotification extends VerifyEmail
{
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
        ->subject('Verifikasi Email')
        ->greeting("Assalamu`alaikum, {$notifiable->nama}!")
        ->line('Silakan klik tombol berikut untuk melakukan verifikasi email.')
        ->action('Verifikasi Email', $this->verificationUrl($notifiable));
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        $expire = Config::get('auth.verification.expire', 60);

        return URL::temporarySignedRoute(
            'anggota.email.verifikasi.cek',
            Carbon::now()->addMinutes($expire),
            [
                'id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForVerification()),
            ]
        );
    }
}
