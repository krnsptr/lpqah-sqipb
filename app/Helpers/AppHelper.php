<?php

use App\Models\Admin;
use App\Models\Anggota;
use Carbon\Carbon;

const ANGKATAN_FIRST_DATE = '2011-07-01';

if (!function_exists('admin')) {
    /**
     * Get authenticated admin
     *
     * @return Admin|null $admin
     */
    function admin(): ?Admin
    {
        return \auth('admin')->user();
    }
}

if (!function_exists('anggota')) {
    /**
     * Get authenticated anggota
     *
     * @return Anggota|null $anggota
     */
    function anggota(): ?Anggota
    {
        return \auth('anggota')->user();
    }
}

if (!function_exists('angkatan')) {

    /**
     * Hitung nomor angkatan berdasarkan tanggal
     *
     * @param $date
     * @return int $angkatan
     */
    function angkatan($date = null): int
    {
        $date = $date ? \Carbon\Carbon::parse($date) : \now();
        $date = $date->startOfQuarter();

        $angkatan = ($date->diffInQuarters(ANGKATAN_FIRST_DATE) / 2) + 1;

        return $angkatan;
    }
}

if (!function_exists('angkatan_date')) {
    /**
     * Hitung tanggal berdasarkan nomor angkatan
     *
     * @param $angkatan
     * @return Carbon $date
     */
    function angkatan_date($angkatan = null): Carbon
    {
        $angkatan ??= angkatan();

        $date = Carbon::parse(ANGKATAN_FIRST_DATE)->addQuartersNoOverflow(
            ($angkatan - 1) * 2
        )->startOfQuarter();

        return $date;
    }
}

if (!function_exists('format_date')) {
    function format_date($date = null): ?string
    {
        if (!($date instanceof Carbon)) return null;

        return $date->isoFormat('D MMMM YYYY');
    }
}

if (!function_exists('format_datetime')) {
    function format_datetime($date = null): ?string
    {
        if (!($date instanceof Carbon)) return null;

        return "{$date->isoFormat('D MMMM YYYY')}, {$date->format('H.i')}";
    }
}

if (!function_exists('format_time')) {
    function format_time($date = null): ?string
    {
        if (!($date instanceof Carbon)) return null;

        return "{$date->format('H:i')}";
    }
}

if (!function_exists('format_number')) {
    function format_number($number): ?string
    {
        if ($number === null) return null;

        return number_format($number, 0, ',', '.');
    }
}

if (!function_exists('format_rupiah')) {
    function format_rupiah($number): ?string
    {
        if ($number === null) return null;

        return 'Rp' . format_number($number);
    }
}
