<?php

namespace App\Enums;

use App\Enum;

/**
 * @method static static Ahad()
 * @method static static Senin()
 * @method static static Selasa()
 * @method static static Rabu()
 * @method static static Kamis()
 * @method static static Jumat()
 * @method static static Sabtu()
 */
final class Hari extends Enum
{
    public const Ahad = 1;
    public const Senin = 2;
    public const Selasa = 3;
    public const Rabu = 4;
    public const Kamis = 5;
    public const Jumat = 6;
    public const Sabtu = 7;
}
