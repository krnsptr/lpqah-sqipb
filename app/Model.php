<?php

namespace App;

class Model extends \Eloquent
{
    protected function getRules(): array
    {
        return [];
    }

    protected function getData(): array
    {
        return $this->getAttributes();
    }

    public function save(array $options = []): bool
    {
        \Validator::make($this->getData(), $this->getRules())->validate();

        return parent::save($options);
    }
}
