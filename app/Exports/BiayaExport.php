<?php

namespace App\Exports;

use App\Models\Santri;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class BiayaExport extends DefaultValueBinder implements
    FromView,
    ShouldAutoSize,
    WithCustomValueBinder,
    WithTitle
{
    public function view(): View
    {
        $list_santri = Santri::angkatanNow()
            ->orderBy('id_anggota')
            ->where(function ($q) {
                $q->whereNotNull('id_kelompok')->orWhereHas('biaya');
            })
            ->get();

        $list_santri->load([
            'anggota:id,nama,email',
            'anggota.profil:id,jenis_kelamin',
            'kelas:id,nama',
            'jenjang:id,nama',
            'biaya',
        ]);

        return \view('exports.biaya', \compact(
            'list_santri',
        ));
    }

    public function title(): string
    {
        return \app('setelan')->detail['biaya']['label'];
    }

    public function bindValue(Cell $cell, $value)
    {
        if (ctype_digit($value) && strlen($value) >= 10) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        return parent::bindValue($cell, $value);
    }
}
