<?php

namespace App\Exports;

use App\Models\Santri;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class SantriExport extends DefaultValueBinder implements
    FromView,
    ShouldAutoSize,
    WithCustomValueBinder,
    WithTitle
{
    public function view(): View
    {
        $list_santri = Santri::angkatanNow()->get();

        $list_santri->load([
            'anggota.profil.provinsi:id,nama',
            'anggota.profil.kota:id,nama',
            'anggota.profil.bank:id,nama',
            'kelas:id,nama',
            'jenjang:id,nama',
            'kelompok:id,angkatan,nomor',
        ]);

        return \view('exports.santri', \compact(
            'list_santri',
        ));
    }

    public function title(): string
    {
        return 'Santri';
    }

    public function bindValue(Cell $cell, $value)
    {
        if (ctype_digit($value) && strlen($value) >= 10) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        return parent::bindValue($cell, $value);
    }
}
