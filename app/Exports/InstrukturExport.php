<?php

namespace App\Exports;

use App\Models\Instruktur;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class InstrukturExport extends DefaultValueBinder implements
    FromView,
    ShouldAutoSize,
    WithCustomValueBinder,
    WithTitle
{
    public function view(): View
    {
        $list_instruktur = Instruktur::angkatanNow()->get();

        $list_instruktur->load([
            'anggota.profil.provinsi:id,nama',
            'anggota.profil.kota:id,nama',
            'anggota.profil.bank:id,nama',
            'anggota.jadwal:id,id_anggota',
            'kelas:id,nama',
            'jenjang:id,nama',
        ]);

        return \view('exports.instruktur', \compact(
            'list_instruktur',
        ));
    }

    public function title(): string
    {
        return 'Instruktur';
    }

    public function bindValue(Cell $cell, $value)
    {
        if (ctype_digit($value) && strlen($value) >= 10) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        return parent::bindValue($cell, $value);
    }
}
