<?php

namespace App\Exports;

use App\Models\Kelompok;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class KelompokExport implements
    FromView,
    ShouldAutoSize,
    WithTitle
{
    public function view(): View
    {
        $list_kelompok = Kelompok::angkatanNow()->get();

        $list_kelompok->load([
            'jenjang:id,nama',
            'jenjang.kelas:id,nama',
            'jadwal:id,hari,waktu_mulai',
            'instruktur.anggota.profil:id,jenis_kelamin,nomor_identitas,nomor_wa,nomor_hp',
            'santri.anggota.profil:id,nomor_identitas,nomor_wa,nomor_hp',
        ]);

        $list_kelompok = $list_kelompok->sortBy(function ($kelompok) {
            $jenis_kelamin = $kelompok->instruktur->anggota->profil->jenis_kelamin;
            $id_jenjang = $kelompok->jenjang->id;
            $hari = $kelompok->jadwal->hari;
            $waktu = $kelompok->jadwal->waktu_mulai;

            return $jenis_kelamin . $id_jenjang . $hari . $waktu;
        });

        return \view('exports.kelompok', \compact(
            'list_kelompok',
        ));
    }

    public function title(): string
    {
        return 'Kelompok';
    }
}
