<?php

namespace App\Providers;

use App\Models\Setelan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class SetelanServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Setelan::class, function () {
            if (!Schema::hasTable('setelan')) {
                return new Setelan();
            }

            return Setelan::angkatanNow()->firstOrNew();
        });

        $this->app->alias(Setelan::class, 'setelan');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Setelan $setelan)
    {
        View::share('setelan', $setelan);
    }
}
