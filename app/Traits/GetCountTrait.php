<?php

namespace App\Traits;

trait GetCountTrait
{
    public static function getCount($jenis_kelamin, $id_kelas, $id_jenjang): int
    {
        $query = static::angkatanNow();

        if ($jenis_kelamin !== 'ALL') {
            $query->whereHas('anggota.profil', function ($q) use ($jenis_kelamin) {
                $q->where('jenis_kelamin', $jenis_kelamin);
            });
        }

        if ($id_kelas !== 'ALL') {
            if ($id_kelas === 'NOT_NULL') {
                $query->whereNotNull('id_kelas');
            } else {
                $query->where('id_kelas', $id_kelas);
            }
        }

        if ($id_jenjang !== 'ALL') {
            if ($id_jenjang === 'NOT_NULL') {
                $query->whereNotNull('id_jenjang');
            } else {
                $query->where('id_jenjang', $id_jenjang);
            }
        }

        return $query->count();
    }
}
