<?php

namespace App\Traits;

trait GetCountAnggotaTrait
{
    public static function getCount($type = 'ALL', $gender = 'ALL'): int
    {
        $query = static::query();

        switch ($type) {
            case 'instruktur':
                $query->whereHas('instruktur', function ($q) {
                    $q->sudahDites();
                });
                break;

            case 'santri':
                $query->whereHas('santri', function ($q) {
                    $q->sudahDites();
                });
                break;

            case 'calon_instruktur':
                $query->whereHas('instruktur', function ($q) {
                    $q->belumDites();
                });
                break;

            case 'calon_santri':
                $query->whereHas('santri', function ($q) {
                    $q->belumDites();
                });
                break;

            case 'belum_daftar':
                $setelan = \app('setelan');
                $angkatan = $setelan->angkatan;
                $angkatan_start = \angkatan_date($angkatan);
                $angkatan_end = \angkatan_date($angkatan + 1);

                $query->doesntHave('santri')->doesntHave('instruktur')
                ->whereBetween('updated_at', [$angkatan_start, $angkatan_end]);

                break;

            default:
                break;
        }

        if ($gender !== 'ALL') {
            $query->whereHas('profil', function ($q) use ($gender) {
                $q->where('jenis_kelamin', $gender);
            });
        }

        return $query->count(\DB::raw('DISTINCT(id)'));
    }
}
