@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Penjadwalan</h2>
        </div>

        <div class="card-body">
          @messages

          @if ($list_santri->count())
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div class="card shadow-sm h-100">
                  <div class="card-header">
                    <h4 class="mb-0">Penjadwalan Santri</h4>
                  </div>
                  <div class="card-body">
                    @if (!$setelan->isDibuka('penjadwalan_santri'))
                      Penjadwalan santri sedang ditutup.
                    @else
                      <div class="row">
                        @foreach ($list_santri as $santri)
                          <div class="col-lg-4 col-md-6 mx-auto my-3">
                            <div class="text-center font-weight-bold">
                              Kelas {{ $santri->kelas->nama }}
                            </div>
                            <hr class="my-3">
                            <div class="row my-1">
                              <div class="col-6 font-weight-bold text-right">
                                Jenjang:
                              </div>
                              <div class="col-6">
                                {{ $santri->jenjang->nama ?? '(Belum dites)' }}
                              </div>
                            </div>
                            <div class="row my-1">
                              <div class="col-6 font-weight-bold text-right">
                                Jalur:
                              </div>
                              <div class="col-6">
                                {{ $santri->kelompok->jadwal->jalur_text ?? '-' }}
                              </div>
                            </div>
                            <div class="row my-1">
                              <div class="col-6 font-weight-bold text-right">
                                Kelompok:
                              </div>
                              <div class="col-6">
                                @if ($kelompok = $santri->kelompok)
                                  <span class="text-monospace">
                                    {{ $kelompok->nomor_reg_lengkap }}
                                  </span>
                                @else
                                  -
                                @endif
                              </div>
                            </div>
                            <div class="row my-1">
                              <div class="col-6 font-weight-bold text-right">
                                Jadwal:
                              </div>
                              <div class="col-6">
                                @if ($jadwal = $santri->kelompok->jadwal ?? false)
                                  {{ $jadwal->getText() }}
                                @else
                                  -
                                @endif
                              </div>
                            </div>
                            <hr class="my-3">
                            @if ($santri->id_jenjang)
                              <form method="post" autocomplete="off"
                                action="{{ route('anggota.santri_penjadwalan', $santri) }}">
                                @csrf

                                <div class="form-group">
                                  <label>Pilih jalur</label>
                                  <select class="form-control" name="jalur">
                                    <option value="">Reguler</option>
                                    @foreach ($santri->kelas->list_jalur as $jalur)
                                      <option value="{{ $jalur }}"
                                      @if (($santri->kelompok->jadwal->jalur ?? null) === $jalur)
                                        selected
                                      @endif>
                                        {{ $jalur }}
                                      </option>
                                    @endforeach
                                  </select>
                                </div>

                                <div class="form-group">
                                  <label>Pilih kelompok</label>
                                  <select class="form-control" name="id_kelompok"
                                    data-id_santri="{{ $santri->id }}"
                                    data-id_kelompok="{{ $santri->id_kelompok }}"
                                    @if (!$santri->id_jenjang) disabled @endif>
                                    <option value="">(Belum dipilih)</option>
                                  </select>
                                </div>
                                <div class="row">
                                  <div class="col-lg-6 mx-auto">
                                    <button type="submit" class="btn btn-block btn-default">
                                      Pilih
                                    </button>
                                  </div>
                                </div>
                              </form>
                              <br>
                            @else
                              <div class="text-center">
                                <p>Ups!</p>
                                <p>Kamu belum mengikuti tes santri {{ $santri->kelas->nama }}.</p>
                                <p class="mt-4 py-3">
                                  Belum boleh penjadwalan ya!
                                </p>
                              </div>
                            @endif
                          </div>
                        @endforeach
                      </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          @endif

          @if ($list_instruktur->count())
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div class="card shadow-sm h-100">
                  <div class="card-header">
                    <h4 class="mb-0">Penjadwalan Instruktur</h4>
                  </div>
                  <div class="card-body">
                    @if (!$setelan->isDibuka('penjadwalan_instruktur'))
                      Penjadwalan instruktur sedang ditutup.
                    @else
                      <div class="mb-4">
                        <h4>Kapasitas Membina</h4>
                        <div>
                          <small>
                            Dalam satu pekan, kamu dapat membina berapa kelompok?
                            (1 kelompok maksimum {{ $kuota }} santri)
                          </small>
                        </div>
                      </div>
                      <form action="{{ route('anggota.instruktur_penjadwalan') }}" method="post">
                        @csrf
                        @foreach ($list_instruktur as $instruktur)
                          <div class="row py-1">
                            <label class="col-md-6 col-lg-3 col-form-label text-md-right">
                              {{ $instruktur->kelas->nama }}
                            </label>
                            <div class="col-md-6 col-lg-3">
                              <div class="input-group">
                                <input type="number" class="form-control"
                                  name="kapasitas_membina[{{ $instruktur->id }}]"
                                  value="{{ $instruktur->detail->kapasitas_membina }}"
                                  min="1" max="5" required>
                                <div class="input-group-append">
                                  <span class="input-group-text bg-secondary">
                                    kelompok
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        @endforeach
                        <br>
                        <div class="row">
                          <div class="col-md-6 col-lg-3"></div>
                          <div class="col-md-6 col-lg-3">
                            <button type="submit" class="btn btn-block btn-default">
                              Simpan
                            </button>
                          </div>
                        </div>
                      </form>

                      <hr>

                      <div class="mb-4 d-flex flex-row justify-content-between">
                        <h4 class="mb-0">Alternatif Jadwal</h4>
                        <a href="{{ route('anggota.jadwal_tambah') }}"
                          class="btn btn-sm btn-success">
                          Tambah
                        </a>
                      </div>
                      <div class="table-responsive">
                        <table class="table stack">
                          <thead class="thead-light">
                            <tr>
                              <th>No.</th>
                              <th>Hari</th>
                              <th>Waktu</th>
                              <th>Jalur</th>
                              @auth ('admin')
                                <th>Kelompok</th>
                                <th>Jenjang</th>
                              @endauth
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            @forelse ($list_jadwal as $jadwal)
                              <tr>
                                <td>{{ $loop->iteration }}.</td>
                                <td>{{ $jadwal->hari->description }}</td>
                                <td>
                                  {{ $jadwal->waktu_mulai->format('H:i') }}
                                  WIB
                                </td>
                                <td>{{ $jadwal->jalur_text }}</td>
                                @auth ('admin')
                                  <td>
                                    {{ $jadwal->kelompok->nomor_reg_lengkap ?? '-' }}
                                  </td>
                                  <td>
                                    {{ $jadwal->kelompok->jenjang->nama ?? '-' }}
                                  </td>
                                @endauth
                                <td>
                                  <a href="{{ route('anggota.jadwal_edit', $jadwal) }}"
                                    class="btn btn-sm btn-default">
                                    Edit
                                  </a>
                                  <a href="{{ route('anggota.jadwal_hapus', $jadwal) }}"
                                    class="btn btn-sm btn-danger">
                                    Hapus
                                  </a>
                                  @auth ('admin')
                                    @if ($jadwal->kelompok)
                                      <a href="javascript:hapus_kelompok({{ $jadwal->id }})"
                                        class="btn btn-sm btn-warning">
                                        Hapus Kelompok
                                      </a>
                                    @else
                                      <a href="javascript:tambah_kelompok({{ $jadwal->id }})"
                                        class="btn btn-sm btn-success">
                                        Tambah Kelompok
                                      </a>
                                    @endif
                                  @endauth
                                </td>
                              </tr>
                            @empty
                              <tr>
                                <td colspan="5">
                                  Kamu belum menambah alternatif jadwal.
                                </td>
                              </tr>
                            @endforelse
                          </tbody>
                        </table>
                      </div>
                      <div class="mt-2">
                        <small>
                          Kamu bisa menambahkan beberapa alternatif jadwal kosong.<br>
                          Admin akan memilih satu atau lebih jadwal tersebut
                          untuk dialokasikan sesuai kemampuanmu.<br>
                          Dengan mengisi formulir penjadwalan ini,
                          kamu siap mengajar pada jadwal tesebut.
                        </small>
                      </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

@auth ('admin')
  @php
    $list_jenjang = \collect();

    foreach ($list_instruktur as $instruktur) {
        if ($instruktur->id_jenjang === null) continue;

        $kode_jenjang = $instruktur->jenjang->kode;
        $kelas = $instruktur->jenjang->kelas;

        foreach ($kelas->jenjang()->where('kode', '<=', $kode_jenjang)->get() as $jenjang) {
          $list_jenjang->push($jenjang);
        }
    }

    $list_jenjang_option = $list_jenjang->map(function ($jenjang) use ($list_instruktur) {
        $instruktur = $list_instruktur->firstWhere('id_kelas', $jenjang->id_kelas);

        return [
          'id_jenjang' => $jenjang->id,
          'id_instruktur' => $instruktur->id,
          'text' => $jenjang->nama,
        ];
    });
  @endphp

  <form autocomplete="off" method="post" action="{{ route('admin.penjadwalan_edit') }}">
    @csrf

    <div class="modal" tabindex="-1" role="dialog" id="hapus_kelompok-modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Hapus Kelompok</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Apakah kamu yakin?</p>
            <input type="hidden" name="aksi" value="hapus_kelompok">
            <input type="hidden" name="id_jadwal" value="">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-warning">Hapus</button>
          </div>
        </div>
      </div>
    </div>
  </form>

  <form autocomplete="off" method="post" action="{{ route('admin.penjadwalan_edit') }}">
    @csrf
    <div class="modal" tabindex="-1" role="dialog" id="tambah_kelompok-modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Tambah Kelompok</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" name="aksi" value="tambah_kelompok">
            <input type="hidden" name="id_jadwal" value="">
            <input type="hidden" name="id_instruktur" id="id_instruktur" value="">

            <div class="form-group row">
              <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                Jenjang
              </label>
              <div class="col-md-8 col-xl-7">
                <select class="form-control" name="id_jenjang" id="id_jenjang" required>
                  <option value=""></option>
                  @foreach ($list_jenjang_option as $option)
                    <option value="{{ $option['id_jenjang'] }}"
                      data-id_instruktur={{ $option['id_instruktur'] }}>
                      {{ $option['text'] }}
                    </option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Tambah</button>
          </div>
        </div>
      </div>
    </div>
  </form>
@endauth

@endsection

@push ('css')
  <link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet">
  <link href="{{ asset('assets/css/style-select2.css') }}" rel="stylesheet">
@endpush

@push ('js')
  <script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="{{ asset('assets/js/select2-cascade.js') }}"></script>
  <script src="{{ asset('assets/js/stacktable.js') }}"></script>
  <script>
    $('select[name=id_kelompok]').each(function () {
      var select = $(this);
      var selectJalur = $(this).parent().parent().find('select[name=jalur]');
      var id_santri = $(this).attr('data-id_santri');
      var id_kelompok = $(this).attr('data-id_kelompok');

      new Select2Cascade(
        selectJalur,
        select,
        @json(route('anggota.santri_penjadwalan', '')) + '/' + id_santri + '/?jalur=:parentId:'
      ).then(function(parent, child) {
        setTimeout(function() {
          child.val(id_kelompok).change();
        }, 200);
      });

      select.select2();
      selectJalur.change();
    });

    $('.table.stack').stacktable({
      displayHeader: false,
    });

    $('.stacktable.large-only').addClass('d-none d-md-table');
    $('.stacktable.small-only').addClass('d-table d-md-none');
    $('.stacktable.small-only .st-head-row').addClass('pt-4 pb-2');
    $('.stacktable th').addClass('bg-secondary');
    $('.stacktable .st-head-row').css('white-space', 'normal');
  </script>
@endpush

@auth ('admin')
  @push ('js')
    <script>
      function hapus_kelompok(id_jadwal) {
        modal = $('#hapus_kelompok-modal');
        modal.find('input[name=id_jadwal]').val(id_jadwal);
        modal.modal();
      }

      function tambah_kelompok(id_jadwal) {
        modal = $('#tambah_kelompok-modal');
        modal.find('input[name=id_jadwal]').val(id_jadwal);
        modal.modal();
      }

      $('#id_jenjang').change(function () {
        option = $(this).find(':selected');
        $('#id_instruktur').val(option.attr('data-id_instruktur'));
      });
    </script>
  @endpush
@endauth
