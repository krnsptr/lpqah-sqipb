@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-md-8 col-lg-6 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Pendaftaran Program</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-12 pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Daftar sebagai
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="sebagai" name="sebagai" required>
                      @php $sebagai = (old('sebagai') ?? request()->input('sebagai')) @endphp
                      <option value=""></option>
                      @if ($setelan->isDibukaPendaftaran('santri'))
                        <option value="santri" @if ($sebagai === 'santri') selected @endif>
                          Santri
                        </option>
                      @endif
                      @if ($setelan->isDibukaPendaftaran('instruktur'))
                        <option value="instruktur" @if ($sebagai === 'instruktur') selected @endif>
                          Instruktur
                        </option>
                      @endif
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_kelas" name="id_kelas" required>
                      <option value=""></option>
                      @foreach (App\Models\Kelas::all() as $kelas)
                        <option value="{{ $kelas->id }}"
                          @if (old('id_kelas') == $kelas->id) selected @endif>
                          {{ $kelas->nama }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto">
                <button type="submit" class="btn btn-block btn-primary">
                  Daftar
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
