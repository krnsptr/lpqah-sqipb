@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Pendaftaran Santri</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nama" name="nama"
                      value="{{ anggota()->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Alamat email
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="email" class="form-control" id="email" name="email"
                      value="{{ anggota()->email }}" disabled>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nama_kelas" name="nama_kelas"
                      value="{{ $kelas->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jenis pendaftaran
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="detail--pendaftaran"
                      name="detail[pendaftaran]" required>
                      <option value=""></option>
                      @foreach (App\Models\Santri::DETAIL_PENDAFTARAN as $key => $value)
                        <option value="{{ $key }}"
                          @if (old('detail.pendaftaran') == $key) selected @endif>
                          {{ $value }}
                        </option>
                      @endforeach
                    </select>
                    <small>
                      Pendaftaran ulang khusus untuk santri lama
                      (pernah belajar {{ $kelas->nama }} di LPQ Al Hurriyyah)
                    </small>
                  </div>
                </div>
              </div>
            </div>

            <div class="row no-gutters" id="daftar_ulang" style="display: none;">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Angkatan sebelumnya
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="detail--angkatan_sebelumnya"
                      name="detail[angkatan_sebelumnya]" required>
                      <option value=""></option>
                      @for ($i = \angkatan() - 1; $i > 0; $i--)
                        <option value="{{ $i }}"
                          @if (old('detail.angkatan_sebelumnya') == $i) selected @endif>
                          {{ $i }}
                          &mdash;
                          {{ \angkatan_date($i)->startOfQuarter()->monthName }} s.d.
                          {{ \angkatan_date($i)->addQuarter()->endOfQuarter()->monthName }}
                          {{ \angkatan_date($i)->format('Y') }}
                        </option>
                      @endfor
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jenjang sebelumnya
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <select class="form-control" id="detail--lulus_jenjang_sebelumnya"
                        name="detail[lulus_jenjang_sebelumnya]" required>
                          <option value=""></option>
                          @foreach (App\Models\Santri::DETAIL_LULUS_JENJANG as $key => $value)
                            <option value="{{ $key }}"
                              @if (old('detail.lulus_jenjang_sebelumnya') === (string) $key)
                                selected
                              @endif>
                              {{ $value }}
                            </option>
                          @endforeach
                        </select>
                      </div>

                      <select class="form-control" id="detail--id_jenjang_sebelumnya"
                        name="detail[id_jenjang_sebelumnya]" required>
                        <option value=""></option>
                        @foreach ($kelas->jenjang as $jenjang)
                          <option value="{{ $jenjang->id }}"
                            @if (old('detail.id_jenjang_sebelumnya') == $jenjang->id)
                              selected
                            @endif>
                            {{ $jenjang->nama }}
                          </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto">
                <button type="submit" class="btn btn-block btn-primary">
                  Daftar
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
  <script>
    $('#detail--pendaftaran').change(function () {
      if ($(this).val() === 'ulang') {
        $('#daftar_ulang').find(':input').prop({ required: true, disabled: false });
        $('#daftar_ulang').fadeIn();
      } else {
        $('#daftar_ulang').find(':input').prop({ required: false, disabled: true });
        $('#daftar_ulang').fadeOut();
      }
    }).change();
  </script>
@endpush
