@extends ('layouts.argon.dashboard')

@php

$text_jenjang_sebelumnya = $santri->detail->nama_jenjang_sebelumnya ?? '';

if (isset($santri->detail->nama_jenjang_sebelumnya)) {
  $lulus = $santri->detail->lulus_jenjang_sebelumnya;
  $text_lulus = $santri::DETAIL_LULUS_JENJANG[$lulus] ?? '';
  $text_jenjang_sebelumnya .= " — $text_lulus";
}

@endphp

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Santri: NRS.{{ $santri->nomor_reg }}</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor registrasi
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext text-monospace"
                      value="{{ $santri->nomor_reg_lengkap }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nama" name="nama"
                      value="{{ $santri->anggota->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Alamat email
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="email" class="form-control-plaintext" id="email" name="email"
                      value="{{ $santri->anggota->email }}" disabled>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext"
                      value="{{ $santri->kelas->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jenjang
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext"
                      value="{{ $santri->jenjang->nama ?? '(Belum dites)' }}" disabled>
                  </div>
                </div>

                @isset ($santri->detail->pendaftaran)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Jenis pendaftaran
                    </label>
                    <div class="col-md-8 col-xl-7">
                      <input type="text" class="form-control-plaintext"
                        value="{{ $santri::DETAIL_PENDAFTARAN[$santri->detail->pendaftaran] }}"
                        disabled>
                    </div>
                  </div>
                @endisset
              </div>
            </div>

            @if (($santri->detail->pendaftaran ?? '') === 'ulang') <hr class="mt-0"> @endif

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                @isset ($santri->detail->angkatan_sebelumnya)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Angkatan sebelumnya
                    </label>
                    <div class="col-md-8 col-xl-7">
                      <input type="text" class="form-control-plaintext"
                        value="{{ $santri->detail->angkatan_sebelumnya }}" disabled>
                    </div>
                  </div>
                @endisset
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                @isset ($santri->detail->id_jenjang_sebelumnya)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Jenjang sebelumnya
                    </label>
                    <div class="col-md-8 col-xl-7">
                      <input type="text" class="form-control-plaintext"
                        value="{{ $text_jenjang_sebelumnya }}" disabled>
                    </div>
                  </div>
                @endisset
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto">
                <a href="{{ route('anggota.dasbor') }}" class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
