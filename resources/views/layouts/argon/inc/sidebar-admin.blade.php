<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
  <div class="container-fluid">

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse"
      data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Brand -->
    <a class="navbar-brand py-0" href="{{ url('/') }}">
      <img src="{{ asset('assets/img/brand/logo.png') }}" class="navbar-brand-img"
        alt="{{ config('app.name') }}">
    </a>

    <!-- User -->
    <ul class="nav align-items-center d-md-none">
      <li class="nav-item dropdown">
        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          <div class="media align-items-center">
            <span class="avatar avatar-sm rounded-circle">
              <img alt="{{ auth('admin')->user()->nama }}"
                src="{{ asset('assets/img/user.png') }}"
                class="p-1 bg-secondary">
            </span>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
          <a href="{{ route('admin.keluar') }}" class="dropdown-item"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
            <span>Keluar</span>
          </a>
        </div>
      </li>
    </ul>

    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
      <div class="navbar-collapse-header d-md-none">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('/') }}">
              <img src="{{ asset('assets/img/brand/logo.png') }}">
            </a>
          </div>
          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
              aria-label="Toggle sidenav">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navigation -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link @ifroute('admin.dasbor') active @endifroute"
            href="{{ route('admin.dasbor') }}">
            <i class="fas fa-columns fa-fw mr-1"></i> Dasbor
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.statistik') active @endifroute"
            href="{{ route('admin.statistik') }}">
            <i class="fas fa-chart-pie fa-fw mr-1"></i> Statistik
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.instruktur') active @endifroute"
            href="{{ route('admin.instruktur') }}">
            <i class="fas fa-chalkboard-teacher fa-fw mr-1"></i> Instruktur
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.santri') active @endifroute"
            href="{{ route('admin.santri') }}">
            <i class="fas fa-user-edit fa-fw mr-1"></i> Santri
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.penjadwalan') active @endifroute"
            href="{{ route('admin.penjadwalan') }}">
            <i class="fas fa-calendar-plus fa-fw mr-1"></i> Penjadwalan
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.biaya') active @endifroute"
            href="{{ route('admin.biaya') }}">
            <i class="fas fa-receipt fa-fw mr-1"></i>
            {{ $setelan->detail['biaya']['label'] }}
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.anggota') active @endifroute"
            href="{{ route('admin.anggota') }}">
            <i class="fas fa-user fa-fw mr-1"></i> Anggota
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.unduh') active @endifroute"
            href="{{ route('admin.unduh') }}">
            <i class="fas fa-download fa-fw mr-1"></i> Unduh Data
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.setelan') active @endifroute"
            href="{{ route('admin.setelan') }}">
            <i class="fas fa-wrench fa-fw mr-1"></i>
            Setelan
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
