<div class="container-fluid mt-auto">
  <footer class="footer py-3">
    <div class="row align-items-center justify-content-xl-between">
      <div class="col-xl-6 my-1">
        <div class="copyright text-center text-xl-left text-muted">
          &copy; {{ now()->year }}
          <a href="https://www.lpqipb.com" target="_blank">
            LPQ Al-Hurriyyah IPB
          </a>
        </div>
      </div>
      <div class="col-xl-6 my-1">
        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
          <li class="nav-item text-sm">
            Dikembangkan oleh Hamba Allah
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>
