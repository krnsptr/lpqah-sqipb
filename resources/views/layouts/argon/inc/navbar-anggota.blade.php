<nav id="navbar-main" class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
  <div class="container px-4 px-lg-0">
    <!-- Brand -->
    <a class="navbar-brand mr-lg-5" href="{{ url('/') }}">
      <img src="{{ asset('assets/img/brand/logo.png') }}" alt="{{ config('app.name') }}">
    </a>

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global"
      aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapse header -->
    <div class="navbar-collapse collapse" id="navbar_global">
      <div class="navbar-collapse-header">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('/') }}">
              <img src="{{ asset('assets/img/brand/logo.png') }}" alt="{{ config('app.name') }}">
            </a>
          </div>

          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false"
              aria-label="Toggle navigation">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navbar items -->
      <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
        <li class="nav-item">
          <a href="{{ route('anggota.dasbor') }}"
            class="nav-link @ifroute('anggota.dasbor') active @endifroute">
            <i class="fas fa-columns fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Dasbor</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('anggota.program_penjadwalan') }}"
            class="nav-link @ifroute('anggota.program_penjadwalan') active @endifroute">
            <i class="far fa-calendar-plus fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Penjadwalan</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('anggota.kelompok') }}"
            class="nav-link @ifroute('anggota.kelompok') active @endifroute">
            <i class="fas fa-users fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Kelompok</span>
          </a>
        </li>
        @if (\anggota()->isSantriSudahDites())
          <li class="nav-item">
            <a href="{{ route('anggota.biaya') }}"
              class="nav-link @ifroute('anggota.biaya') active @endifroute">
              <i class="fas fa-receipt fa-fw mr-1"></i>
              <span class="nav-link-inner--text">
                {{ $setelan->detail['biaya']['label'] }}
              </span>
            </a>
          </li>
        @endif
        <li class="nav-item">
          <a href="{{ route('anggota.jadwal_cari') }}"
            class="nav-link @ifroute('anggota.jadwal_cari') active @endifroute">
            <i class="fas fa-search fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Cari Jadwal</span>
          </a>
        </li>
        <li class="nav-item d-md-none">
          <a href="{{ route('anggota.profil') }}"
            class="nav-link @ifroute('anggota.profil') active @endifroute">
            <i class="fas fa-user fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Edit Profil</span>
          </a>
        </li>
        <li class="nav-item d-md-none">
          <a href="{{ route('anggota.keluar') }}"
            class="nav-link @ifroute('anggota.keluar') active @endifroute"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Keluar</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav align-items-center d-none d-md-flex ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="{{ auth()->user()->nama }}"
                  src="{{ asset('assets/img/user.png') }}">
              </span>
              <div class="media-body ml-2 d-none d-lg-block">
                <span class="mb-0 text-sm font-weight-bold">
                  {{ auth()->user()->nama }}
                </span>
              </div>
            </div>
          </a>

          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <a href="{{ route('anggota.profil') }}" class="dropdown-item">
              <i class="fas fa-user fa-fw mr-1"></i>
              <span>Edit Profil</span>
            </a>
            <a href="{{ route('anggota.keluar') }}" class="dropdown-item"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
              <span>Keluar</span>
            </a>
          </div>
        </li>
      </ul>

      <form method="post" action="{{ route('anggota.keluar') }}" id="logout-form"
        style="display: none;">
        @csrf
      </form>
    </div>
  </div>
</nav>
