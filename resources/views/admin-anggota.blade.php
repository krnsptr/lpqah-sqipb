@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header bg-gradient-primary py-3 pt-md-6">
  <div class="container-fluid">
    <div class="row">
      <div class="col px-1 px-md-0">
        <div class="card shadow">
          <div class="card-header">
            <h3 class="mb-0">
              Anggota
            </h3>
          </div>
          <div class="px-3">
            @messages
          </div>
          <div class="table-responsive">
            <table class="table datatables" id="table">
              <thead class="thead-light">
                <tr>
                  <th style="max-width: 6rem;">ID</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Status</th>
                  <th>Nomor Identitas</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <style>
    .w-4rem {
      width: 4rem !important;
    }
  </style>
@endpush

@push ('js')
  <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="//cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.4/jquery.dataTables.yadcf.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.35/dayjs.min.js"></script>
  <script>
    mapper = {};
    mapper['jenis_kelamin'] = @json(App\Models\AnggotaProfil::JENIS_KELAMIN);
    mapper['status'] = @json(App\Models\AnggotaProfil::STATUS);

    render = {};

    render['jenis_kelamin'] = function (data) {
      return mapper['jenis_kelamin'][data];
    };

    render['status'] = function (data) {
      return mapper['status'][data];
    };

    filter = {};
    filter['jenis_kelamin'] = [];
    filter['status'] = [];

    for (f in filter) {
      for (key in mapper[f]) {
        filter[f].push({ value: key, label: mapper[f][key] });
      }
    }
  </script>
  <script>
    aksi = '<button type="button" onclick="lihat(this)" class="btn btn-sm btn-default">Lihat</button>';

    function lihat(button) {
      id = $(button).closest('tr').attr('id');
      window.open('{{ route('admin.anggota_lihat', '') }}' + '/' + id);
    }

    table = $('#table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ route('admin.anggota') }}',
      dom: "<'row'<'col-lg-4 p-3'l>>rt<'row'<'col-lg-6 p-3'i><'col-lg-6 p-3'p>>",
      language: {
        paginate: {
          previous: '&laquo;',
          next: '&raquo;',
        }
      },
      lengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, 'All'],
      ],
      rowId: 'id',
      columnDefs: [
        { targets: '_all', searchable: false, orderable: false },
      ],
      columns: [
        {
          data: 'id',
          name: 'id',
          orderable: true,
          searchable: true,
        },
        {
          data: 'nama',
          name: 'nama',
          searchable: true,
        },
        {
          data: 'profil.jenis_kelamin',
          name: 'profil.jenis_kelamin',
          defaultContent: '-',
          searchable: true,
          render: render['jenis_kelamin'],
        },
        {
          visible: false,
          data: 'profil.status',
          name: 'profil.status',
          defaultContent: '-',
          searchable: true,
          render: render['status'],
        },
        {
          visible: false,
          data: 'profil.nomor_identitas',
          name: 'profil.nomor_identitas',
          defaultContent: '-',
          searchable: true,
        },
        {
          data: 'email',
          name: 'email',
          searchable: true,
        },
        {
          data: null,
          defaultContent: aksi,
          searchable: false,
          orderable: false,
        },
      ],
      order: [
        [0, 'asc'],
      ],
    });

    yadcf.init(table, [
      {
        column_number: 0,
        filter_type: 'text',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm w-4rem mt-1',
        filter_delay: 1000,
        filter_match_mode: 'exact',
      },
      {
        column_number: 1,
        filter_type: 'text',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        filter_delay: 1000,
      },
      {
        column_number: 2,
        filter_type: 'select',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        data: filter['jenis_kelamin'],
      },
      {
        column_number: 3,
        filter_type: 'select',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        data: filter['status'],
      },
      {
        column_number: 4,
        filter_type: 'text',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        filter_delay: 1000,
      },
      {
        column_number: 5,
        filter_type: 'text',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        filter_delay: 1000,
      },
    ]);
  </script>
@endpush
