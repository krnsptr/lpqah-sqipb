@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Kelompok</h2>
        </div>

        <div class="card-body" style="min-height: 52vh;">
          @messages

          @if (!$setelan->isDibuka('publikasi_jadwal'))
            Jadwal belum dipublikasikan.
          @elseif ($list_instruktur->count('kelompok') + $list_santri->count('kelompok') < 1)
            Kamu belum memiliki kelompok.
          @else
            @foreach ($list_instruktur as $instruktur)
              @foreach ($instruktur->kelompok as $kelompok)
                <div class="card shadow-sm border mb-5">
                  <div class="card-header">
                    <h4 class="mb-0">Kelompok {{ $kelompok->nomor }}</h4>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-bordered stack">
                      <thead class="thead-light">
                        <tr>
                          <th>Kode</th>
                          <th>Kelas</th>
                          <th>Jenjang</th>
                          <th>Jadwal</th>
                          <th>Instruktur</th>
                          <th>Nomor WA</th>
                          <th>Nomor HP</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="text-monospace">{{ $kelompok->nomor_reg_lengkap }}</td>
                          <td>{{ $kelompok->jenjang->kelas->nama }}</td>
                          <td>{{ $kelompok->jenjang->nama }}</td>
                          <td>{{ $kelompok->jadwal->getText() }}</td>
                          <td>{{ $kelompok->jadwal->anggota->nama }}</td>
                          <td>
                            @php
                              $nomor_wa = $kelompok->jadwal->anggota->profil->nomor_wa;
                              $nomor_wa_plain = preg_replace('/[^0-9]/', '', $nomor_wa);
                            @endphp
                            <a href="https://wa.me/{{ $nomor_wa_plain }}">
                              {{ $nomor_wa }}
                            </a>
                          </td>
                          <td>
                            @php
                              $nomor_hp = $kelompok->jadwal->anggota->profil->nomor_hp;
                              $nomor_hp_plain = preg_replace('/[^\+0-9]/', '', $nomor_hp);
                            @endphp
                            <a href="tel:{{ $nomor_hp_plain }}">
                              {{ $nomor_hp }}
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <div class="table-responsive">
                    <table class="table table-bordered stack">
                      <thead class="thead-light">
                        <tr>
                          <th>No.</th>
                          <th>Santri</th>
                          <th>Nomor WA</th>
                          <th>Nomor HP</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($kelompok->santri as $santri)
                          <tr>
                            <td>{{ $loop->iteration }}.</td>
                            <td>{{ $santri->anggota->nama }}</td>
                            <td>
                              @php
                                $nomor_wa = $santri->anggota->profil->nomor_wa;
                                $nomor_wa_plain = preg_replace('/[^0-9]/', '', $nomor_wa);
                              @endphp
                              <a href="https://wa.me/{{ $nomor_wa_plain }}">
                                {{ $nomor_wa }}
                              </a>
                            </td>
                            <td>
                              @php
                                $nomor_hp = $santri->anggota->profil->nomor_hp;
                                $nomor_hp_plain = preg_replace('/[^\+0-9]/', '', $nomor_hp);
                              @endphp
                              <a href="tel:{{ $nomor_hp_plain }}">
                                {{ $nomor_hp }}
                              </a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              @endforeach
            @endforeach

            @foreach ($list_santri as $santri)
              @php
                $kelompok = $santri->kelompok;
                if (!$kelompok) continue;
              @endphp
              <div class="card shadow-sm border mb-5">
                <div class="card-header">
                  <h4 class="mb-0">Kelompok {{ $kelompok->nomor }}</h4>
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered stack">
                    <thead class="thead-light">
                      <tr>
                        <th>Kode</th>
                        <th>Kelas</th>
                        <th>Jenjang</th>
                        <th>Jadwal</th>
                        <th>Instruktur</th>
                        <th>Nomor WA</th>
                        <th>Nomor HP</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-monospace">{{ $kelompok->nomor_reg_lengkap }}</td>
                        <td>{{ $kelompok->jenjang->kelas->nama }}</td>
                        <td>{{ $kelompok->jenjang->nama }}</td>
                        <td>{{ $kelompok->jadwal->getText() }}</td>
                        <td>{{ $kelompok->jadwal->anggota->nama }}</td>
                        <td>
                          @php
                            $nomor_wa = $kelompok->jadwal->anggota->profil->nomor_wa;
                            $nomor_wa_plain = preg_replace('/[^0-9]/', '', $nomor_wa);
                          @endphp
                          <a href="https://wa.me/{{ $nomor_wa_plain }}">
                            {{ $nomor_wa }}
                          </a>
                        </td>
                        <td>
                          @php
                            $nomor_hp = $kelompok->jadwal->anggota->profil->nomor_hp;
                            $nomor_hp_plain = preg_replace('/[^\+0-9]/', '', $nomor_hp);
                          @endphp
                          <a href="tel:{{ $nomor_hp_plain }}">
                            {{ $nomor_hp }}
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered stack">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>Santri</th>
                        <th>Nomor WA</th>
                        <th>Nomor HP</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($kelompok->santri as $santri)
                        <tr>
                          <td>{{ $loop->iteration }}.</td>
                          <td>{{ $santri->anggota->nama }}</td>
                          <td>
                            @php
                              $nomor_wa = $santri->anggota->profil->nomor_wa;
                              $nomor_wa_plain = preg_replace('/[^0-9]/', '', $nomor_wa);
                            @endphp
                            <a href="https://wa.me/{{ $nomor_wa_plain }}">
                              {{ $nomor_wa }}
                            </a>
                          </td>
                          <td>
                            @php
                              $nomor_hp = $santri->anggota->profil->nomor_hp;
                              $nomor_hp_plain = preg_replace('/[^\+0-9]/', '', $nomor_hp);
                            @endphp
                            <a href="tel:{{ $nomor_hp_plain }}">
                              {{ $nomor_hp }}
                            </a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            @endforeach
          @endif
        </>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('js')
  <script src="{{ asset('assets/js/stacktable.js') }}"></script>
  <script>
    $('.table.stack').stacktable({
      displayHeader: false,
    });

    $('.stacktable.large-only').addClass('d-none d-md-table');
    $('.stacktable.small-only').addClass('d-table d-md-none');
    $('.stacktable.small-only .st-head-row').addClass('pt-4 pb-2 text-monospace');
    $('.stacktable th').addClass('bg-secondary');
    $('.stacktable .st-head-row').css('white-space', 'normal');
  </script>
@endpush
