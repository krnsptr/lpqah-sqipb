@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-lg-6 col-md-6 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Hapus Jadwal</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf

            <div class="row no-gutters">
              <div class="col-12 text-center">
                Apakah kamu yakin akan menghapus jadwal ini?

                <h3 class="my-3">{{ $jadwal->getText() }}</h3>
              </div>
            </div>

            <hr>

            <div class="row">
              <div class="col-lg-4 col-md-6 mb-2 mx-auto order-lg-2">
                <button type="submit" class="btn btn-block btn-danger">
                  Hapus
                </button>
              </div>
              <div class="col-lg-4 col-md-6 mb-2 mx-auto order-lg-1">
                <a href="#" onclick="history.go(-1);"
                  class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.6/flatpickr.min.css"
    integrity="sha384-x4SoWYNT3M7FCKjXbjyLU3aCOAxxwv7FzUip+JKc0H1AeVvmiDAYlwW/C9wQwTf7"
    crossorigin="anonymous">
@endpush

@push ('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.6/flatpickr.min.js"
    integrity="sha384-qR47O9ivmZhlLjVQtQhadoZe1k+TSYZRe5SgOziMjg11vDh1EjUTQ4TedMViShCw"
    crossorigin="anonymous"></script>

  <script>
    $('#waktu_mulai').flatpickr({
      enableTime: true,
      noCalendar: true,
      time_24hr: true,
      dateFormat: "H:i",
      minTime: "05:00",
      maxTime: "19:00",
    });
  </script>
@endpush
