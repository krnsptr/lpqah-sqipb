@extends ('layouts.argon.dashboard')

@section ('content')

<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Unduh Data</h2>
        </div>

        <div class="card-body">
          @messages

          <div class="text-center">
            <a href="{{ request()->fullUrlwithQuery(['unduh' => 'instruktur']) }}"
              class="btn btn-primary my-2">
              <i class="fas fa-download fa-fw mr-1"></i>
              Instruktur
            </a>

            <a href="{{ request()->fullUrlwithQuery(['unduh' => 'santri']) }}"
              class="btn btn-primary my-2">
              <i class="fas fa-download fa-fw mr-1"></i>
              Santri
            </a>

            <a href="{{ request()->fullUrlwithQuery(['unduh' => 'kelompok']) }}"
              class="btn btn-primary my-2">
              <i class="fas fa-download fa-fw mr-1"></i>
              Kelompok
            </a>

            <a href="{{ request()->fullUrlwithQuery(['unduh' => 'biaya']) }}"
              class="btn btn-primary my-2">
              <i class="fas fa-download fa-fw mr-1"></i>
              {{ $setelan->detail['biaya']['label'] }}
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
