@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Anggota: {{ $anggota->nama }}</h2>
        </div>

        <div class="card-body">
          @messages

          <div class="row no-gutters">
            <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Nama lengkap
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $anggota->nama }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Alamat email
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $anggota->email }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Jenis kelamin
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->jenis_kelamin_text ?? '-' }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Username
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->username ?? '-' }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Tanggal lahir
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ format_date($profil->tanggal_lahir ?? null) ?? '-' }}
                </div>
              </div>
            </div>

            <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Status
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->status_text ?? '-' }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Nomor identitas
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->jenis_identitas ?? '' }} {{ $profil->nomor_identitas ?? '-' }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Nomor WA
                </label>
                <div class="col-md-8 col-xl-7">
                  @if ($profil->nomor_wa ?? $anggota->nomor_wa)
                    @php
                      $nomor_wa = $profil->nomor_wa ?? $anggota->nomor_wa;
                      $nomor_wa_plain = preg_replace('/[^0-9]/', '', $nomor_wa);
                    @endphp
                    <a href="https://wa.me/{{ $nomor_wa_plain }}">
                      {{ $nomor_wa }}
                    </a>
                  @else
                    -
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Nomor HP
                </label>
                <div class="col-md-8 col-xl-7">
                  @if ($profil->nomor_hp)
                    @php
                      $nomor_hp = $profil->nomor_hp;
                      $nomor_hp_plain = preg_replace('/[^\+0-9]/', '', $nomor_hp);
                    @endphp
                    <a href="tel:{{ $nomor_hp_plain }}">
                      {{ $nomor_hp }}
                    </a>
                  @else
                    -
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Waktu daftar
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ format_datetime($anggota->created_at) }} WIB
                </div>
              </div>
            </div>

            <div class="col-xl-6 mb-3 d-flex flex-column">
              <div class="text-center px-xl-5 mt-3">
                <h3>Asal Daerah</h3>
                <hr>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Provinsi
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->provinsi->nama ?? '-' }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Kota
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->kota->nama ?? '-' }}
                </div>
              </div>
            </div>

            <div class="col-xl-6 mb-3 d-flex flex-column">
              <div class="text-center px-xl-5 mt-3">
                <h3>Nomor Rekening</h3>
                <hr>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Bank
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->bank->nama ?? '-' }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Nomor Rekening
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->nomor_rekening ?? '-' }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-4 col-xl-5 font-weight-bold text-md-right">
                  Atas Nama
                </label>
                <div class="col-md-8 col-xl-7">
                  {{ $profil->nama_rekening ?? '-' }}
                </div>
              </div>
            </div>
          </div>

          <hr class="mt-0">

          <div class="row">
            <div class="col-lg-3 col-md-4 mx-auto py-1">
              <form autocomplete="off" method="post"
                action="{{ route('admin.anggota_masuk', $anggota) }}">
                @csrf
                <button type="submit" class="btn btn-block btn-default">
                  Masuk
                </button>
              </form>
            </div>

            <div class="col-lg-3 col-md-4 mx-auto py-1">
              <a href="#reset_password" class="btn btn-block btn-warning" data-toggle="modal">
                Reset Password
              </a>
            </div>

            <div class="col-lg-3 col-md-4 mx-auto py-1">
              <a href="javascript:close();" class="btn btn-block btn-secondary">
                Tutup
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
  @csrf

  <div class="modal fade" tabindex="-1" role="dialog" id="reset_password">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Reset Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <div class="form-group row">
            <label class="col-md-4 col-xl-5 col-form-label text-md-right">
              Alamat email
            </label>
            <div class="col-md-8 col-xl-7">
              <input type="email" class="form-control" id="email" name="email"
                value="{{ $anggota->email }}" required>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-xl-5 col-form-label text-md-right">
              Password baru
            </label>
            <div class="col-md-8 col-xl-7">
              <div class="input-group">
                <input type="password" class="form-control" id="password" name="password" required>

                <div class="input-group-append">
                  <span class="input-group-text bg-secondary password-toggle" style="cursor: pointer;">
                    <i class="fas fa-fw fa-eye"></i>
                    <i class="fas fa-fw fa-eye-slash d-none"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary mx-1" data-dismiss="modal">
            Batal
          </button>

          <button type="submit" class="btn btn-warning mx-1" name="aksi" value="reset_password">
            Reset
          </button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection

@push ('css')
  <link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet">
  <link href="{{ asset('assets/css/style-select2.css') }}" rel="stylesheet">
@endpush

@push ('js')
  <script src="//unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/imask/6.0.3/imask.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="{{ asset('assets/js/select2-cascade.js') }}"></script>
  <script>
    IMask($('#username')[0], {
      mask: /^[a-z0-9_]+$/,
    });

    IMask($('#nomor_hp')[0], {
      mask: '{+62}-800-0000-00[000]',
      lazy: false,
    });

    IMask($('#nomor_wa')[0], {
      mask: '{+62}-800-0000-00[000]',
      lazy: false,
    });

    $('#tanggal_lahir').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      defaultViewDate: '-19y',
      endDate: '0d',
      startView: 'years',
    });

    var map_jenis_identitas = @json($profil::JENIS_IDENTITAS);
    var list_regex = @json($profil::REGEX);

    var old_nomor_identitas = $('#nomor_identitas').val();
    var imask_nomor_identitas;

    $('#status').change(function () {
      var status = $(this).val();
      var list_jenis_identitas = map_jenis_identitas[status] || [];

      $('#jenis_identitas > option').each(function () {
        if (list_jenis_identitas.indexOf($(this).val()) === -1) {
          $(this).prop('disabled', true).hide();
        } else {
          $(this).prop('disabled', false).show();
        }

        if ($('#jenis_identitas > option:selected').is(':hidden')) {
          $('#jenis_identitas > option:visible').prop('selected', true).change();
        }
      });
    }).change();

    $('#jenis_identitas').change(function () {
      var jenis_identitas = String($('#jenis_identitas').val());
      var nomor_identitas = String($('#nomor_identitas').val());
      var regex = list_regex[jenis_identitas] || '';

      regex = regex.replace(/\//g, '');
      regex = new RegExp(regex, 'gi');

      if (imask_nomor_identitas !== undefined) {
        imask_nomor_identitas.destroy();
      }

      if (!nomor_identitas.match(regex)) {
        if (old_nomor_identitas.match(regex)) {
          $('#nomor_identitas').val(old_nomor_identitas).change();
        } else {
          $('#nomor_identitas').val('').change();
        }

        if (nomor_identitas !== '') {
          old_nomor_identitas = nomor_identitas;
        }
      }

      if (jenis_identitas === 'NIK') {
        imask_nomor_identitas = new IMask($('#nomor_identitas')[0], {
          mask: '00.00.00.00-00-00.0000',
          lazy: false,
        });
      } else if (jenis_identitas === 'NIM') {
        imask_nomor_identitas = new IMask($('#nomor_identitas')[0], {
          mask: 'a0[a][0]000000[0]',
        });
      }
    }).change();

    $.ajax({
      url: @json(route('api.web.provinsi')),
      dataType: 'json',
      success: function(data) {
        id_provinsi = @json((string) ($profil->id_provinsi ?? old('id_provinsi')));

        $('#id_provinsi').select2({ data: data }).val(id_provinsi).change();
        $('#id_kota').select2();
      }
    });

    new Select2Cascade(
      $('#id_provinsi'),
      $('#id_kota'),
      @json(route('api.web.kota')) + '/?id_provinsi=:parentId:'
    ).then(function(parent, child) {
      setTimeout(function() {
        id_kota = @json((string) ($profil->id_kota ?? old('id_kota')));
        child.val(id_kota).change();
      }, 200);
    });
  </script>

  <script>
    $('.password-toggle').click(function() {
      inputGroup = $(this).closest('.input-group');
      input = $(inputGroup).find('input');
      input.attr('type', (input.attr('type') === 'text') ? 'password' : 'text');
      input.next().find('i').toggleClass('d-none');
    });
  </script>
@endpush
