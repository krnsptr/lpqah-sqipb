@extends ('layouts.argon.dashboard')

@section ('content')

@include ('layouts.argon.inc.header-guest')

<div class="container mt--8 mb-6">
  <div class="row mt--2 justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">
        <div class="card-header bg-transparent">
          <h1 class="text-center">Verifikasi Gagal</h1>
        </div>
        <div class="px-3 pt-3">
          @messages
        </div>
        <div class="card-body px-lg-5 pt-0 pb-0">
          <div class="my-2">
            Pastikan alamat email, akun, dan token sudah sesuai.
          </div>
        </div>
        <div class="card-footer bg-transparent text-center">
          <a href="{{ route('anggota.dasbor') }}">
            Kembali
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
