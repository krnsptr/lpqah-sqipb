@php

$conditions = [
  'instruktur' => fn () => anggota()->isInstrukturSudahDites(),
  'santri' => fn () => anggota()->isSantriSudahDites(),
  'calon_instruktur' => fn () => anggota()->isInstrukturBelumDites(),
  'calon_santri' => fn () => anggota()->isSantriBelumDites(),
  'umum' => fn () => true,
];

@endphp

@foreach ($conditions as $slug => $condition)
  @isset ($setelan->detail['pengumuman'][$slug])
    @php $pengumuman = $setelan->detail['pengumuman'][$slug] @endphp

    @if (($pengumuman['show'] ?? false) && $condition())
      <div class="row">
        <div class="col">
          <div class="alert alert-secondary fade show my-1">
            <h4>{{ $pengumuman['title'] ?? '' }}</h4>
            <div>{!! $pengumuman['text'] ?? '' !!}</div>
          </div>
        </div>
      </div>
    @endif
  @endisset
@endforeach
