@extends ('layouts.argon.dashboard')

@php

$text_jenjang_sebelumnya = $santri->detail->nama_jenjang_sebelumnya ?? '';

if (isset($santri->detail->nama_jenjang_sebelumnya)) {
  $lulus = $santri->detail->lulus_jenjang_sebelumnya;
  $text_lulus = $santri::DETAIL_LULUS_JENJANG[$lulus] ?? '';
  $text_jenjang_sebelumnya .= " — $text_lulus";
}

@endphp

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Edit Santri: NRS.{{ $santri->nomor_reg }}</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor registrasi
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    <span class="text-monospace">{{ $santri->nomor_reg_lengkap }}</span>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama lengkap
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    <a href="{{ route('admin.anggota_lihat', $santri->anggota) }}">
                      {{ $santri->anggota->nama }}
                    </a>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor identitas
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $santri->anggota->profil->nomor_identitas }}
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $santri->kelas->nama }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jenjang
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $santri->jenjang->nama ?? '(Belum dites)' }}
                  </div>
                </div>

                @isset ($santri->detail->pendaftaran)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Jenis pendaftaran
                    </label>
                    <div class="col-md-8 col-xl-7 my-auto">
                      {{ $santri::DETAIL_PENDAFTARAN[$santri->detail->pendaftaran] }}
                    </div>
                  </div>
                @endisset
              </div>
            </div>

            <hr class="mt-0">

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelompok
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    @if ($kelompok = $santri->kelompok)
                      <span class="text-monospace">
                        {{ $kelompok->nomor_reg_lengkap }}
                      </span>
                    @else
                      -
                    @endif

                    @if ($jadwal = $santri->kelompok->jadwal ?? false)
                      {{ $jadwal->getText() }}
                    @endif
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Instruktur
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    @if ($instruktur = $santri->kelompok->jadwal ?? false)
                      <a
                        href="{{ route('admin.anggota_lihat', $jadwal->anggota) }}">
                        {{ $jadwal->anggota->nama }}
                      </a>
                    @else
                      -
                    @endif
                  </div>
                </div>
              </div>
            </div>

            @if (($santri->detail->pendaftaran ?? '') === 'ulang') <hr class="mt-0"> @endif

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                @isset ($santri->detail->angkatan_sebelumnya)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Angkatan sebelumnya
                    </label>
                    <div class="col-md-8 col-xl-7 my-auto">
                      {{ $santri->detail->angkatan_sebelumnya }}
                    </div>
                  </div>
                @endisset
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                @isset ($santri->detail->id_jenjang_sebelumnya)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Jenjang sebelumnya
                    </label>
                    <div class="col-md-8 col-xl-7 my-auto">
                      {{ $text_jenjang_sebelumnya }}
                    </div>
                  </div>
                @endisset
              </div>
            </div>

            <hr class="mt-0">

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8">
                    <select class="form-control" name="id_kelas" id="id_kelas" required></select>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right font-weight-bold">
                    Jenjang
                  </label>
                  <div class="col-md-8">
                    <select class="form-control" name="id_jenjang" id="id_jenjang"></select>
                  </div>
                </div>
              </div>
            </div>

            <div class="row no-gutters">
              <div class="col-xl-12 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-2 col-form-label text-md-right font-weight-bold">
                    Kelompok
                  </label>
                  <div class="col-md-10">
                    <select class="form-control" name="id_kelompok" id="id_kelompok"></select>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto py-1">
                <button type="submit" class="btn btn-block btn-default">
                  Simpan
                </button>
              </div>
              <div class="col-lg-3 col-md-4 mx-auto py-1">
                <a href="javascript:close();" class="btn btn-block btn-secondary">
                  Tutup
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet">
  <link href="{{ asset('assets/css/style-select2.css') }}" rel="stylesheet">
@endpush

@push('js')
  <script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="{{ asset('assets/js/select2-cascade.js') }}"></script>
  <script>
    $.ajax({
      url: @json(route('api.web.kelas')),
      dataType: 'json',
      success: function(data) {
        id_kelas = @json((string) ($santri->id_kelas));

        $('#id_kelas').select2({ data: data }).val(id_kelas).change();
        $('#id_jenjang').select2();
        $('#id_kelompok').select2();
      }
    });

    new Select2Cascade(
      $('#id_kelas'),
      $('#id_jenjang'),
      @json(route('api.web.jenjang')) + '/?id_kelas=:parentId:'
    ).then(function(parent, child) {
      newOption = new Option('(Belum dites)', '', true, false);
      child.prepend(newOption).trigger('change');

      setTimeout(function() {
        id_jenjang = @json((string) ($santri->id_jenjang));
        child.val(id_jenjang).change();
      }, 500);
    });

    new Select2Cascade(
      $('#id_jenjang'),
      $('#id_kelompok'),
      @json(route('api.web.kelompok'))
      + '/?id_jenjang=:parentId:'
      + '&jenis_kelamin=' + @json($santri->anggota->profil->jenis_kelamin)
    ).then(function(parent, child) {
      newOption = new Option('(Belum dipilih)', '', true, false);
      child.prepend(newOption).trigger('change');

      setTimeout(function() {
        id_kelompok = @json((string) ($santri->id_kelompok));
        child.val(id_kelompok).change();
      }, 500);
    });
  </script>
@endpush
