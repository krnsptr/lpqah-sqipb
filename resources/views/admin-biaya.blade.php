@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header bg-gradient-primary py-3 pt-md-6">
  <div class="container-fluid">
    <div class="row">
      <div class="col px-1 px-md-0">
        <div class="card shadow">
          <div class="card-header">
            <h3 class="mb-0">
              {{ $setelan->detail['biaya']['label'] }}
            </h3>
          </div>
          <div class="px-3">
            @messages
          </div>
          <div class="table-responsive">
            <table class="table datatables" id="table">
              <thead class="thead-light">
                <tr>
                  <th style="max-width: 6rem;">Nomor Reg</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Kelas</th>
                  <th>Jenjang</th>
                  <th>Nominal</th>
                  <th>Status</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link href="//cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <style>
    .w-4rem {
      width: 4rem !important;
    }
  </style>
@endpush

@push ('js')
  <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="//cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.4/jquery.dataTables.yadcf.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.35/dayjs.min.js"></script>
  <script>
    mapper = {};
    mapper['jenis_kelamin'] = @json(App\Models\AnggotaProfil::JENIS_KELAMIN);

    mapper['kelas'] = @json(App\Models\Kelas::all()->mapWithKeys(function ($kelas) {
      return [$kelas->nama => $kelas->nama];
    }));

    mapper['jenjang'] = @json(App\Models\Jenjang::all()->mapWithKeys(function ($jenjang) {
      return [$jenjang->nama => $jenjang->nama];
    }));

    mapper['status'] = @json(App\Models\Biaya::STATUS);

    render = {};

    render['jenis_kelamin'] = function (data) {
      return mapper['jenis_kelamin'][data];
    };

    render['status'] = function (data) {
      return mapper['status'][data];
    };

    filter = {};
    filter['jenis_kelamin'] = [];
    filter['kelas'] = [];
    filter['jenjang'] = [{ value: '-', label: '(Belum dites)' }];
    filter['status'] = [];

    for (f in filter) {
      for (key in mapper[f]) {
        filter[f].push({ value: key, label: mapper[f][key] });
      }
    }
  </script>
  <script>
    aksi = '<button type="button" onclick="edit(this)" class="btn btn-sm btn-default">Edit</button>';

    function edit(button) {
      id = $(button).closest('tr').attr('id');
      window.open('{{ route('admin.biaya_edit', '') }}' + '/' + id);
    }

    table = $('#table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ route('admin.biaya') }}',
      dom: "<'row'<'col-lg-4 p-3'l>>rt<'row'<'col-lg-6 p-3'i><'col-lg-6 p-3'p>>",
      language: {
        paginate: {
          previous: '&laquo;',
          next: '&raquo;',
        }
      },
      lengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, 'All'],
      ],
      rowId: 'id',
      columnDefs: [
        { targets: '_all', searchable: false, orderable: false },
      ],
      columns: [
        {
          data: 'nomor',
          name: 'nomor',
          orderable: true,
          searchable: true,
        },
        {
          data: 'anggota.nama',
          name: 'anggota.nama',
          searchable: true,
        },
        {
          visible: false,
          data: 'anggota.profil.jenis_kelamin',
          name: 'anggota.profil.jenis_kelamin',
          defaultContent: '-',
          searchable: true,
          render: render['jenis_kelamin'],
        },
        {
          data: 'kelas.nama',
          name: 'kelas.nama',
          defaultContent: '-',
          searchable: true,
        },
        {
          visible: false,
          data: 'jenjang.nama',
          name: 'jenjang.nama',
          defaultContent: '(Belum dites)',
          searchable: true,
        },
        {
          data: 'biaya.nominal',
          name: 'biaya.nominal',
          render: $.fn.dataTable.render.number(' '),
          className: 'text-right',
        },
        {
          data: 'biaya.status',
          name: 'biaya.status',
          searchable: true,
          render: render['status'],
        },
        {
          // visible: false,
          data: 'biaya.keterangan',
          name: 'biaya.keterangan',
          defaultContent: '-',
          searchable: true,
        },
        {
          data: null,
          defaultContent: aksi,
          searchable: false,
          orderable: false,
        },
      ],
      order: [
        [0, 'asc'],
      ],
    });

    yadcf.init(table, [
      {
        column_number: 0,
        filter_type: 'text',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm w-4rem mt-1',
        filter_delay: 1000,
        filter_match_mode: 'exact',
      },
      {
        column_number: 1,
        filter_type: 'text',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        filter_delay: 1000,
      },
      {
        column_number: 2,
        filter_type: 'select',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        data: filter['jenis_kelamin'],
      },
      {
        column_number: 3,
        filter_type: 'select',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        data: filter['kelas'],
      },
      {
        column_number: 4,
        filter_type: 'select',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        data: filter['jenjang'],
      },
      {
        column_number: 6,
        filter_type: 'select',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        data: filter['status'],
      },
      {
        column_number: 7,
        filter_type: 'text',
        filter_reset_button_text: false,
        filter_default_label: '',
        style_class: 'form-control form-control-sm mt-1',
        filter_delay: 1000,
      },
    ]);

    // setInterval(function () {
    //   table.ajax.reload();
    // }, 30 * 1000);
  </script>
@endpush
