@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Edit Instruktur: NRI.{{ $instruktur->nomor_reg }}</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor registrasi
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    <span class="text-monospace">{{ $instruktur->nomor_reg_lengkap }}</span>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama lengkap
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    <a href="{{ route('admin.anggota_lihat', $instruktur->anggota) }}">
                      {{ $instruktur->anggota->nama }}
                    </a>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor identitas
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $instruktur->anggota->profil->nomor_identitas }}
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $instruktur->kelas->nama }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jenjang
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $instruktur->jenjang->nama ?? '(Belum dites)' }}
                  </div>
                </div>

                @isset ($instruktur->detail->pendaftaran)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Jenis pendaftaran
                    </label>
                    <div class="col-md-8 col-xl-7 my-auto">
                      {{ $instruktur::DETAIL_PENDAFTARAN[$instruktur->detail->pendaftaran] }}
                    </div>
                  </div>
                @endisset
              </div>
            </div>

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 py-1 text-md-right font-weight-bold">
                    Memenuhi syarat
                  </label>
                  <div class="col-md-8 col-xl-7">
                    @php
                      $kelas = $instruktur->kelas;

                      $list_syarat = (
                        $setelan->detail['syarat_instruktur'][$kelas->id]
                        ?? $kelas->detail->syarat_instruktur
                      );
                    @endphp

                    @foreach ($list_syarat as $syarat)
                      <div class="custom-control custom-checkbox py-1">
                        <input type="checkbox" class="custom-control-input" disabled
                          name="detail[syarat_instruktur][{{ $loop->index }}]"
                          id="detail[syarat_instruktur][{{ $loop->index }}]"
                          value="{{ $syarat }}"
                          @if (array_search($syarat, $instruktur->detail->syarat_instruktur) !== false)
                            checked
                          @endif>
                        <label class="custom-control-label"
                          for="detail[syarat_instruktur][{{ $loop->index }}]">
                          {{ $syarat }}
                        </label>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8">
                    <select class="form-control" name="id_kelas" id="id_kelas" disabled></select>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right font-weight-bold">
                    Jenjang
                  </label>
                  <div class="col-md-8">
                    <select class="form-control" name="id_jenjang" id="id_jenjang"></select>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto py-1">
                <button type="submit" class="btn btn-block btn-default">
                  Simpan
                </button>
              </div>
              <div class="col-lg-3 col-md-4 mx-auto py-1">
                <a href="javascript:close();" class="btn btn-block btn-secondary">
                  Tutup
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet">
  <link href="{{ asset('assets/css/style-select2.css') }}" rel="stylesheet">
@endpush

@push('js')
  <script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="{{ asset('assets/js/select2-cascade.js') }}"></script>
  <script>
    $.ajax({
      url: @json(route('api.web.kelas')),
      dataType: 'json',
      success: function(data) {
        id_kelas = @json((string) ($instruktur->id_kelas));

        $('#id_kelas').select2({ data: data }).val(id_kelas).change();
        $('#id_jenjang').select2();
      }
    });

    new Select2Cascade(
      $('#id_kelas'),
      $('#id_jenjang'),
      @json(route('api.web.jenjang')) + '/?id_kelas=:parentId:'
    ).then(function(parent, child) {
      newOption = new Option('(Belum dites)', '', true, false);
      child.prepend(newOption).trigger('change');

      setTimeout(function() {
        id_jenjang = @json((string) ($instruktur->id_jenjang));
        child.val(id_jenjang).change();
      }, 200);
    });
  </script>
@endpush
