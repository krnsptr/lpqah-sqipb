<table style="border: 1px solid #000;">
  <thead>
    <tr>
      <th>Nomor Reg</th>
      <th>Nama</th>
      <th>Email</th>
      <th>Username</th>
      <th>Jenis Kelamin</th>
      <th>Tanggal Lahir</th>
      <th>Status</th>
      <th>Nomor Identitas</th>
      <th>Nomor WA</th>
      <th>Nomor HP</th>
      <th>Asal Provinsi</th>
      <th>Asal Kota</th>
      <th>Bank Rekening</th>
      <th>Nomor Rekening</th>
      <th>Nama Rekening</th>
      <th>Kelas</th>
      <th>Jenjang</th>
      <th>Pendaftaran</th>
      <th>Angkatan Sebelumnya</th>
      <th>Jenjang Sebelumnya</th>
      <th>Kelompok</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_santri as $santri)
      <tr>
        <td>{{ $santri->nomor }}</td>
        <td>{{ $santri->anggota->nama }}</td>
        <td>{{ $santri->anggota->email }}</td>
        <td>{{ $santri->anggota->profil->username ?? '' }}</td>
        <td>
          {{ $santri->anggota->profil->jenis_kelamin_text ?? '' }}
        </td>
        <td>
          @isset ($santri->anggota->profil->tanggal_lahir)
            {{ $santri->anggota->profil->tanggal_lahir->format('Y-m-d') }}
          @endisset
        </td>
        <td>
          {{ $santri->anggota->profil->status_text ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->jenis_identitas ?? '' }}
          {{ $santri->anggota->profil->nomor_identitas ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->nomor_wa ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->nomor_hp ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->provinsi->nama ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->kota->nama ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->bank->nama ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->nomor_rekening ?? '' }}
        </td>
        <td>
          {{ $santri->anggota->profil->nama_rekening ?? '' }}
        </td>
        <td>
          {{ $santri->kelas->nama ?? '' }}
        </td>
        <td>
          {{ $santri->jenjang->nama ?? '(Belum dites)' }}
        </td>
        <td>
          {{ $santri->detail->pendaftaran ?? '' }}
        </td>
        <td>
          {{ $santri->detail->angkatan_sebelumnya ?? '' }}
        </td>
        <td>
          @isset ($santri->detail->id_jenjang_sebelumnya)
            {{ $santri->detail->nama_jenjang_sebelumnya ?? '' }}
            |
            {{ $santri->detail->lulus_jenjang_sebelumnya ?? '' }}
          @endisset
        </td>
        <td>
          {{ $santri->kelompok->nomor ?? '' }}
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
