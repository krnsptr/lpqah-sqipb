<table style="border: 1px solid #000;">
  <thead>
    <tr>
      <th>Nomor Reg</th>
      <th>Nama</th>
      <th>Email</th>
      <th>Jenis Kelamin</th>
      <th>Kelas</th>
      <th>Jenjang</th>
      <th>Nominal</th>
      <th>Status</th>
      <th>Keterangan</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_santri as $santri)
      <tr>
        <td>{{ $santri->nomor }}</td>
        <td>{{ $santri->anggota->nama }}</td>
        <td>{{ $santri->anggota->email }}</td>
        <td>
          {{ $santri->anggota->profil->jenis_kelamin_text ?? '' }}
        </td>
        <td>
          {{ $santri->kelas->nama ?? '' }}
        </td>
        <td>
          {{ $santri->jenjang->nama ?? '(Belum dites)' }}
        </td>
        <td>
          {{ $santri->biaya->nominal }}
        </td>
        <td>
          {{ $santri->biaya->status_text }}
        </td>
        <td>
          {{ $santri->biaya->keterangan }}
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
