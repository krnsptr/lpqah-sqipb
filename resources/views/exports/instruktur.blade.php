<table style="border: 1px solid #000;">
  <thead>
    <tr>
      <th>Nomor Reg</th>
      <th>Nama</th>
      <th>Email</th>
      <th>Username</th>
      <th>Jenis Kelamin</th>
      <th>Tanggal Lahir</th>
      <th>Status</th>
      <th>Nomor Identitas</th>
      <th>Nomor WA</th>
      <th>Nomor HP</th>
      <th>Asal Provinsi</th>
      <th>Asal Kota</th>
      <th>Bank Rekening</th>
      <th>Nomor Rekening</th>
      <th>Nama Rekening</th>
      <th>Kelas</th>
      <th>Jenjang</th>
      <th>Pendaftaran</th>
      <th>Memenuhi Syarat</th>
      <th>Kapasitas Membina</th>
      <th>Penjadwalan</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_instruktur as $instruktur)
      <tr>
        <td>{{ $instruktur->nomor }}</td>
        <td>{{ $instruktur->anggota->nama }}</td>
        <td>{{ $instruktur->anggota->email }}</td>
        <td>{{ $instruktur->anggota->profil->username ?? '' }}</td>
        <td>
          {{ $instruktur->anggota->profil->jenis_kelamin_text ?? '' }}
        </td>
        <td>
          @isset ($instruktur->anggota->profil->tanggal_lahir)
            {{ $instruktur->anggota->profil->tanggal_lahir->format('Y-m-d') }}
          @endisset
        </td>
        <td>
          {{ $instruktur->anggota->profil->status_text ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->jenis_identitas ?? '' }}
          {{ $instruktur->anggota->profil->nomor_identitas ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->nomor_wa ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->nomor_hp ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->provinsi->nama ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->kota->nama ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->bank->nama ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->nomor_rekening ?? '' }}
        </td>
        <td>
          {{ $instruktur->anggota->profil->nama_rekening ?? '' }}
        </td>
        <td>
          {{ $instruktur->kelas->nama ?? '' }}
        </td>
        <td>
          {{ $instruktur->jenjang->nama ?? '(Belum dites)' }}
        </td>
        <td>
          {{ $instruktur->detail->pendaftaran ?? '' }}
        </td>
        <td>
          @isset ($instruktur->detail->syarat_instruktur)
            {{ implode(' | ', $instruktur->detail->syarat_instruktur) }}
          @endisset
        </td>
        <td>{{ $instruktur->detail->kapasitas_membina ?? '' }}</td>
        <td>
          @isset ($instruktur->anggota->jadwal)
            {{ $instruktur->anggota->jadwal->count() ? 'sudah' : 'belum' }}
          @else
            belum
          @endisset
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
