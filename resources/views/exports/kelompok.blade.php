<table>
  @foreach ($list_kelompok as $kelompok)
    <tr>
      <th><strong></strong></th>
      <th><strong>Kelompok</strong></th>
      <th><strong>Jenis Kelamin</strong></th>
      <th><strong>Jenjang</strong></th>
      <th><strong>Hari</strong></th>
      <th><strong>Waktu</strong></th>
    </tr>

    <tr>
      <td></td>
      <td>{{ $kelompok->nomor }}</td>
      <td>{{ $kelompok->instruktur->anggota->profil->jenis_kelamin_text }}</td>
      <td>{{ $kelompok->jenjang->nama }}</td>
      <td>{{ $kelompok->jadwal->hari->description }}</td>
      <td>{{ $kelompok->jadwal->waktu_mulai->format('H:i') }}</td>
    </tr>

    <tr>
      <th><strong></strong></th>
      <th colspan="2"><strong>Nama Instruktur</strong></th>
      <th><strong>Nomor Identitas</strong></th>
      <th><strong>Nomor WA</strong></th>
      <th><strong>Nomor HP</strong></th>
    </tr>

    <tr>
      <td></td>
      <td colspan="2">{{ $kelompok->instruktur->anggota->nama }}</td>
      <td>{{ $kelompok->instruktur->anggota->profil->nomor_identitas }}</td>
      <td>{{ $kelompok->instruktur->anggota->profil->nomor_wa }}</td>
      <td>{{ $kelompok->instruktur->anggota->profil->nomor_hp }}</td>
    </tr>

    <tr>
      <th><strong>No.</strong></th>
      <th colspan="2"><strong>Nama Santri</strong></th>
      <th><strong>Nomor Identitas</strong></th>
      <th><strong>Nomor WA</strong></th>
      <th><strong>Nomor HP</strong></th>
    </tr>

    @foreach ($kelompok->santri as $santri)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td colspan="2">{{ $santri->anggota->nama }}</td>
        <td>{{ $santri->anggota->profil->nomor_identitas }}</td>
        <td>{{ $santri->anggota->profil->nomor_wa }}</td>
        <td>{{ $santri->anggota->profil->nomor_hp }}</td>
      </tr>
    @endforeach

    <tr></tr>
  @endforeach
</table>
