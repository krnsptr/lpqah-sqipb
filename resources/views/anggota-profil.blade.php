@extends ('layouts.argon.dashboard')

@php
  $is_rekening_hidden = $profil->isRekeningHidden();
  $is_rekening_required = $profil->isRekeningRequired();
@endphp

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Edit Profil</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Nama lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nama" name="nama"
                      placeholder="Contoh: Muhammad Fulan"
                      value="{{ $anggota->nama ?? old('nama') }}" required
                      @if ($profil->exists && !admin()) disabled @endif>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Alamat email
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="email" class="form-control" id="email" name="email"
                      value="{{ $anggota->email ?? old('email') }}" required
                      @if ($anggota->email_verified_at && !admin()) disabled @endif>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Jenis kelamin
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" required
                      @if ($profil->exists && !admin()) disabled @endif>
                      <option value=""></option>
                      @foreach ($profil::JENIS_KELAMIN as $key => $value)
                        <option value="{{ $key }}"
                        @if (($profil->jenis_kelamin ?? old('jenis_kelamin')) == $key)
                          selected
                        @endif>{{ $value }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Username
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control text-lowercase" id="username" name="username"
                      value="{{ $profil->username ?? old('username') }}"
                      pattern="[A-Za-z0-9_]{6,16}" maxlength="16" required>
                    <small class="text-muted ml-1">
                      6&ndash;16 karakter (huruf, angka, underscore)
                    </small>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Tanggal lahir
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text bg-secondary">
                          <i class="far fa-fw fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control" id="tanggal_lahir"
                        name="tanggal_lahir" placeholder="HH-BB-TTTT" required
                        @if ($profil->tanggal_lahir)
                          value="{{ $profil->tanggal_lahir->format('d-m-Y') }}"
                        @elseif (old('tanggal_lahir'))
                          value="{{ old('tanggal_lahir') }}"
                        @endif>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Status
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="status" name="status" required>
                      <option value=""></option>
                      @foreach ($profil::STATUS as $key => $value)
                        <option value="{{ $key }}"
                        @if (($profil->status ?? old('status')) == $key)
                          selected
                        @endif>{{ $value }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Nomor identitas
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <select class="form-control" id="jenis_identitas" name="jenis_identitas" required>
                          <option value="NIM" id="jenis_identitas[NIM]"
                            @if (($profil->jenis_identitas ?? old('jenis_identitas')) === 'NIM')
                              selected
                            @endif>
                            NIM
                          </option>
                          <option value="NIK" id="jenis_identitas[NIK]"
                            @if (($profil->jenis_identitas ?? old('jenis_identitas')) === 'NIK')
                              selected
                            @endif>
                            NIK
                          </option>
                        </select>
                      </div>
                      <input type="text" class="form-control"
                        id="nomor_identitas" name="nomor_identitas" required
                        value="{{ $profil->nomor_identitas ?? old('nomor_identitas') }}">
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Nomor WA
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="tel" class="form-control" id="nomor_wa" name="nomor_wa"
                      value="{{ $profil->nomor_wa ?? $anggota->nomor_wa ?? old('nomor_wa') }}"
                      required>
                    <small>Untuk memudahkan komunikasi, wajib pakai WA ya</small>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Nomor HP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="tel" class="form-control" id="nomor_hp" name="nomor_hp"
                      value="{{ $profil->nomor_hp ?? old('nomor_hp') }}" required>
                    <small>Nomor yang bisa dihubungi jika WA offline</small>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column">
                <div class="text-center px-xl-5 mt-3">
                  <h3>Asal Daerah</h3>
                  <hr>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Provinsi
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_provinsi" name="id_provinsi"></select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Kota
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_kota" name="id_kota"></select>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column"
                @if ($is_rekening_hidden) style="display: none !important;" @endif>
                <div class="text-center px-xl-5 mt-3">
                  <h3>
                    Rekening
                    @if (!$is_rekening_required)
                      (Opsional)
                    @endif
                  </h3>
                  <hr>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Bank
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_bank" name="id_bank"
                      @if ($is_rekening_required) required @endif></select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Nomor Rekening
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="tel" class="form-control" id="nomor_rekening" name="nomor_rekening"
                      value="{{ $profil->nomor_rekening ?? old('nomor_rekening') }}"
                      @if ($is_rekening_required) required @endif>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                    Atas Nama
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nama_rekening" name="nama_rekening"
                      value="{{ $profil->nama_rekening ?? old('nama_rekening') }}"
                      @if ($is_rekening_required) required @endif>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto">
                <button type="submit" class="btn btn-block btn-primary">
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet">
  <link href="{{ asset('assets/css/style-select2.css') }}" rel="stylesheet">
@endpush

@push ('js')
  <script src="//unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/imask/6.0.3/imask.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="{{ asset('assets/js/select2-cascade.js') }}"></script>
  <script>
    IMask($('#username')[0], {
      mask: /^[A-Za-z0-9_]+$/,
    });

    IMask($('#nomor_hp')[0], {
      mask: '{+62}-800-0000-00[000]',
      lazy: false,
    });

    IMask($('#nomor_wa')[0], {
      mask: '{+62}-800-0000-00[000]',
      lazy: false,
    });

    IMask($('#nomor_rekening')[0], {
      mask: '0000000000[0000000000]',
      lazy: true,
    });

    $('#tanggal_lahir').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      defaultViewDate: '-19y',
      endDate: '0d',
      startView: 'years',
    });

    var map_jenis_identitas = @json($profil::JENIS_IDENTITAS);
    var list_regex = @json($profil::REGEX);

    var old_nomor_identitas = $('#nomor_identitas').val();
    var imask_nomor_identitas;

    $('#status').change(function () {
      var status = $(this).val();
      var list_jenis_identitas = map_jenis_identitas[status] || [];

      $('#jenis_identitas > option').each(function () {
        if (list_jenis_identitas.indexOf($(this).val()) === -1) {
          $(this).prop('disabled', true).hide();
        } else {
          $(this).prop('disabled', false).show();
        }
      });

      if ($('#jenis_identitas > option:selected').is(':disabled')) {
        $('#jenis_identitas > option:not(:disabled)').prop('selected', true).change();
      }
    }).change();

    $('#jenis_identitas').change(function () {
      var jenis_identitas = String($('#jenis_identitas').val());
      var nomor_identitas = String($('#nomor_identitas').val());
      var regex = list_regex[jenis_identitas] || '';

      regex = regex.replace(/\//g, '');
      regex = new RegExp(regex, 'gi');

      if (imask_nomor_identitas !== undefined) {
        imask_nomor_identitas.destroy();
      }

      if (!nomor_identitas.match(regex)) {
        if (old_nomor_identitas.match(regex)) {
          $('#nomor_identitas').val(old_nomor_identitas).change();
        } else {
          $('#nomor_identitas').val('').change();
        }

        if (nomor_identitas !== '') {
          old_nomor_identitas = nomor_identitas;
        }
      }

      if (jenis_identitas === 'NIK') {
        imask_nomor_identitas = new IMask($('#nomor_identitas')[0], {
          mask: '00.00.00.00-00-00.0000',
          lazy: false,
        });
      } else if (jenis_identitas === 'NIM') {
        imask_nomor_identitas = new IMask($('#nomor_identitas')[0], {
          mask: 'a0[a][0]000000[00]',
        });
      }
    }).change();

    $.ajax({
      url: @json(route('api.web.provinsi')),
      dataType: 'json',
      success: function(data) {
        id_provinsi = @json((string) ($profil->id_provinsi ?? old('id_provinsi')));

        $('#id_provinsi').select2({ data: data }).val(id_provinsi).change();
        $('#id_kota').select2();
      }
    });

    new Select2Cascade(
      $('#id_provinsi'),
      $('#id_kota'),
      @json(route('api.web.kota')) + '/?id_provinsi=:parentId:'
    ).then(function(parent, child) {
      setTimeout(function() {
        id_kota = @json((string) ($profil->id_kota ?? old('id_kota')));
        child.val(id_kota).change();
      }, 200);
    });

    $.ajax({
      url: @json(route('api.web.bank')),
      dataType: 'json',
      success: function(data) {
        id_bank = @json((string) ($profil->id_bank ?? old('id_bank')));
        $('#id_bank').select2({ data: data }).val(id_bank).change();
      }
    });
  </script>
@endpush
