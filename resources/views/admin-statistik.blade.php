@extends ('layouts.argon.dashboard')

@php
  $genders = ['L' => 'Laki-Laki', 'P' => 'Perempuan', 'ALL' => 'Semua'];
@endphp

@section ('content')

<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Statistik</h2>
        </div>

        <div class="card-body">
          @messages

          <style>
            .table.statistik th {
              text-align: center;
              vertical-align: middle !important;
              text-transform: none;
            }

            .table.statistik td {
              text-align: right;
            }
          </style>

          @php
            $anggota = new App\Models\Anggota();
            $types = ['instruktur', 'santri', 'calon_instruktur', 'calon_santri', 'belum_daftar'];
          @endphp

          <div class="table-responsive">
            <table class="table table-bordered table-striped statistik">
              <thead>
                <tr>
                  <th scope="col">Jenis Kelamin</th>

                  @foreach ($types as $type)
                    <th scope="col">
                      {{ Str::title(str_replace('_', ' ', $type)) }}
                    </th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                @foreach ($genders as $gender_code => $gender_name)
                  <tr>
                    <th scope="row">{{ $gender_name }}</th>
                    @foreach ($types as $type)
                      <td>{{ $anggota::getCount($type, $gender_code) }}</td>
                    @endforeach
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          <br>

          <div class="table-responsive">
            <table class="table table-bordered table-striped statistik">
              <thead>
                <tr>
                  <th scope="row" colspan="2">Kelas</th>
                  @foreach ($list_kelas as $kelas)
                    <th scope="colgroup" colspan="{{ $kelas->jenjang_count + 2 }}">
                      {{ $kelas->nama }}
                    </th>
                  @endforeach
                </tr>

                <tr>
                  <th scope="row" colspan="2">Jenjang</th>
                  @foreach ($list_kelas as $kelas)
                    <th scope="col">
                      Belum dites
                    </th>
                    <th scope="col">
                      Diterima
                    </th>
                    @foreach ($kelas->jenjang as $jenjang)
                      <th scope="col">
                        {{ $jenjang->nama }}
                      </th>
                    @endforeach
                  @endforeach
                </tr>
              </thead>

              @php
                $instruktur = new App\Models\Instruktur;
                $santri = new App\Models\Santri;

                $types = ['instruktur', 'santri'];
              @endphp

              <tbody>
                @foreach ($types as $type)
                  @foreach ($genders as $gender_code => $gender_name)
                    <tr>
                      @if ($loop->index === 0)
                        <th rowspan="{{ count($genders) }}" scope="rowgroup">
                          {{ ucfirst($type) }}
                        </th>
                      @endif

                      <th scope="row">{{ $gender_name }}</th>

                      @foreach ($list_kelas as $kelas)
                        <td>
                          {{ $$type::getCount($gender_code, $kelas->id, null) }}
                        </td>
                        <td>
                          {{ $$type::getCount($gender_code, $kelas->id, 'NOT_NULL') }}
                        </td>
                        @foreach ($kelas->jenjang as $jenjang)
                          <td>
                            {{ $$type::getCount($gender_code, $kelas->id, $jenjang->id) }}
                          </td>
                        @endforeach
                      @endforeach
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
