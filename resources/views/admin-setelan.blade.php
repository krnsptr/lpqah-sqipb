@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Setelan
            <a href="#pindah_semester-modal" data-toggle="modal"
              class="btn btn-sm btn-primary float-right">
              <i class="fas fa-share mr-2"></i>
              Pindah Semester
            </a>
          </h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf

            <div class="form-group row">
              <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                Angkatan
              </label>
              <div class="col-md-7">
                <div class="input-group">
                  <input type="text" name="angkatan" id="angkatan" class="form-control"
                    value="{{ $setelan->angkatan }}" readonly>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                Pendaftaran instruktur
              </label>
              <div class="col-md-7">
                <div class="input-group">
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_buka_pendaftaran_instruktur" id="waktu_buka_pendaftaran_instruktur"
                    value="{{ $setelan->waktu_buka_pendaftaran_instruktur }}" required>
                  <div class="input-group-append input-group-prepend">
                    <span class="input-group-text bg-secondary">
                      &ndash;
                    </span>
                  </div>
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_tutup_pendaftaran_instruktur" id="waktu_tutup_pendaftaran_instruktur"
                    value="{{ $setelan->waktu_tutup_pendaftaran_instruktur }}" required>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                Pendaftaran santri
              </label>
              <div class="col-md-7">
                <div class="input-group">
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_buka_pendaftaran_santri" id="waktu_buka_pendaftaran_santri"
                    value="{{ $setelan->waktu_buka_pendaftaran_santri }}" required>
                  <div class="input-group-append input-group-prepend">
                    <span class="input-group-text bg-secondary">
                      &ndash;
                    </span>
                  </div>
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_tutup_pendaftaran_santri" id="waktu_tutup_pendaftaran_santri"
                    value="{{ $setelan->waktu_tutup_pendaftaran_santri }}" required>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                Penjadwalan instruktur
              </label>
              <div class="col-md-7">
                <div class="input-group">
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_buka_penjadwalan_instruktur" id="waktu_buka_penjadwalan_instruktur"
                    value="{{ $setelan->waktu_buka_penjadwalan_instruktur }}" required>
                  <div class="input-group-append input-group-prepend">
                    <span class="input-group-text bg-secondary">
                      &ndash;
                    </span>
                  </div>
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_tutup_penjadwalan_instruktur" id="waktu_tutup_penjadwalan_instruktur"
                    value="{{ $setelan->waktu_tutup_penjadwalan_instruktur }}" required>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                Penjadwalan santri
              </label>
              <div class="col-md-7">
                <div class="input-group">
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_buka_penjadwalan_santri" id="waktu_buka_penjadwalan_santri"
                    value="{{ $setelan->waktu_buka_penjadwalan_santri }}" required>
                  <div class="input-group-append input-group-prepend">
                    <span class="input-group-text bg-secondary">
                      &ndash;
                    </span>
                  </div>
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_tutup_penjadwalan_santri" id="waktu_tutup_penjadwalan_santri"
                    value="{{ $setelan->waktu_tutup_penjadwalan_santri }}" required>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                Publikasi jadwal
              </label>
              <div class="col-md-7">
                <div class="input-group">
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_buka_publikasi_jadwal" id="waktu_buka_publikasi_jadwal"
                    value="{{ $setelan->waktu_buka_publikasi_jadwal }}" required>
                  <div class="input-group-append input-group-prepend">
                    <span class="input-group-text bg-secondary">
                      &ndash;
                    </span>
                  </div>
                  <input type="text" class="form-control flatpickr bg-white"
                    name="waktu_tutup_publikasi_jadwal" id="waktu_tutup_publikasi_jadwal"
                    value="{{ $setelan->waktu_tutup_publikasi_jadwal }}" required>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                Kuota kelompok
              </label>
              <div class="col-md-7">
                @foreach (App\Models\AnggotaProfil::JENIS_KELAMIN as $key => $value)
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-secondary">
                        {{ $key }}
                      </span>
                    </div>
                    <input type="text" name="detail[kuota_kelompok_{{ $key }}]"
                      id="detail[kuota_kelompok_{{ $key }}]" class="form-control mask-number" required
                      value="{{ $setelan->getKuotaKelompok($key) }}">
                    <div class="input-group-append">
                      <span class="input-group-text bg-secondary">
                        santri
                      </span>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>

            <div class="card border shadow-sm my-3">
              <div class="card-header bg-secondary">
                <h3 class="mb-0">Biaya</h3>
              </div>

              <div class="card-body">
                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Label
                  </label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <input type="text" name="detail[biaya][label]" id="detail[biaya][label]"
                        class="form-control" required
                        value="{{ $setelan->detail['biaya']['label'] }}">
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Nominal
                  </label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text bg-secondary">
                          Rp
                        </span>
                      </div>

                      <input type="text" name="detail[biaya][nominal]" id="detail[biaya][nominal]"
                        class="form-control mask-number" required
                        value="{{ $setelan->detail['biaya']['nominal'] }}">
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Keterangan
                  </label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <input type="text" name="detail[biaya][keterangan]" id="detail[biaya][keterangan]"
                        value="{{ $setelan->detail['biaya']['keterangan'] }}"
                        class="form-control" required>
                    </div>
                  </div>
                </div>

                <hr>

                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Rekening Instruktur
                  </label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <select name="detail[biaya][rekening_instruktur_display]"
                        id="detail[biaya][rekening_instruktur_display]"
                        class="form-control" required>
                        <option value=""></option>
                        @foreach ($setelan::REKENING_DISPLAY as $key => $value)
                          <option value="{{ $key }}"
                          @if ($setelan->detail['biaya']['rekening_instruktur_display'] === $key)
                            selected
                          @endif>{{ $value }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Rekening Santri
                  </label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <select type="text" name="detail[biaya][rekening_santri_display]"
                        id="detail[biaya][rekening_santri_display]"
                        class="form-control" required>
                        <option value=""></option>
                        @foreach ($setelan::REKENING_DISPLAY as $key => $value)
                          <option value="{{ $key }}"
                          @if ($setelan->detail['biaya']['rekening_santri_display'] === $key)
                            selected
                          @endif>{{ $value }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="card border shadow-sm my-3">
              <div class="card-header bg-secondary">
                <h3 class="mb-0">Syarat Instruktur</h3>
              </div>

              <div class="card-body">
                @foreach (App\Models\Kelas::all() as $kelas)
                  @php
                    $list_syarat = (
                      $setelan->detail['syarat_instruktur'][$kelas->id]
                      ?? $kelas->detail->syarat_instruktur
                    );
                  @endphp
                  <div class="form-group row">
                    <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                      {{ $kelas->nama }}
                    </label>
                    <div class="col-md-7">
                      <div class="input-group">
                        <textarea class="form-control" rows="3"
                          @if ($setelan->waktu_buka_pendaftaran_instruktur <= now()) readonly @endif
                          name="detail[syarat_instruktur][{{ $kelas->id }}]">{{
                            implode("\r\n", $list_syarat)
                          }}</textarea>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>

            <div class="card border shadow-sm my-3">
              <div class="card-header bg-secondary">
                <h3 class="mb-0">
                  Jalur KBM
                </h3>
                <span class="font-weight-bold text-danger">
                Note: reguler tidak perlu ditulis
                </span>
              </div>

              <div class="card-body">
                @foreach (App\Models\Kelas::all() as $kelas)
                  @php
                    $list_jalur = $kelas->list_jalur;
                  @endphp
                  <div class="form-group row">
                    <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                      {{ $kelas->nama }}
                    </label>
                    <div class="col-md-7">
                      <div class="input-group">
                        <textarea class="form-control" rows="3"
                          @if ($setelan->waktu_buka_pendaftaran_instruktur <= now()) readonly @endif
                          name="detail[jalur][{{ $kelas->id }}]">{{
                            implode("\r\n", $list_jalur)
                          }}</textarea>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>

            <div class="card border shadow-sm my-3">
              <div class="card-header bg-secondary">
                <h3 class="mb-0">Pengumuman</h3>
              </div>

              <div class="card-body">
                @foreach ($setelan->detail['pengumuman'] as $p => $pengumuman)
                  <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">
                      &nbsp;
                    </label>
                    <div class="col-lg-10 my-auto">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="detail[pengumuman][{{ $p }}][show]"
                          id="detail[pengumuman][{{ $p }}][show]" class="custom-control-input" value="1"
                          {{ $pengumuman['show'] ? 'checked' : '' }}>
                        <label class="custom-control-label" for="detail[pengumuman][{{ $p }}][show]">
                          Tampilkan pengumuman untuk <strong>{{ $p }}</strong>
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">
                      Judul
                    </label>
                    <div class="col-lg-10">
                      <div class="input-group">
                        <input type="text" name="detail[pengumuman][{{ $p }}][title]"
                          id="detail[pengumuman][{{ $p }}][title]"
                          value="{{ $setelan->detail['pengumuman'][$p]['title'] }}"
                          class="form-control" required>
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">
                      Isi
                    </label>
                    <div class="col-lg-10 py-2">
                      <textarea name="detail[pengumuman][{{ $p }}][text]"
                        class="form-control ckeditor border"
                        rows="5">{{ $pengumuman['text'] }}</textarea>
                    </div>
                  </div>

                  @if (!$loop->last)
                    <hr>
                  @endif
                @endforeach
              </div>
            </div>

            {{-- <hr class="mt-0"> --}}

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto">
                <button type="submit" class="btn btn-block btn-default">
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<form autocomplete="off" method="post" action="{{ url()->current() }}">
  @csrf

  <div class="modal" tabindex="-1" role="dialog" id="pindah_semester-modal">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Pindah Semester</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <div class="form-group row">
            <label class="col-md-5 col-form-label text-md-right font-weight-bold">
              Angkatan
            </label>
            <div class="col-md-7">
              <div class="input-group">
                <input type="number" name="angkatan" id="angkatan" class="form-control"
                  value="{{ $setelan->angkatan }}">
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary" name="aksi" value="pindah_semester">
            Simpan
          </button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection

@push ('css')
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.css">
  <link rel="stylesheet"
    href="//cdn.jsdelivr.net/npm/selectize-bootstrap4-theme@2.0.2/dist/css/selectize.bootstrap4.css">
  <style>
    div[contenteditable] {
      border: 1px solid rgba(50, 151, 211, 0.25);
      border-radius: 0.375rem;
      padding: 1em;
    }
  </style>
@endpush

@push ('js')
  <script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.js"></script>
  <script src="//unpkg.com/imask@4.1.5/dist/imask.min.js"></script>
  <script src="//unpkg.com/selectize@0.12.6/dist/js/standalone/selectize.min.js"></script>
  <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
  <script>
    $('input.flatpickr').flatpickr({
      enableTime: true,
      enableSeconds: true,
      dateFormat: 'Y-m-d H:i:S',
      time_24hr: true,
    });

    $('input.mask-number').each(function() {
      var id = $(this).attr('id');
      var value = $(this).val();

      var imask = IMask(this, {
        mask: Number,
        scale: 0,
        thousandsSeparator: '.',
        radix: ','
      });

      var parent = $(this).parent();
      var unmasked = $('<input>').attr({
        type: 'hidden',
        id: id + '_unmasked',
        name: id,
        value: value
      });

      $(this).removeAttr('name');
      unmasked.insertAfter(parent);

      imask.on('accept', function() {
        $(unmasked).val(imask.unmaskedValue);
      });
    });

    label = @json($setelan->detail['biaya']['label']);
    keterangan = @json($setelan->detail['biaya']['keterangan']);

    labelList = @json($setelan::BIAYA_LABEL);
    keteranganList = @json($setelan::make()->detail['biaya']['keterangan']).split(',');

    labelOptions = $.map(labelList,function (value) {
      return { value: value, text: value };
    });

    keteranganOptions = $.map(keteranganList,function (value) {
      return { value: value, text: value };
    });

    if (labelOptions.indexOf(label) === -1) {
      labelOptions.push({ value: label, text: label });
    }

    $('#' + $.escapeSelector('detail[biaya][label]')).selectize({
      options: labelOptions,
      delimiter: ',',
      create: true,
      persist: false,
      maxItems: 1,
    });

    $('#' + $.escapeSelector('detail[biaya][keterangan]')).selectize({
      options: keteranganOptions,
      delimiter: ',',
      create: true,
      persist: false,
      maxItems: null,
    });

    $('textarea.ckeditor').each(function() {
      CKEDITOR.inline(this);
    });
  </script>
@endpush
