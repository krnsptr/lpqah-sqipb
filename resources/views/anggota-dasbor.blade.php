@extends('layouts.argon.dashboard')

@php

$color_santri = 'info';
$color_instruktur = 'danger';

$icon_santri = 'fas fa-fw fa-user-edit';
$icon_instruktur = 'fas fa-fw fa-chalkboard-teacher';

$colors_kelas = [
  'THS' => 'purple',
  'THF' => 'warning',
  'ARB' => 'success',
];

@endphp

@section ('content')

<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
  <div class="container-fluid">
    <div class="row">
      <div class="col px-4 px-xl-6">
        <div class="font-weight-bold text-white">
          Asssalamu`alaikum
        </div>
        <div class="display-3 text-white">
          {{ anggota()->nama }}
        </div>
        <p class="text-white mt-2 mb-5">
          Selamat datang di {{ config('app.name') }}.
        </p>
      </div>
    </div>
    <div class="mb-4">
      @messages
      @include ('partials.pengumuman')
    </div>
  </div>
</div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-12">
      <div class="card shadow h-100">
        <div class="card-header">
          <div class="row mb-0">
            <div class="col-6">
              <h3 class="mb-lg-0" id="dasbor">Dasbor</h3>
            </div>
          </div>
        </div>
        <div class="card-body">
          <h4 class="mb-4">
            Program Saya
            @if ($setelan->isDibukaPendaftaran())
              <a href="{{ route('anggota.program_daftar') }}"
                class="btn btn-sm text-capitalize btn-success float-right mb-1">
                Tambah Program
              </a>
            @endif
          </h4>

          @if (anggota()->santri->count() + anggota()->instruktur->count() < 1)
            <div class="alert alert-warning mt-2">
              Kamu belum mendaftar program.
            </div>

            @if ($setelan->isDibukaPendaftaran())
              <div class="alert alert-secondary">
                Klik
                <span class="badge badge-success text-capitalize">
                  Tambah Program
                </span>
                untuk mendaftar.
              </div>
            @else
              <div class="alert alert-danger">
                Pendaftaran program sedang ditutup.
                Harap cek timeline akademik SQ-IPB.
              </div>
            @endif
          @endif

          <div class="row">
            @foreach (anggota()->santri as $santri)
              <div class="col-xl-4 col-lg-6">
                <a href="{{ route('anggota.santri_lihat', $santri) }}"
                  class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Santri {{ $santri->kelas->nama }}
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{-- Jenjang: --}}
                          {{ $santri->jenjang->nama ?? '(Belum dites)' }}
                        </span>
                      </div>
                      <div class="col-auto">
                        @php $color = $colors_kelas[$santri->kelas->kode] @endphp
                        <div class="icon icon-shape bg-{{ $color }} text-white rounded-circle shadow">
                          <i class="{{ $icon_santri }}"></i>
                        </div>
                      </div>
                    </div>
                    <div class="text-muted mt-2">
                      {{-- <i class="fas fa-fw fa-angle-right"></i> --}}
                      <span>
                        <span class="text-sm">Nomor Registrasi:</span>
                        <span class="text-monospace">
                          {{ $santri->nomor_reg_lengkap }}
                        </span>
                      </span>
                    </div>
                  </div>
                </a>
              </div>
            @endforeach

            @foreach (anggota()->instruktur as $instruktur)
              <div class="col-xl-4 col-lg-6">
                <a href="{{ route('anggota.instruktur_lihat', $instruktur) }}"
                  class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Instruktur {{ $instruktur->kelas->nama }}
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{-- Jenjang: --}}
                          {{ $instruktur->jenjang->nama ?? '(Belum dites)' }}
                        </span>
                      </div>
                      <div class="col-auto">
                        @php $color = $colors_kelas[$instruktur->kelas->kode] @endphp
                        <div class="icon icon-shape bg-{{ $color }} text-white rounded-circle shadow">
                          <i class="{{ $icon_instruktur }}"></i>
                        </div>
                      </div>
                    </div>
                    <div class="text-muted mt-2">
                      {{-- <i class="fas fa-fw fa-angle-right"></i> --}}
                      <span>
                        <span class="text-sm">Nomor Registrasi:</span>
                        <span class="text-monospace">
                          {{ $instruktur->nomor_reg_lengkap }}
                        </span>
                      </span>
                    </div>
                  </div>
                </a>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
