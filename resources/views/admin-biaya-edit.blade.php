@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Edit Biaya: NRS.{{ $santri->nomor_reg }}</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor registrasi
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    <a href="{{ route('admin.santri_edit', $santri) }}">
                      <span class="text-monospace">{{ $santri->nomor_reg_lengkap }}</span>
                    </a>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama lengkap
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    <a href="{{ route('admin.anggota_lihat', $santri->anggota) }}">
                      {{ $santri->anggota->nama }}
                    </a>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $santri->kelas->nama }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jenjang
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $santri->jenjang->nama ?? '(Belum dites)' }}
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nominal
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ format_rupiah($santri->biaya->nominal) }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Status
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $santri->biaya->status_text }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Keterangan
                  </label>
                  <div class="col-md-8 col-xl-7 my-auto">
                    {{ $santri->biaya->keterangan ?? '-' }}
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right font-weight-bold">
                    Nominal
                  </label>
                  <div class="col-md-8">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text bg-secondary">
                          Rp
                        </span>
                      </div>

                      <input type="text" name="nominal" id="nominal" class="form-control rupiah"
                        value="{{ $santri->biaya->nominal }}" required>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right font-weight-bold">
                    Status
                  </label>
                  <div class="col-md-8">
                    <select class="form-control" name="status" id="status">
                      <option value=""></option>
                      @foreach ($santri->biaya::STATUS as $key => $value)
                        <option value="{{ $key }}"
                        @if (($santri->biaya->status) == $key)
                          selected
                        @endif>{{ $value }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <div class="row no-gutters">
              <div class="col-xl-12 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-2 col-form-label text-md-right font-weight-bold">
                    Keterangan
                  </label>
                  <div class="col-md-10">
                    <input type="text" name="keterangan" id="keterangan" class="form-control"
                      value="{{ $santri->biaya->keterangan }}">
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto">
                <button type="submit" class="btn btn-block btn-default">
                  Simpan
                </button>
              </div>
              <div class="col-lg-3 col-md-4 mx-auto">
                <a href="javascript:close();" class="btn btn-block btn-secondary">
                  Tutup
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link rel="stylesheet"
    href="//cdn.jsdelivr.net/npm/selectize-bootstrap4-theme@2.0.2/dist/css/selectize.bootstrap4.css">
@endpush
@push ('js')
  <script src="//unpkg.com/imask@4.1.5/dist/imask.min.js"></script>
  <script src="//unpkg.com/selectize@0.12.6/dist/js/standalone/selectize.min.js"></script>
  <script>
    $('input.rupiah').each(function () {
      var id = $(this).attr('id');
      var value = $(this).val();
      var imask = IMask(this, { mask: Number, scale: 0, thousandsSeparator: '.', radix: ',' });
      var parent = $(this).parent();
      var unmasked = $('<input>').attr({ type: 'hidden', id: id + '_unmasked', name: id, value: value });

      $(this).removeAttr('name');
      unmasked.insertAfter(parent);
      imask.on('accept', function () { $('#' + id + '_unmasked').val(imask.unmaskedValue); });
    });

    keterangan = @json($santri->biaya->keterangan);
    keteranganList = @json($setelan->detail['biaya']['keterangan']);

    keteranganOptions = $.map(keteranganList.split(','),function (value) {
      return { value: value, text: value };
    });

    if (keteranganOptions.indexOf(keterangan) === -1) {
      keteranganOptions.push({ value: keterangan, text: keterangan });
    }

    $('#keterangan').selectize({
      options: keteranganOptions,
      delimiter: ',',
      create: true,
      persist: false,
      maxItems: 1,
    });
  </script>
@endpush
