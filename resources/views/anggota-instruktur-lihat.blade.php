@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Instruktur: NRI.{{ $instruktur->nomor_reg }}</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor registrasi
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext text-monospace"
                      value="{{ $instruktur->nomor_reg_lengkap }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nama" name="nama"
                      value="{{ $instruktur->anggota->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Alamat email
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="email" class="form-control-plaintext" id="email" name="email"
                      value="{{ $instruktur->anggota->email }}" disabled>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kelas
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext"
                      value="{{ $instruktur->kelas->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jenjang
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext"
                      value="{{ $instruktur->jenjang->nama ?? '(Belum dites)' }}" disabled>
                  </div>
                </div>

                @if ($instruktur->detail->pendaftaran)
                  <div class="form-group row">
                    <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                      Jenis pendaftaran
                    </label>
                    <div class="col-md-8 col-xl-7">
                      <input type="text" class="form-control-plaintext"
                        value="{{ $instruktur::DETAIL_PENDAFTARAN[$instruktur->detail->pendaftaran] }}"
                        disabled>
                    </div>
                  </div>
                @endif
              </div>
            </div>

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 py-1 text-md-right font-weight-bold">
                    Memenuhi syarat
                  </label>
                  <div class="col-md-8 col-xl-7">
                    @php
                      $kelas = $instruktur->kelas;
                      $syarat_instruktur = $setelan->detail['syarat_instruktur'][$kelas->id] ?? [];
                    @endphp
                    @foreach ($syarat_instruktur as $syarat)
                      <div class="custom-control custom-checkbox py-1">
                        <input type="checkbox" class="custom-control-input" disabled
                          name="detail[syarat_instruktur][{{ $loop->index }}]"
                          id="detail[syarat_instruktur][{{ $loop->index }}]"
                          value="{{ $syarat }}"
                          @if (array_search($syarat, $instruktur->detail->syarat_instruktur) !== false)
                            checked
                          @endif>
                        <label class="custom-control-label"
                          for="detail[syarat_instruktur][{{ $loop->index }}]">
                          {{ $syarat }}
                        </label>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto">
                <a href="{{ route('anggota.dasbor') }}" class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
