@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-lg-6 col-md-6 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Edit Jadwal</h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="main-form">
            @csrf

            <div class="row no-gutters">
              <div class="col-12 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Hari
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="hari" name="hari" required>
                      <option value=""></option>
                      @foreach (App\Enums\Hari::getInstances() as $hari)
                        <option value="{{ $hari->value }}"
                        @if ($jadwal->hari->is($hari)) selected @endif>
                          {{ $hari->description }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Waktu
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <div class="input-group">
                      <input type="text" class="form-control bg-white"
                        id="waktu_mulai" name="waktu_mulai" required
                        value="{{ $jadwal->waktu_mulai->format('H:i') }}">
                      <div class="input-group-append">
                        <span class="input-group-text bg-secondary">
                          WIB
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jalur
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="jalur" name="jalur">
                      <option value="">Reguler</option>
                      @foreach ($list_jalur as $jalur)
                        <option value="{{ $jalur }}"
                        @if ($jadwal->jalur === $jalur) selected @endif>
                          {{ $jalur }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-4 col-md-6 mb-2 mx-auto">
                <button type="submit" class="btn btn-block btn-default">
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.6/flatpickr.min.css"
    integrity="sha384-x4SoWYNT3M7FCKjXbjyLU3aCOAxxwv7FzUip+JKc0H1AeVvmiDAYlwW/C9wQwTf7"
    crossorigin="anonymous">
@endpush

@push ('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.6/flatpickr.min.js"
    integrity="sha384-qR47O9ivmZhlLjVQtQhadoZe1k+TSYZRe5SgOziMjg11vDh1EjUTQ4TedMViShCw"
    crossorigin="anonymous"></script>

  <script>
    $('#waktu_mulai').flatpickr({
      enableTime: true,
      noCalendar: true,
      time_24hr: true,
      dateFormat: 'H:i',
      minTime: @json(App\Models\Jadwal::WAKTU_MULAI_MIN),
      maxTime: @json(App\Models\Jadwal::WAKTU_MULAI_MAX),
    });
  </script>
@endpush
