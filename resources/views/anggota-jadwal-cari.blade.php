@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">Cari Jadwal</h2>
        </div>

        <div class="card-body" style="min-height: 52vh;">
          @messages

          @if (!$setelan->isDibuka('publikasi_jadwal'))
            Jadwal belum dipublikasikan.
          @else
            <form action="{{ route('anggota.jadwal_cari') }}" method="get" autocomplete="off">
              <div class="row">
                <div class="col-lg-8 mx-auto">
                  <div class="card shadow-sm mb-5">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-lg-5 col-md-6 my-1">
                          <label>Jenjang</label>
                          <select class="form-control" name="id_jenjang" id="id_jenjang" required>
                            <option value=""></option>
                            @foreach (App\Models\Kelas::get() as $kelas)
                              <optgroup label="{{ $kelas->nama }}">
                                @foreach ($kelas->jenjang as $jenjang)
                                  <option value="{{ $jenjang->id }}"
                                    @if (request()->input('id_jenjang') === (string) $jenjang->id)
                                      selected
                                    @endif>
                                    {{ $jenjang->nama }}
                                  </option>
                                @endforeach
                              </optgroup>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-lg-5 col-md-6 my-1">
                          <label>Hari</label>
                          <select class="form-control" name="hari" id="hari">
                            <option value="">(Semua hari)</option>
                            @foreach (App\Enums\Hari::getInstances() as $hari)
                              <option value="{{ $hari->value }}"
                                @if ((string) $hari->value === request()->input('hari')))
                                  selected
                                @endif>
                                {{ $hari->description }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-lg-2 pt-2">
                          <div style="line-height: 1.8em;">&nbsp;</div>
                          <button type="submit" class="btn btn-default btn-block">
                            Cari
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>

            <div class="card shadow-sm border mb-5">
              <div class="table-responsive">
                <table class="table table-bordered stack">
                  <thead class="thead-light">
                    <tr>
                      <th>Kelompok</th>
                      <th>Jenjang</th>
                      <th>Jadwal</th>
                      <th>Santri</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($list_kelompok as $kelompok)
                      <tr>
                        <td class="text-monospace">{{ $kelompok->nomor_reg_lengkap }}</td>
                        <td>{{ $kelompok->jenjang->nama }}</td>
                        <td>{{ $kelompok->jadwal->getText() }}</td>
                        <td>
                          {{ $kelompok->santri_count }} orang
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @if ($list_kelompok->count() < 1)
                <div class="row">
                  <div class="col">
                    <div class="alert alert-info fade show m-1">
                      Tidak ada jadwal ditemukan.
                    </div>
                  </div>
                </div>
              @endif
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('js')
  <script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="{{ asset('assets/js/stacktable.js') }}"></script>
  <script>
    $('.table.stack').stacktable({
      displayHeader: false,
    });

    $('.stacktable.large-only').addClass('d-none d-md-table');
    $('.stacktable.small-only').addClass('d-table d-md-none');
    $('.stacktable.small-only .st-head-row').addClass('pt-4 pb-2 text-monospace');
    $('.stacktable th').addClass('bg-secondary');
    $('.stacktable .st-head-row').css('white-space', 'normal');
  </script>
@endpush
