@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-xl-4 col-lg-6">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Anggota Sistem
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['pengguna']) }} akun
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-user"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Santri Diterima
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['santri']) }} orang
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-purple text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-user-edit"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Instruktur Diterima
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['instruktur']) }} orang
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-chalkboard-teacher"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xl-4 col-lg-6">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Belum Daftar
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['belum_daftar']) }} akun
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-default text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-user"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Calon Santri
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['calon_santri']) }} orang
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-user-edit"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Calon Instruktur
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['calon_instruktur']) }} orang
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-chalkboard-teacher"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('js')
  <script>
    $(document).ready(function () {
      if (window.opener !== null) {
        window.close();
      }
    });
  </script>
@endpush
