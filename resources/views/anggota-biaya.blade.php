@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            {{ $setelan->detail['biaya']['label'] }}
          </h2>
        </div>

        <div class="card-body" style="min-height: 52vh;">
          @messages

          @if (!$list_santri->count())
            Kamu belum terdaftar (belum diterima) sebagai santri.
          @else
            <div class="table-responsive">
              <table class="table table-bordered stack">
                <thead class="thead-light">
                  <tr>
                    <th>Kelas</th>
                    <th>Nominal</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($list_santri as $santri)
                    <tr>
                      <td>{{ $santri->kelas->nama }}</td>
                      <td>{{ format_rupiah($santri->biaya->nominal) }}</td>
                      <td>{{ $santri->biaya->status_text }}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

            <br>

            <div>
              @php
                $pengumuman = $setelan->detail['pengumuman']['pembayaran'] ?? null;
              @endphp

              @if (($pengumuman['show'] ?? false))
                <div class="row">
                  <div class="col">
                    <div class="alert alert-secondary fade show my-1">
                      <h4>{{ $pengumuman['title'] ?? '' }}</h4>
                      <div>{!! $pengumuman['text'] ?? '' !!}</div>
                    </div>
                  </div>
                </div>
              @endif
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('js')
  <script src="{{ asset('assets/js/stacktable.js') }}"></script>
  <script>
    $('.table.stack').stacktable({
      displayHeader: false,
    });

    $('.stacktable.large-only').addClass('d-none d-md-table');
    $('.stacktable.small-only').addClass('d-table d-md-none');
    $('.stacktable.small-only .st-head-row').addClass('pt-4 pb-2');
    $('.stacktable th').addClass('bg-secondary');
    $('.stacktable .st-head-row').css('white-space', 'normal');
  </script>
@endpush
